package c_patterns.singleton;

public class MyClass {
    private static MyClass instance;
    private int field;
    private MyClass(int field) {
        this.field = field;
    }

    public static MyClass getInstance(int field) {
        if (instance == null) {
            instance = new MyClass(field);
        }
        return instance;
    }

    public int getField() {
        return field;
    }
}