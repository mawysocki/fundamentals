package c_patterns.singleton;

import java.util.Random;

public class Runner {
    public static void main(String[] args) throws ClassNotFoundException {
        MyClass myClass = MyClass.getInstance(5);
        MyClass myClass1 = MyClass.getInstance(19);

        System.out.println(myClass == myClass1);
        Random r1 = new Random();
        Random r2 = new Random();
        System.out.println(r1 == r2);
        System.out.println(myClass.getField());

    }
}
