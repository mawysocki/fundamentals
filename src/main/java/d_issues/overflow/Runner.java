package d_issues.overflow;

public class Runner {
    public static void main(String[] args) {
        //Ile wynosi wartość bezwzględna z najmniejszej liczby typu Integer?
        System.out.println(Math.abs(Integer.MIN_VALUE));

        //Skąd ta różnica?
        System.out.println(Math.abs(Integer.MIN_VALUE+1));

        //Zakres typu Integer to <-2147483648; 2147483647>
        //Jeśli weźmiemy liczbę -2147483648 i zmienimy na dodatnią 2147483648 to będzie o 1 poza zakresem
        //Dojdzie do przepełnienia licznika i zwrócona wartość to -2147483648

    }
}
