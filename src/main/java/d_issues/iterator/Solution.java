package d_issues.iterator;

import java.util.Iterator;

public class Solution {
    public static void getTwoCubesIterator() {
        System.out.println("\nSolution with Iterator:");
        for (Iterator<Cube> i = MyCollections.CUBE_A.iterator(); i.hasNext();) {
            Cube cube1Next = i.next();
            for (Iterator<Cube> j = MyCollections.CUBE_B.iterator(); j.hasNext();) {
                System.out.printf("Cuba1: %s, Cube2: %s", cube1Next, j.next());
                System.out.println();
            }
        }
    }

    public static void getTwoCubesLoop() {
        System.out.println("\nSolution with Loop:");
        Iterator<Cube> iteratorA = MyCollections.CUBE_A.iterator();
        for (Cube i : MyCollections.CUBE_A) {
            for (Cube j : MyCollections.CUBE_B) {
                System.out.printf("Cuba1: %s, Cube2: %s", i, j);
                System.out.println();
            }
        }
    }
}
