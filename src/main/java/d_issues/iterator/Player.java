package d_issues.iterator;

import java.util.*;

public class Player {

    //Metoda ma wyświetlić wszystkie kombinacje rzutu kostką, dlatego robi pętle podwójną
    public static void getTwoCubes() {
        System.out.println("Wrong way");
        for (Iterator<Cube> i = MyCollections.CUBE_A.iterator(); i.hasNext();) {
            for (Iterator<Cube> j = MyCollections.CUBE_B.iterator(); i.hasNext();) {
                System.out.printf("Cuba1: %s, Cube2: %s", i.next(), j.next());
                System.out.println();
            }
        }
    }
    //PROBLEM: Wszystkich kombinacji powinno być 6x6 czyli 36. Używając iteratora w ten sposób będziemy mieli tylko 6 wywołań
    //Przez to, że iterator A oraz B są takiej długości, nie wyrzuca nam to błędu, więc problem trudniej zdiagnozować
}
