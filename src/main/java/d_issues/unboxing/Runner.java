package d_issues.unboxing;

public class Runner {
    static Integer x;
    //Jaki rezultat wyświetli się na konsoli?
    //a) true
    //b) false
    //c) 0
    //d) Null
    //e) NullPointerException
    public static void main(String[] args) {
        System.out.println(x == 0);
    }
    //zmienna x jest typu referencyjnego a nie prostego, dlatego jej domyślna wartość to null
    //Podczas przyrównania do typu int dokonuje się autounboxing czyli konwersja z typu referncyjnego do prostego
    //Z racji, że x jest null to nie można na nim wykonać takiej operacji i zwróci błąd nullpointerexception
}
