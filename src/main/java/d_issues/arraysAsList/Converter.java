package d_issues.arraysAsList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Converter {

    public static void convertToList() {
        System.out.println("convertToList");
        Integer[] arr = {1, 2, 3};
        //Podczas konwertowania tablicy na listę, lista ta staje się nierozszerzalna
        List<Integer> list = Arrays.asList(arr);
        try {
            list.add(4); // nie będzie działać
        }
        catch (UnsupportedOperationException e) {

        }
        finally {
            list.set(0, 0); // działa
            //list i arr zwracają tę samą zawartość
            //arr została zmodyfikowana poprzez modyfikację list
            //Nie można zwiększać takiej listy ponieważ, nie można zwiększać tablicy z której powstała
            System.out.println("list: " + list);
            String arrayOutput = "arr: [";
            for (Integer integer : arr) {
                arrayOutput += integer;
                arrayOutput += ",";
            }
            arrayOutput += "]";
            System.out.println(arrayOutput);
        }
    }

    public static void convertListWithNewInstance() {
        System.out.println("convertListWithNewInstance");
        Integer[] arr = {1, 2, 3};
        List<Integer> list = new ArrayList<>(Arrays.asList(arr));
        list.add(4); // działa
        list.set(0, 0); // działa na kopii
        System.out.println("list: " + list);
        String arrayOutput = "arr: [";
        for (Integer integer : arr) {
            arrayOutput += integer;
            arrayOutput += ",";
        }
        arrayOutput += "]";
        System.out.print(arrayOutput);
        //Dzięki stworzeniu nowej instancji list jest kopią arr, więc arr nie zmienia swojej wartości
        //List natomiast zmienia poprawnie pierwszy element jak i dodaje nowy
    }
}
