package e_libs.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(6700); // Wybierz dowolny numer portu
        Socket clientSocket = serverSocket.accept(); // Akceptuj połączenie od klienta
        System.out.println("Serwer nasluchuje na porcie 6700...");
        PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        BufferedReader cosnoleReader = new BufferedReader(new InputStreamReader(System.in));
        writer.println("HTTP/1.1 200 OK");
        writer.println("Content-Type: text/html; charset=utf-8");
        writer.println("Connection: Keep-Alive");
        writer.println();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write message:");
        String strIn, strOut = null;
        boolean stopMessage = false;
        while (!stopMessage) {
            while (true) {
                strOut = scanner.nextLine();
                if (strOut.equalsIgnoreCase("STOP")) {
                    break;
                }
                if (strOut.equalsIgnoreCase("EXIT")) {
                    stopMessage = true;
                }
                writer.println(strOut);
            }

            while ((strIn = reader.readLine()) != null) {
                System.out.println("Otrzymano wiadomosc od klienta: " + strIn);
            }

        }
        scanner.close();
        clientSocket.close();


    }
}