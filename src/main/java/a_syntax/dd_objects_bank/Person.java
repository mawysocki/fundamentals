package a_syntax.dd_objects_bank;

import java.util.Random;

public class Person {

    private String name;
    private String lastName;
    private int salary;

    public Person(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public int getSalary() {
        System.out.println("Current salary: " + salary);
        return salary;
    }

    public void goToWork() {
       salary = new Random().nextInt(9000) + 1000;
    }


    //Metoda toString wywoływana jest automatycznie przy użyciu metody println()
    @Override
    public String toString() {
        return String.format("%s %s", name, lastName);
    }



}
