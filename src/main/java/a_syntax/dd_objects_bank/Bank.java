package a_syntax.dd_objects_bank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Bank {

    private String bankName;
    private int creditPercent;
    private List<Person> clients;
    private HashMap<Person, Integer> accounts;

    public Bank(String bankName, int creditPercent) {
        this.bankName = bankName;
        this.creditPercent = creditPercent;
        clients = new ArrayList<>();
        accounts = new HashMap<>();
    }

    public void registerClient(Person person, int depositMoney) {
        if (!isClientExist(person)) {
            clients.add(person);
            accounts.put(person, depositMoney);
            System.out.println(person + " registered successfully");
        } else {
            System.out.println(person + " already exists");
        }
    }

    public void displayAllClients() {
        System.out.println("Clients in bank: " + bankName);
//        if (clients.isEmpty()) {
//            System.out.println("No clients..");
//        } else {
//            for (Person client : clients) {
//                System.out.println(String.format("%s. Account: %s", client, accounts.get(client)));
//            }
//        }

        clients.forEach(System.out::println);

    }

    public boolean isClientExist(Person client) {
        boolean isExist = clients.contains(client);
        if (!isExist) {
            System.out.println("Client " + client + " does not exist in bank " + bankName);
        }
        return isExist;
    }

    public boolean validateCredit(Person client, int expectedCredit, int months) {
        System.out.println(String.format("Credit verification in bank %s for client: %s", bankName, client));
        if (isClientExist(client)) {
            int cashBack = expectedCredit * (100 + creditPercent) / 100;
            int maxValue = (client.getSalary() / 2) * months;
            System.out.println("Total sum back: " + cashBack);
            System.out.println("Maximum client abilities: " + maxValue);
            System.out.println("Credit accepted: " + (maxValue >= cashBack));
            return maxValue >= cashBack;
        } else {
            return false;
        }
    }

    public int withdrawMoney(Person person, int money) {
        if (isClientExist(person)) {
            int currenteState = accounts.get(person);
            if (currenteState >= money) {
                accounts.put(person, currenteState-money);
                return money;
            } else {
                System.out.println("Sorry. You don't have enough money");
                return 0;
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return String.format("Current offer: %s percent", creditPercent);
    }
}
