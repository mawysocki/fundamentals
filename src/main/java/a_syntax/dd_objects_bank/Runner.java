package a_syntax.dd_objects_bank;

public class Runner {

    public static void main(String[] args) {
        Person client1 = new Person("Mark", "Jonson");
        Person client2 = new Person("Anna", "Owen");
        System.out.println(client1);
        client1.getSalary();
        client1.goToWork();
        client1.getSalary();

        Bank bank1 = new Bank("West", 5);
        Bank bank2 = new Bank("East", 6);
        System.out.println(bank1);
        System.out.println(bank2);

        System.out.println(bank1.isClientExist(client1));
        bank1.registerClient(client1, 10000);
        System.out.println(bank1.isClientExist(client1));
        bank1.registerClient(client1, 10000);
        bank1.registerClient(client2, 15000);
        bank1.displayAllClients();
        bank2.displayAllClients();
        System.out.println("=========CREDITS========");
        bank1.validateCredit(client2, 100000, 60);
        client2.goToWork();
        bank2.validateCredit(client2, 200000, 72);
        bank2.registerClient(client2, 50000);
        bank2.validateCredit(client2, 200000, 72);

        System.out.println(client2 + ": my cash:");
        int myCash = bank2.withdrawMoney(client2, 100000);
        System.out.println(client2 + ": first withdraw: " + myCash);
        myCash = bank2.withdrawMoney(client2, 10000);
        System.out.println(client2 + ": second withdraw: " + myCash);
        System.out.println("--------CLIENT 3--------");
        Person client3 = new Person("Carol", "Wright");
        System.out.println(client3 + ": second withdraw: " + myCash);
        myCash = bank2.withdrawMoney(client3, 5000);
        System.out.println(client3 + ": second withdraw: " + myCash);
        bank2.registerClient(client3, 10000);
        myCash = bank2.withdrawMoney(client3, 8000);
        System.out.println(client3 + ": second withdraw: " + myCash);
        myCash = bank2.withdrawMoney(client3, 5000);
        System.out.println(client3 + ": second withdraw: " + myCash);


    }
}
