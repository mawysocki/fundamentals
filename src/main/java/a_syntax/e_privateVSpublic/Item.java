package a_syntax.e_privateVSpublic;

//Prosty obiekt typu Item posiada tylko 2 pola.
//Nazwa po której identyfikujemy handlowany przedmiot
//Jego nazwa nadawana jest przy stworzeniu i nie ma możliwości dalszej modyfikacji
//Wartość przedmiotu nadawana jest również na początku ale dzięki metodzie "setValue" mamy możliwość zmiany
public class Item {
    private String name;
    private int value;

    public Item(String name, int value) {
        this.name = name;
        this.value = value;
    }

    //Mamy tutaj przykład użycia getterów i setterów
    //Akurat dla imienia mamy tylko getter, bo chcemy znać jego wartość ale nie chcemy jej modyfikować
    //Dla pola value mamy zarówno getter jak i setter
    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }
}
