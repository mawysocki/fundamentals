package a_syntax.e_privateVSpublic;

public class HumanResources {

    //Kadry dokonują przeglądu celów.
    //Jako parametr przyjmują cały obiekt sprzedawcy
    //Wykorzystywane jest jego imię ale nie jest one brane z pola tylko z metody (GETTER)
    //Metoda "isTargetAchived" sprawdza czy cel został osiągnięty,
    //a "toTarget" zlicza nam tylko ile brakuje do celu
    //Natomiast same kadry nie mają dostępu do dokładnych danych odnośnie aktualnego stanu konta
    public void checkTarget(SalesMan man) {
        System.out.println("KONTROLA KADR");
        if (man.isTargetAchived()) {
            System.out.println(man.getName() + " spełnił cele. Może iść na urlop");
        }
        else {
            System.out.println(man.getName() + " - do celu brakuje " + man.toTarget());
            if (man.isDebt()) {
                System.out.println(man.getName() + " ma debet!");
            }
        }

    }

}
