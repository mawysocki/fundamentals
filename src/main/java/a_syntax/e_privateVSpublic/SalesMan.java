package a_syntax.e_privateVSpublic;

import java.util.ArrayList;
import java.util.List;

public class SalesMan {

    //Sprzedawca ma kilka pól które go indentyfikują ale wszystkie z nich są PRYWATNE
    //To znaczy, że dostęp do nich jest tylko z poziomu tej klasy
    //W ten sposób nie można modyfikować tych pól ani nawet znać wartości poza klasą.
    //To wymusza przygotowanie odpowiednich publicznych metod, które będą dane pola modyfikowąć
    //Będzie to jedyna forma w jakiej możemy wpłynąć na te pola z zewnątrz.
    private final String name;
    private final String lastName;
    private int experience = 0;
    private int money = 0;
    private final int target;
    private final List<Item> items;

    public SalesMan(String name, String lastName, int experience) {
        this.name = name;
        this.lastName = lastName;
        this.experience = experience;
        target = experience * 10000;
        items = new ArrayList<>();
    }

    public String getName() {
        return name;
    }
    public void introduceYourself() {
        System.out.println("Jestem " + name);
        System.out.println("Moje nazwisko to " + lastName);
        System.out.println("Mam " + experience + " lat doświadczenia");
        System.out.println("Mój cel roczny to osiągnąć " + target + " PLN");
        System.out.println("___________________________");
    }

    public void buyItem(Item item) {
        reduceMoney(item.getValue());
        //Zwieksza wartość danego przedmiotu dwukrotnie
        item.setValue(item.getValue() * 2);
        //Dodaj do listy elementów które posiada
        items.add(item);
    }

    public void sellItem(String nameItem) {
        //Szuka po nazwie na liście przechowywanych przedmiotów
        for (Item item: items) {
            //Gdy znajdzie odpowiedni element to dokonuje jego sprzedaży
            //poprzez dodanie jego wartości do swojego konta
            //Na końcu usuwa element z listy
            if (nameItem.equals(item.getName())) {
                addMoney(item.getValue());
                items.remove(item);
                break;
            }
        }
    }

    //Na zewnątrz klasy można zweryfikować czy ilość pieniedzy przekroczyła cel,
    //chociaż nie znamy dokładnie ani celu ani aktualnego stanu konta
    public boolean isTargetAchived(){
        return money >= target;
    }
    public boolean isDebt() {
        return money < 0;
    }
    public int toTarget() {
        return target-money;
    }

    private void addMoney(int value) {
        money += value;
    }
    private void reduceMoney(int value) {
        money -= value;
    }



}
