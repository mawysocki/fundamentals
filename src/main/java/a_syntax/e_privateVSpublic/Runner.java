package a_syntax.e_privateVSpublic;

public class Runner {
    public static void main(String[] args) throws NoSuchFieldException {
        //Tworzymy obiekt kadr odpowiedzialnych za sprawdzanie sprzedawców
        HumanResources hr = new HumanResources();

        //Tworzymy dwóch sprzedawców. Jako parametr podajemy lata doświadczenia
        SalesMan mirek = new SalesMan("Mirosław", "Bogucki", 3);
        SalesMan andrzej = new SalesMan("Andrzej", "Krysik", 10);
        mirek.introduceYourself();
        andrzej.introduceYourself();

        //Zakup odbywa się poprzez stworzenie nowego przedmiotu z nazwą i jego wartością
        mirek.buyItem(new Item("TV", 2000));
        mirek.buyItem(new Item("Opel", 15000));
        mirek.buyItem(new Item("Polisa", 4000));
        mirek.buyItem(new Item("Rower", 1000));
        mirek.buyItem(new Item("Złoto", 10000));

        //Podczas sprzedaży wystarczy podać nazwę gdyż cena jest juz ustalona wcześniej i zależy od ceny kupna
        mirek.sellItem("Złoto");
        mirek.sellItem("Polisa");
        mirek.sellItem("Samochód");
        mirek.sellItem("TV");

        andrzej.buyItem(new Item("Kawalerka", 100000));
        andrzej.buyItem(new Item("Kino domowe", 6000));
        andrzej.buyItem(new Item("Ford Mustang", 75000));
        andrzej.buyItem(new Item("Biżuteria", 20000));
        andrzej.buyItem(new Item("Srebro", 5000));


        andrzej.sellItem("Ford Mustang");
        andrzej.sellItem("Kino domowe");
        andrzej.sellItem("Srebro");

        //Pierwsza kontrola kadr czy cele zostały spełnione
        hr.checkTarget(mirek);
        hr.checkTarget(andrzej);

        mirek.sellItem("Opel");
        andrzej.sellItem("Biżuteria");

        //Druga kontrola kadr czy cele zostały spełnione
        hr.checkTarget(mirek);
        hr.checkTarget(andrzej);

        andrzej.sellItem("Kawalerka");

        //Trzecia kontrola kadr czy cele zostały spełnione
        hr.checkTarget(andrzej);

    }



}
