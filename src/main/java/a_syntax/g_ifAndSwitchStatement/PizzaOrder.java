package a_syntax.g_ifAndSwitchStatement;

public class PizzaOrder {
    public int findPriceA(String size) {
        int price = 0;
        switch (size) {
            case "S":
                price = 25;
                break;
            case "M":
                price = 30;
                break;
            case "L":
                price = 35;
                break;
            case "XL":
                price = 40;
                break;
            default:
                System.out.println("Nie ma takiego rozmiaru");
                break;

        }
        return price;
    }

    //Switch z wykorzystaniem return nie wymaga użycia break
    //Return musi być wywołany w każdej możliwej ścieżce. Każdy case oraz ścieżka default
    public int findPriceB(String size) {
        int price = 0;
        switch (size) {
            case "S":
                return 25;
            case "M":
                return 30;
            case "L":
                return 35;
            case "XL":
                return 40;
            default: {
                System.out.println("Nie ma takiego rozmiaru");
                return 0;
            }

        }


    }

    //Nowa wersja switch z java 14
    public int findPriceC(String size) {
        return switch (size) {
            case "S" -> 25;
            case "M" -> 30;
            case "L" -> 35;
            case "XL" -> 40;
            default -> 0;
        };
    }


}
