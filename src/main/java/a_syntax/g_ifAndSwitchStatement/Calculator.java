package a_syntax.g_ifAndSwitchStatement;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Calculator {

    //Klasa jest hermetycznie zamknięta majac jedynie jedną metodę publiczną
    //Pozostałe metody są składowymi na całość obliczenia i nie ma potrzeby, aby były uruchamiane oddzielnie
    //Mogą więc pozostac jako prywatne
    public void calculate() {
        calculate(getNumber(), getNumber(), getOperator());
    }

    //Klauzula default jest pusta ponieważ nie ma możliwości podania błędnego argumentu w tym miejscu
    //wywołanie metody calculate wystepuje jedynie w ciele publicznej wersji metody calculate
    //Tam natomiast jako operator przekazany jest wynik metody getOperator
    private void calculate(double x, double y, String operator) {
        //Każdy z dostępnych operatorów ma swój odpowiednik do wywołanej metody
        switch (operator) {
            case "+":
                add(x, y);
                break;
            case "-":
                substract(x, y);
                break;
            case "*":
                multiply(x, y);
                break;
            case "/":
                divide(x, y);
                break;
            default:
                break;
        }
    }

    //Wymaga pobrania dwóch numerów, więc zostało opakowane w metodę, którą można wykonać 2 razy
    //Zmniejsza to użycie kodu, zwiększając czytelność
    private double getNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę:");
        return scanner.nextDouble();
    }

    //Metoda ta zwraca odpowiedni operator matematyczny wybrany przez użytkownika
    private String getOperator() {
        Scanner scanner = new Scanner(System.in);
        //Pozwala na wybranie jedynie tych dostępnych w kolekcji Set.
        Set<String> operators = new HashSet<>(Arrays.asList("+", "-", "*", "/"));
        System.out.println("Wybierz jeden z dostępnych operatorów: + - * /");
        String operator = scanner.next();
        //Następuje sprawdzenie czy wybrany element znajduje się w kolekcji
        if (operators.contains(operator)) {
            //Jeśli tak to go zwraca
            return operator;
        } else {
            //Jeśli nie to następuje rekurencyjne wywołanie metody (czyli sama siebie)
            //Powoduje to ponowne wykonanie się jej aż do osiągnięcia pożądanego rezultatu
            System.out.println("Nieprawidłowy operator!");
            return getOperator();
        }
    }

    //Każda operacja rozdzielona jest na oddzielną metodę.
    //Zwiększa to granulacje i czytelność
    private void add(double x, double y) {
        System.out.println(String.format("Dodawanie: %s + %s = %s", x, y, x + y));
    }

    private void substract(double x, double y) {
        System.out.println(String.format("Odejmowanie: %s - %s = %s", x, y, x - y));
    }

    private void multiply(double x, double y) {
        System.out.println(String.format("Mnożenie: %s * %s = %s", x, y, x * y));
    }

    private void divide(double x, double y) {
        if (y > 0) {
            System.out.println(String.format("Dzielenie: %s / %s = %s", x, y, x / y));
        } else {
            System.out.println("Nie dzielimy przez zero!");
        }
    }
}
