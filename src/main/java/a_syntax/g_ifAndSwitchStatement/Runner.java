package a_syntax.g_ifAndSwitchStatement;

import java.util.Scanner;

public class Runner {
    public static void main(String[] args) {
        pizza();
        calculator();
    }

    public static void pizza() {
        PizzaOrder pizzaOrder = new PizzaOrder();
        Scanner s = new Scanner(System.in);
        System.out.println("Podaj rozmiar jaki chcesz");
        //Pobieramy wartośc z konsoli
        String size = s.nextLine();
        System.out.println(pizzaOrder.findPriceA(size));
        System.out.println(pizzaOrder.findPriceB(size));
        System.out.println(pizzaOrder.findPriceC(size));
        //Wyświetli, że nie ma takiego rozmiaru
        System.out.println(pizzaOrder.findPriceA("XXL"));
        System.out.println(pizzaOrder.findPriceB("XXL"));
        System.out.println(pizzaOrder.findPriceC("XXL"));
    }

    public static void calculator() {
        //Tworzenie obiektu bez przypisywania do zmiennej
        //Pozwala to wywołać bezposrednio metodę ale tylko raz
        new Calculator().calculate();
    }
}
