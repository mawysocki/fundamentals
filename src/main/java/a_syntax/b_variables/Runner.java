package a_syntax.b_variables;

import java.util.Scanner;

public class Runner {
    //Java jest silnie typowana!
    //Oznacza to, że każda zmienna która ma jakieś dane musi mieć podane jakiego typu są to dane
    //Składnia: Typ nazwa = wartość;
    public static void main(String[] args) {
        //Zmienne w javie dzielą się na 2 typy:
        //Proste i referencyjne
        //Typy proste mają bezpośrednie odwołanie do wartości
        //Typy referencyjne (złożone) mają odwołanie do pamięci, gdzie wartośc jest przechowywana
        primitives();
        references();
        fitNumbers();
    }


    //Typy proste to takie które mają bezpośrednio przypisaną wartość do zmiennej.
    //Posiadają domyślne wartości w zalezności od typu. Dla liczb domyślną wartością jest 0.
    public static void primitives() {
        //Typy całkowitoliczbowe z maksymalnymi wartościami
        //Byte to 8 bitów czyli daje 2^8=256 kombinacji. 1 bit przeznaczony na znak.
        //Daje to przedział <-128; 127>
        byte byteNumber = -127;
        //Short składa się z 2 bajtów czyli 16 bitów. Sytuacja analogiczna 2^16=..
        short shortNumber = 32767;
        //Int składa się z 4 bajtów czyli 32 bitów. Sytuacja analogiczna 2^32=..
        int intNumber = 2147483647;
        //Long składa się z 8 bajtów czyli 64 bitów. Sytuacja analogiczna 2^64=..
        long longNumber = 9223372036854775807L;

        //Typy float i double są dla liczb zmienno przecinkowych
        //Nie ma tu maksymalnej wartości,
        //gdyż duże liczby są zamieniane na notację naukową (2 miliardy = 2.0E9
        //Double ma większy poziomem precyzji
        float floatNumber = 99999999999.999F;
        double doubleNumber = 99999999999.999;
        System.out.println("Float & double");
        System.out.println(floatNumber);
        System.out.println(doubleNumber);

        //Char to znaki specjalne zakodowane w postaci Unicode.
        //https://pl.wikipedia.org/wiki/UTF-16
        //Wykorzystuje 2 bajty ale zapis jest w postaci 16tkowej (hexadecymalnej) Cyfry od 0 do F
        //Wartość występuje w apostrofach, a kod znaku poprzedzony musi być poprzedzony backslashem oraz literą u
        char dollar = '\u0024';
        char euro = '\u20AC';
        System.out.println("Wolisz zarabiać w " + dollar + " czy w " + euro + "?");

        //Typ boolean przyjmuje tylko "true" albo "false"
        //Pozwala to wyrażać czy dane sformułowanie jest prawdziwe czy nie
        boolean trueOrFalse = true;
        //Zmienna która została już zadeklarowana (<typ> <nazwa>) może uzyskać nową wartość
        //Podawanie ponownie typu jest zakazane. Wystarczy użyć samej zmiennej
        trueOrFalse = false;
//        trueOrFalse = 0; - nieprawidłowy typ
    }

    private static void references() {
        //String NIE jest typem prostym tylko referencyjnym
        //Domyślna wartość dla typu referencyjnego jest "null"
        String textempty;
//        System.out.println(textempty); - nie można wykonać ponieważ zmienna nie jest zainicjalizowana.
        //W przypadku typu prostego można byłoby to zrobić

        //W zmiennej text1 tak naprawdę nie jest modyfikowana istniejąca wartość,
        // a powstaje nowy obiekt typu String.
        // Określa się, że taka klasa jest IMMUTABLE. Czyli obiektu takiego nie da się modyfikować
        //W javie dla Stringu zastosowany jest zapis skrótowy, który złudząco przypomina typ prosty
        String text1 = "Mój pierwszy tekst";
        text1 = "Mój drugi tekst";
        //Można potraktować to jako nowy obiekt
        text1 = new String("Mój trzeci tekst");
        System.out.println(text1);

    }

    private static void primitiveVSReference() {
        //Każdy obiekt przechowywany w zmiennej to również typ referencyjny
        String q = "Mój tekst";
        MyClass obj1 = new MyClass("abc", 5);
        MyClass obj2 = new MyClass("abc", 5);
    }

    //Metoda przyjmuje wartość od użytkownika i weryfikuje w jakiego rodzaju zmiennej może być przechowyhwana
    private static void fitNumbers() {
        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            try {
                System.out.println("Podaj liczbę: ");
                long x = sc.nextLong();
                System.out.println(x + " można przechować w:");
                if (x >= -128 && x <= 127) System.out.println("* byte");
                if (x >= -32768 && x <= 32767) System.out.println("* short");
                if (x >= -2147483648 && x <= 2147483647) {
                    System.out.println("* int");
                }
                System.out.println("* long");
            } catch (Exception e) {
                System.out.println(sc.next() + " can't be fitted anywhere.");
            }
        }
    }

}

