package a_syntax.b_variables;

public class MyClass {

    private String x;
    private int y;

    public MyClass(String x, int y) {
        this.x = x;
        this.y = y;
    }


    @Override
    public String toString() {
        return "MyClass{" +
                "x='" + x + '\'' +
                ", y=" + y +
                '}';
    }
}
