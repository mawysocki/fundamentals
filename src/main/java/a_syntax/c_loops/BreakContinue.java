package a_syntax.c_loops;

public class BreakContinue {

//  continue jest słowem kluczowym który przerywa wykonanie danej iteracji i przechodzi do następnej
//  w tym wypadku wywołane jest wtedy gdy reszta z dzielenia jest 1 czyli gdy f jest nieparzyste
//  co za tym idzie zostaną wyświetlone tylko liczby parzyste
//  w przypadku liczb nieparzystych nie było możliwości wykonania println, bo wcześniej wystąpił continue.
    public void loopWithContinue() {
        System.out.println("Pętla z użyciem continue: ");
        for (int f = 0; f < 10; f++) {
            if (f % 2 == 1) {
                continue;
            }
            System.out.println("f: " + f);
        }
    }
//  Break jest drugim sposobem na zatrzymanie pętli zaraz po niespełnionym warunku.
//  W tym przypadku warunek był zawsze spełniony, ponieważ zaczynaliśmy od g=0 i wartości szły w góry
//  Tutaj umieszczenie break w środku powodu zatrzymanie pętli po 10 razie
    public void loopWithBreak() {
        System.out.println("Pętla z użyciem break: ");
        int g = 0;
        while (g >= 0) {
            if (g > 10) {
                break;
            }
            System.out.println("g: " + g);
            g++;
        }
    }
//  Uzycie obydwu keywordów w jednej pętli
//  Pętla sama w sobie jest nieskończona ale ma przerwanie dzięki break, gdy zostanie spełniony warunek w if
//  Continue przeskakuje wyświetlanie tekstu gdy liczba nie jest parzysta
    public void breakAndContinue() {
        int h = 0;
        while (true) {
            h+=1;
            if (h % 2 == 1) {
                continue;
            }
            if (h > 10) {
                break;
            }
            System.out.println("h: " + h);
        }
    }
}
