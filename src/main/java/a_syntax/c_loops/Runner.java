package a_syntax.c_loops;

public class Runner {
    public static void main(String[] args) {
        Loops loops = new Loops();
        BreakContinue breakContinue = new BreakContinue();

        System.out.println("Moje pętle: ");
        loops.forLoop();
        loops.whileLoop();
        loops.doWhileLoop();
        loops.foreachLoop();
        breakContinue.loopWithContinue();
        breakContinue.loopWithBreak();
        breakContinue.breakAndContinue();
    }




}
