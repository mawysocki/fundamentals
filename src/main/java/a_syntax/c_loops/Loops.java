package a_syntax.c_loops;

public class Loops {
    //  Pętla for ma 3 pola.
//  1 - deklaracja zmiennej z początkową wartością biorąca udział w iteracji
//  int a = 0
//  2 - warunek który definiuje jak długo będzie realizowana pętla (działa dopóki jest to prawda)
//  a < 10
//  3 - akcja wykonana po pojedyńczym przejściu pętli, aby w końcu zmienna mogła osiągnąć cel zatrzymujący pętlę
//  a++ jest to zwiększenie wartości "a" o 1 po wykonaniu raz zawartości pętli
//  Nazywa to się INKREMENTACJĄ
    public void forLoop() {
        System.out.println("Pętla for: ");
        for (int a = 0; a < 10; a++) {
            System.out.println("a: " + a);
        }
    }

    //  Pętla while ma w sobie jedynie warunek który musi być spełniony, aby pętla mogła się wykonać.
//  Czyli druga część z pętli for.
//  Deklaracja zmiennej musi odbyć się wcześniej
//  Natomiast modyfikacja zmiennej odbywa się wewnątrz pętli
//  b-- to zmniejszenie wartości zmiennej b o 1
//  Nazywa się to DEKREMENTACJĄ
    public void whileLoop() {
        System.out.println("Pętla while: ");
        int b = 10;
        while (b > 0) {
            System.out.println("b: " + b);
            b--;
        }
    }

//  Podobna do pętli while. Z tą różnicą, że tutaj pętla zawsze wykona się co najmniej raz.
//  Najpierw wykona się część z bloku "do" a potem dopiero sprawdzany jest warunek
    public void doWhileLoop() {
        System.out.println("Pętla do while: ");
        int c = 0;
        do {
            System.out.println("c: " + c);
            c++;
        } while (c > 0 && c < 5);
//  Pętla do while przy danych warunkach wykona się 5 razy
//  Dla c=0 jeszcze nie zdążył być sprawdzony warunek, a iteracja się wykonała
//  Przy pętli while dla tych samych warunków pętla w ogóle się nie wykona
//  d równe 0 jest odrzucane już na początku przez co w ogóle nie wchodzi do pętli
//  Tym sposobem wartość d nie ma możliwość ulec zmianie i uruchomić kiedykolwiek pętlę
        int d = 0;
        while (d > 0 && d < 5) {
            System.out.println("d: " + d);
            d++;
        }
    }

    //  Pętla foreach używa tego samego słowa kluczowego co for
//  Różnica polega na tym, że foreach nie używa liczb do iteracji, a kolekcji
//  Tzn przechodzi przez wszystkie elementy zbioru, bez względu na wielkość zbioru
//  W nawiasie nalezy zadeklarować nazwę zmiennej dla pojedyńczego elementu danego zbioru.
//  Typ zmiennej musi być zgodny z typem zbioru.
//  Tzn jeśli tablica jest typu String to zmienna również musi być String
//  Po dwukropku podajemy nazwę zbioru po którym iterujemy
    public void foreachLoop() {
        String[] listaImion = new String[5];
        listaImion[0] = "Mariusz";
        listaImion[1] = "Andrzej";
        listaImion[2] = "Anna";
        listaImion[3] = "Karolina";
        listaImion[4] = "Rafał";
        for (String imie : listaImion) {
            System.out.println("Aktualne imie: " + imie);
        }
    }
}
