package a_syntax.k_collections;


//Niektóre klasy nie są domyślnie widocznie i wymagają zaimportowania
import java.util.HashMap;
//Dotyczy to również naszych własnych klas jeśli powstały w innej przestrzeni (folderze) niż klasa w której jesteśmy
import a_syntax.d_classesAndObjects.Car;

public class MyMap {
    //Mapa nie ma uporządkowanych elementów, więc nie da się odwołać za pomocą indeksu
    //Do elementu odwołujemy się za pomocą klucza
    //Nie można mieć duplikatów kluczy. (Nadpisuje istniejącą wartość dla klucza)
    //Mapa nazywana jest również słownikiem w innych językach
    private HashMap<String, Car> owners;

    public MyMap() {
        this.owners = createMap();
    }

    private HashMap<String, Car> createMap() {
        //Mapa z 4 elementami gdzie kluczem sa imiona a wartością obiekty typu Car
        //Obiekty te nie są przypisane do żadnych zmiennych tylko bezpośrednio do mapy
        //Oznacza się, że nie da się do nich odnieść w inny sposób niż poprzez mapę
        HashMap<String, Car> carOwners = new HashMap();
        carOwners.put("Andrzej", new Car("Porshe", 2016, 3.5, 90000));
        carOwners.put("Robert", new Car("Audi", 2007, 1.9, 211000));
        carOwners.put("Marek", new Car("Ford", 2015, 2.0, 116000));
        carOwners.put("Alicja", new Car("Toyota", 1999, 1.4, 390000));
        return carOwners;
    }

    public void displayCar(String name) {
        //Klasa Car została wzięta z innego katalogu
        //Mapa "owners" zwraca obiekt klasy Car na bazie klucza (imienia właściciela)
        //getBrand wywołane bezpośrednio na obiekcie typu Car, który zwróciła mapa

        //Następuje tutaj sprawdzenie czy dana występuje jako klucz w mapie
        if (this.owners.containsKey(name)) {
            System.out.println(String.format("%s posiada samochód marki: %s", name, this.owners.get(name).getBrand()));
        } else {
            //Jeśli nie to znaczy to jest jednoznaczne z tym, że nie jest posiadaczem samochodu
            System.out.println(String.format("%s nie posiada samochodu", name));
        }

    }
}
