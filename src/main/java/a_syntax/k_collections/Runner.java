package a_syntax.k_collections;

public class Runner {
    public static void main(String[] args) {
        seperate("Array");
        array();
        seperate("List");
        list();
        seperate("Map");
        map();
        seperate("Set");
        set();



    }

    private static void array() {
        int x = 5; int y = 9;
        MyArray myArray = new MyArray();
        myArray.displayResult(x, y);
    }

    private static void list() {
        new MyList();
    }
    private static void map() {
        MyMap myMap = new MyMap();
        myMap.displayCar("Robert");
        myMap.displayCar("Ryszard");
    }

    private static void set() {
        MySet mySet = new MySet();
        mySet.createSet();
    }

    private static void seperate(String category) {
        System.out.println("___________\n"+category);
    }
}
