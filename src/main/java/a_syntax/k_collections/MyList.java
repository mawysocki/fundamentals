package a_syntax.k_collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class MyList {
    String[] namesArr = {"Adam", "Ewa", "Kacper"};
    public MyList() {
        convertFromArray();
        iterate();
        removeWithLoop();
        removeWithStream();
    }

    private void convertFromArray() {

        List<String> names1 = Arrays.asList(namesArr);
        List<String> names2 = Arrays.asList("Adam", "Ewa", "Kacper");
        //Nie można rozszerzać listy jeśli jej powstanie wywodzi się z tablicy
        //Grozi błędem UnsupportedOperationException
        //  names1.add("Robert");
        //  names2.add("Robert");3

        //Działa to również w drugą stronę. Mimo, że obiekt istnieje to nie można go usunąć
        //Spowodwane jest tym, że lista w tej implementacji jest niemodyfikowalna
        System.out.println(String.format("Pierwszy element: %s", names1.get(0)));
//        names1.remove(0);

        //Należy wywołać ją poprzez konstruktor
        List<String> names3 = new ArrayList<>(Arrays.asList(namesArr));
        //Dodawanie i usuwanie elementów z takiej listy jest dozwolone
        names3.add("Robert");
        //Metoda usuwająca element zwraca go, wiec można go później jeszcze użyć
        String removed = names3.remove(0);
        System.out.println(String.format("Usunięta osoba: %s", removed));

    }

    private void iterate() {
        List<String> names = new ArrayList<>(Arrays.asList(namesArr));
        //Użycie iteratora do przejścia po kolekcji
        Iterator<String> iterator = names.iterator();
        //Sprawdza czy występuje kolejny element w kolekcji
        while (iterator.hasNext()){
            //Odwołuje się do kolejnego elementu
            System.out.println(iterator.next());
            //Jeśli metoda byłaby użyta
        }
    }

    //ANTYWZORZEC
    //Nie powinno się usuwać elementów za pomocą listy i indeksów
    private void removeWithLoop() {
        List<String> users = new ArrayList<>(
                Arrays.asList("User", "User1", "User2", "User3", "User4")
        );

        //Indeks za każdym razem zwiększa się o 1 natomiast rozmiar listy jest ruchomy
        //To powoduje, że część elementów zostanie pominięta
        for (int i = 0; i < users.size(); i++) {
            System.out.println(String.format("index %s, size %s, name: %s", i, users.size(), users.get(i)));
            if (users.get(i).startsWith("User")) {
                users.remove(i);
            }
        }
        System.out.println(users);
    }

    //Skuteczniejszą formą usuwania elementów z listy jest użycie streamu i przypisanie filtrowanych wartości do nowej listy
    private void removeWithStream() {
        List<String> users = new ArrayList<>(
                Arrays.asList("User", "User1", "User2", "User3", "User4")
        );
        List<String> usersFiltered = users.stream()
                .filter(name -> name.startsWith("Kacper"))
                .collect(Collectors.toList());
        System.out.println(String.format("Pozostali: %s", usersFiltered));
    }

}
