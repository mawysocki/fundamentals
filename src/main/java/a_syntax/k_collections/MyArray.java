package a_syntax.k_collections;

public class MyArray {
    //Metoda zwraca tabliczkę mnożenie w postaci dwuwymiarowej tablicy typu int o rozmiarze 10x10
    //Aby przejść przez 2 wymiary należy użyć podwójnej pętli
    private int[][] getMultiplyTable() {
        int[][] table = new int[10][10];
        for (int x = 1; x <= table.length; x++) {
            for (int y = 1; y <= table.length; y++) {
                table[x-1][y-1] = x*y;
            }
        }
        return table;
    }

    public int multiplyNumbers(int x, int y) {
        return getMultiplyTable()[x-1][y-1];
    }

    public void displayResult(int x, int y) {
        System.out.println(String.format("%s * %s = %s", x, y, multiplyNumbers(x,y)));
    }
}
