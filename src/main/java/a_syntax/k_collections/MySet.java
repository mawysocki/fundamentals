package a_syntax.k_collections;

import java.util.HashSet;
import java.util.Set;

public class MySet {
    public void createSet() {
        //Set jest podobne do listy strukturą ale nie daje gwarancji kolejności według dodawanych elementów
        Set<String> set = new HashSet<>();
        set.add("Marian");
        set.add("Karolina");
        set.add("Rafał");



        for (String s : set) {
            System.out.println(s);
        }

        //Znajduje pierwszy element set
        System.out.println(set.iterator().next());
    }
}
