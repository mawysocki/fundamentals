package a_syntax.aa_input;

import java.util.Scanner;

public class Runner {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Jak masz na imie?");
        //Pobieramy wartośc z konsoli
        String name = s.nextLine();
        System.out.println("Ile masz lat " + name + "?");
        //Dane z konsoli, które mają być liczbami pobieramy za pomocą metody nextInt()
        //Problemem tej metody jest to, że nie ogarnia entera
        //To powoduje, że kolejne zczytywanie z konsoli (nazwisko) zostanie pominięte

        //int age = s.nextInt();

        //Możemy użyć metody takiej samej jak dla tekstu tylko przekonwertować wartość za pomocą statycznej metody valueOf z klasy Integer
        int age = Integer.valueOf(s.nextLine());

        System.out.println("Jakie jest Twoje nazwisko?");
        String lastName = s.nextLine(); // Metoda przyjmie enter jeśli wcześniej użyjemy nextInt
        //Łączenie tekstu ze zmienną badź innym tekstem nazywamy konkatenacją
        System.out.println("Witaj " + name + "! Twoje nazwisko to " + lastName +"! Milo Cie poznac mimo " + age + " lat!");
        //Drugi sposób wplatania zmiennej w tekst
        String text = String.format("Witaj %s! Twoje nazwisko to %s! Milo Cie poznac mimo %s lat!", name, lastName,age);
        System.out.println(text);
        //Pozwala zrobić formatowanie od razu w metodzie print ale nie daje ona złamania linii,
        //dlatego na końcu zdania trzeba dodać ręcznie "\n"
        System.out.printf("Witaj %s! Twoje nazwisko to %s! Milo Cie poznac mimo %s lat!\n", name, lastName,age);

        inputTypes();
    }

    public static void inputTypes() {
        System.out.println("_________");
        System.out.println("Input types");
        Scanner scan = new Scanner(System.in);
        int i = scan.nextInt();
        double d = scan.nextDouble();
        scan.hasNextLine();
        String s = scan.nextLine();
        scan.close();
        System.out.println("String: " + s);
        System.out.println("Double: " + d);
        System.out.println("Int: " + i);
    }
}
