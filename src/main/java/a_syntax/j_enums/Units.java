package a_syntax.j_enums;

public enum Units {

    KG("KILOGRAM"),
    G("GRAM"),
    L("LITR"),
    M("METR"),
    Hz("HERC");

    private final String name;

    Units(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
