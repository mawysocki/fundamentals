package a_syntax.j_enums;

public class Runner {
    public static void main(String[] args) {
        displayUnit(Units.G);
        displayUnit(Units.Hz);
        displayUnit(Units.KG);
        displayUnit(Units.L);
        displayUnit(Units.M);

        displayAllUnits();
    }

    public static void displayUnit(Units unit){
        System.out.println("Używana jednostka: " + unit.getName());
    }

    public static void displayAllUnits() {
        for(Units unit: Units.values()) {
            System.out.println(unit);
            System.out.println(unit.getName());
        }
    }

}
