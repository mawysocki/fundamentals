package a_syntax.z_stream;

import java.util.Arrays;

public class StringStream {

    public void capitalizeSentenceWithStream(String sentence) {
        //Metoda stream przyjmuje tablicę Stringów która otrzymujemy po wywołaniu "split()"
        //Mapowanie bierze każdy z elementów tablicy i zmienia wyraz z wielkiej litery
        //foreach przechodzi po wszystkich gotowych elementach i je wyświetla
        Arrays.stream(sentence.split(" ")).map(e -> e.substring(0, 1).toUpperCase() + e.substring(1).toLowerCase() + " ").forEach(System.out::print);
    }
}
