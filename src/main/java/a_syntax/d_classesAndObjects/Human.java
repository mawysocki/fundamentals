package a_syntax.d_classesAndObjects;

public class Human {

    //Human ma 4 pola różnego typu, który charakteryzuje tę klasę.
    //Imie, nazwisko, wiek i samochód.
    //Jest to uproszczona reprezentacja człowieka i jego danych o nim.
    //Na poziomie klasy, wiemy jakiego typu dane może zawierać ale nie jakie są faktycznie
    //Klasa to jedynie szkic na bazie którego tworzą się realne rzeczy (obiekty)
    private String name;
    private String lastName;
    private int age;
    private Car car;

    //W konstruktorze przyjmowane są argumenty niezbędne do zidentyfikowania danej osoby.
    //Występuje w dwóch formach, ponieważ posiadanie samochodu nie jest obowiązkowe do istnienia osoby.
    //Osoba istniejąc już może kupić sobie samochód w każdej chwili. Imię, Nazwisko i wiek ma od początku
    public Human(String name, String lastName, int age, Car car) {
        //Nazwy parametrów konstruktora mogą być takie same jak nazwy pól klasy
        //Do przypisywanych pól trzeba użyć słówka "this", które oznacza, że odpytujemy tę klase w której jesteśmy
        //co oznacza tutaj, że pytamy pola tej klasy.
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.car = car;
    }

    //Druga wersja konstruktora, która nie wymaga podania samochodu
    public Human(String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public Car getCar() {
        return car;
    }

    //Metoda przedstawiania sie osoby powoduje wyświetlanie się wszystkich pól związanych z osobą
    //If weryfikuje czy ktoś posiada samochód.
    //Jeśli ktoś nie ma to oznacza, że zmienna "car" jest pusta (null) i nic nie przechowuje
    public void introduceYourself() {
        System.out.println(String.format("Nazywam się %s %s\ni mam %s lat", this.name, this.lastName, this.age));
        if (car != null) {
            System.out.println("Mam auto marki " + this.car.getBrand());
            System.out.println("Wyprodukowane je w " + this.car.getProductionYear());
            System.out.println("Jego pojemność silnika to " + this.car.getEngineCapacity());
            System.out.println("Aktualny przebieg to: " + this.car.getDistance());
        } else {
            System.out.println("Niestety nie posiadam samochodu");
        }
        System.out.println("___________________________");
    }

    //Metoda pozwala przypisać nowy samochód.
    //Przyjmuje te same argumenty jak konstruktor klasy Car
    //Obiekt tworzony jest wewnątrz metody.
    //Jeśli ktoś nie miał samochodu to po wywołaniu tej metody już będzie miał
    //Jeśli już ktoś miał samochód to zostanie on nadpisany i ma teraz nowy
    public void buyNewCar(String brand, int productionYear, double engineCapacity, long distance) {
        this.car = new Car(brand, productionYear, engineCapacity, distance);
    }

    //Druga wersja tej samej metody (PRZECIĄŻĘNIE METODY! [Overloading])
    //Jako argument wymaga całego obiektu typu Car, który musi zostać stworzony w innym miejscu
    public void buyNewCar(Car newCar) {
        this.car = newCar;
    }
}
