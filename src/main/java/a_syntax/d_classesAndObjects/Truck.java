package a_syntax.d_classesAndObjects;

//Klasa może dziedziczyć po innej klasie przejmując jej cechy (pola i metody)
//Cechy te widoczne są w klasie potomnej jeśli nie są prywatne
//Klasa rozszerza inna klasę za pomocą "extends"
public class Truck extends Car{
    //Klasa ta posiada w sumie 5 pól. 1 własną i 4 po klasie bazowej Car
    //Pola klasy Car nie są widoczne dla klasy Truck bo są prywatne
    //To nie przeszkadza w użyciu tych pól, gdyż gettery dla nich są publiczne
    //Co za tym idzie, metody te są dostępny z poziomu klasy Truck
    private int loadingMeters;
    public Truck(String brand, int productionYear, double engineCapacity, long distance, int loadingMeters) {
        //"super" jest to wywołanie konstruktora z klasy bazowej Car
        super(brand, productionYear, engineCapacity, distance);
        //super.getBrand() - pozwala odnieść sie do metody z klasy bazowej
        this.loadingMeters = loadingMeters;
        //this.getBrand() - możliwe jest to równiez przez użycie this ponieważ te metody zostały odziedziczone
    }


    public int getLoadingMeters() {
        return loadingMeters;
    }

}
