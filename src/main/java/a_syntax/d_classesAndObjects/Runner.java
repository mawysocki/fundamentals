package a_syntax.d_classesAndObjects;

public class Runner {
    public static void main(String[] args) {
        Car toyota = new Car("Toyota", 1999, 1.6, 390000);
        Car audi = new Car("Audi", 2005, 1.9, 285000);

        Human dorota = new Human("Dorota", "Kowalska", 26, toyota);
        Human marek = new Human("Marek", "Nowak", 31, audi);
        Human dominik = new Human("Dominik", "Krawczyk", 19);

        dorota.introduceYourself();
        marek.introduceYourself();
        dominik.introduceYourself();

        System.out.println("#########");
        System.out.println("Zakupy");
        System.out.println("#########");

        //Marek kupuje sobie nowy samochód
        //Wykorzystana jest metoda w wersji pobierającej 4 pola
        marek.buyNewCar("Porshe", 2016, 3.0, 45000);
        //Dominik kupuje samochód od Doroty
        dominik.buyNewCar(dorota.getCar());
        //Dorota przestaje mieć samochód, więc jej pole "car" musi stać się null
        //Dzięki wersji metody "buyNewCar" przyjmującej cały obiekt, możemy również przekazać "null"
        dorota.buyNewCar(null);

        dorota.introduceYourself();
        marek.introduceYourself();
        dominik.introduceYourself();

        //Obiekt klasy potomnej może zostać przypisany do zmiennej typu klasy bazowej
        Car star = new Truck("Star", 1999, 2.0, 415000, 100);
        //Metoda oczekująca obiektu klasy bazowej Car może przyjąć również klasę pochodną Truck
        Human mietek = new Human("Mieczysław", "Żołądek", 45, star);
        mietek.introduceYourself();



    }
}
