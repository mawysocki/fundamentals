package a_syntax.d_classesAndObjects;

public class Car {
    private String brand;
    private int productionYear;
    private double engineCapacity;
    private long distance;

    public Car(String brand, int productionYear, double engineCapacity, long distance) {
        this.brand = brand;
        this.productionYear = productionYear;
        this.engineCapacity = engineCapacity;
        this.distance = distance;
    }

    //Klasa zawiera same gettery i żadnego settera
    //To powoduje, że wszystkie prywatne pola nie będą mogły zmienić swojej wartości
    public String getBrand() {
        return brand;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public double getEngineCapacity() {
        return engineCapacity;
    }

    public long getDistance() {
        return distance;
    }
}