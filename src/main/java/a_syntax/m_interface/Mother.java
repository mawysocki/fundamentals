package a_syntax.m_interface;

public interface Mother {

    void motherMethod();

    public static void motherStaticMethod() {};

    int MOTHER_VALUE = 100;
}
