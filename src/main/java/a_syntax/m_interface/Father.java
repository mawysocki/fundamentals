package a_syntax.m_interface;

public interface Father {

    void fatherMethod();

    static void fatherStaticMethod() {};


    int FATHER_VALUE = 200;
}
