package a_syntax.m_interface;

public class Runner {
    public static void main(String[] args) {
        //Nowo tworzony obiekt można przypisać do zmiennej o kilku typach:
        //Typ jego własnej klasy (object1)
        //Typ interfejsu, który implementuje dana klasa (object2)
        //Typ interfejsu po, którym dziedziczy implementowany interfejs (object4, object5)
        //Wszystkie użycia "new" muszą dotyczeć klasy. Nie można stworzyc obiektu interfejsu (object2)
        MyClass object1 = new MyClass();
        //MyInterface object2 = new MyInterface(); //Nie można stworzyć obiektu z interfejsu
        MyInterface object3 = new MyClass();
        Mother object4 = new MyClass();
        Father object5 = new MyClass();

        //Obiekt przypisany do zmiennej o typie klasy widzi wszystkie możliwe metody
        object1.interfaceMethod();
        object1.defaultMethod();
        object1.motherMethod();
        object1.fatherMethod();
        object1.ownMethod();

        //Obiekt klasy MyClass przypisany do zmiennej typu MyInterface pozwala wywołać
        //wszystkie metody zaimplementowane w interfejsie
        object3.interfaceMethod();
        object3.defaultMethod();
        object3.motherMethod();
        object3.fatherMethod();
        //Metoda "ownMethod" jest własnościową metodą klasy, dlatego nie jest widoczna przez interfejs
//        object3.ownMethod();

        //Obiekt przypisany do interfejsu klasy bazowej ma możliwość wywołania tylko metod z tego interfejsu
        //Wszystkie inne metody sa niedostępne
        object4.motherMethod();
        object5.fatherMethod();

        //Interfejs może zawierać metodę statyczną
        MyInterface.staticMethod();
//        MyInterface.motherStaticMethod();
        Mother.motherStaticMethod();
        Father.fatherStaticMethod();
        

        //Pola z interfejsów dostępne są bezpośrednio z nazwy interfejsu
        //Interfejs potomny widzi wszystkie pola interfejsów bazowych
        //Sa one domyślnie publiczne oraz oznaczone jako "final"
        System.out.println(MyInterface.VALUE);
        System.out.println(MyInterface.MOTHER_VALUE);
        System.out.println(MyInterface.FATHER_VALUE);
//        MyInterface.VALUE = 15;  Nie można przypisać nowej wartości bo jest final

        //"instanceof" sprawcza czy dany obiekt jest instancją danej klasy bądź interfejsu
        //Tutaj możemy zaobserwować, że wszystkie odpowiedzi są prawidłowe
        System.out.println(object1 instanceof MyClass);
        System.out.println(object1 instanceof MyInterface);
        System.out.println(object1 instanceof Mother);
        System.out.println(object1 instanceof Father);

        System.out.println(object3 instanceof MyClass);
        System.out.println(object3 instanceof MyInterface);
        System.out.println(object3 instanceof Mother);
        System.out.println(object3 instanceof Father);

        System.out.println(object4 instanceof MyClass);
        System.out.println(object4 instanceof MyInterface);
        System.out.println(object4 instanceof Mother);
        System.out.println(object4 instanceof Father);

    }
}
