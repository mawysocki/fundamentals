package a_syntax.m_interface;

//Użycie słowa kluczowego "implements" dla MyInterfejs oznacza implementowanie tego interfejsu w tej klasie
//Wymaga zaimplementowania metod z tego interfejsu
//InteliJ automatycznie to podpowiada gdy tylko zrobimy "implements" bez dodania metod
//Każda metoda ma swoją własną wersję w każdej klasie implementującej dany interfejs
public class MyClass implements MyInterface{

    @Override
    public void fatherMethod() {

    }

    @Override
    public void motherMethod() {

    }


    @Override
    public void interfaceMethod() {

    }

    //Metoda domyślna może zostać przesłonieta w ten sam sposób
    //InteliJ automatycznie wygenerował implementacje w postaci wywołania oryginalnej metody domyślnej stworzonej w interfejsie
    @Override
    public void defaultMethod() {
        MyInterface.super.defaultMethod();
    }

    //Jest to zwykła metoda dla danej klasy niezwiązana z interfejsem, dlatego nie ma @Override
    public void ownMethod() {

    }

}
