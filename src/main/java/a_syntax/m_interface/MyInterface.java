package a_syntax.m_interface;

//Interfejs w przeciwieństwie do klasy może dziedziczyć po dwóch interfejsach równocześnie
public interface MyInterface extends Mother, Father{

    //Metoda w interfejsie jest domyślnie publiczna
    //Nie ma ona swojej implementacji, a jedynie sygnaturę
    void interfaceMethod();

    //Metoda domyślna ma swoją implementację już wewnątrz interfejsu
    //W klasie implementującej można nadpisać jej ciało
    default void defaultMethod() {
        System.out.println("Domyslna metoda interfejsu");
    }

    //Metoda statyczna jest również automatycznie publiczna
    //Ma ona swoje ciało
    static void staticMethod() {System.out.println("Metoda statyczna interfejsu");

    }
    //Interfejsy również mogą posiadać pola
    //Pola te są stałymi z domyślnie użytym słowem "final"
    //Domyślnie są publiczne i nie trzeba używać "public"
    //Domyślnie również są statyczne i nie wymagają "static"
    //Użycie któregś z tych słów kluczowych nie spowoduje błędu ale jest uznany za nadmiarowy
    //Pole z racji statyczności wywołujemy bezpośrednio z klasy, a nie z obiektu
    public final static int VALUE = 350;

    //Od Javy 9 Interfacy mogą posiadać metody prywatne
    private void privateMethod() {

    }

}
