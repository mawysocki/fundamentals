package a_syntax.r_format;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyDate {
    public void findDay(int month, int day, int year)  {
        String inputDate = String.format("%s/%s/%s", day, month, year);
        SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
        Date dt1;
        try {
            dt1 = format1.parse(inputDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        DateFormat format2 = new SimpleDateFormat("EEEE");
        System.out.println(format2.format(dt1).toUpperCase());
    }
}
