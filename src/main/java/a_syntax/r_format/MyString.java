package a_syntax.r_format;

public class MyString {

    public void capitalizeWord(String word) {
        System.out.println(word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase());
    }

    public void capitalizeWords(String sentence) {
        for (String x : sentence.split(" ")) {
            System.out.print(x.substring(0, 1).toUpperCase() + x.substring(1).toLowerCase() + " ");
        }

    }
}
