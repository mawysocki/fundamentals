package a_syntax.r_format;

public class Runner {
    public static void main(String[] args) {
        MyCurrency myCurrency = new MyCurrency();
        MyDate myDate = new MyDate();
        MyString myString = new MyString();

        myCurrency.displayCurrency(123.456);
        myDate.findDay(6,9,2022);

        myString.capitalizeWord("zdanie Z WIELKIEJ liTeRY");
        myString.capitalizeWords("powiększ każdy wyraz z tego zdania");
    }
}
