package a_syntax.i_inheritance;

public class ChildB extends Parent {
    @Override
    public void familyMethod() {
        System.out.println("Metoda wywołana z klasy ChildB");
    }
}
