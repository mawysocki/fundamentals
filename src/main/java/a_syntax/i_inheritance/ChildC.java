package a_syntax.i_inheritance;

public class ChildC extends Parent{
    @Override
    public void familyMethod() {
        super.familyMethod();
        System.out.println("Metoda wywołana z klasy ChildC");
    }
}
