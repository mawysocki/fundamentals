package a_syntax.i_inheritance;

public class Runner {
    public static void main(String[] args) {
        Parent parent = new Parent();
        parent.familyMethod();
        ChildA childA1 = new ChildA();
        childA1.familyMethod();
        ChildB childB1 = new ChildB();
        childB1.familyMethod();
        ChildC childC1 = new ChildC();
        childC1.familyMethod();
        System.out.println("____________________");
        Parent[] children = new Parent[3];
        children[0] = new ChildA();
        children[1] = new ChildB();
        children[2] = new ChildC();
        for (Parent child : children) {
            child.familyMethod();
        }

    }
}
