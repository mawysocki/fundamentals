package a_syntax.i_inheritance;

public class ChildA extends Parent{

    @Override
    public void familyMethod() {
        System.out.println("Metoda wywołana z klasy ChildA");
    }
}
