package a_syntax.zz_reflection;

//Klasa posiada pole prywatne z okresloną wartością
//oraz getter, który pozwala pobrać wartość ale nie zmienic
public class MyClass {

    private int value = 100;

    public int getValue() {
        return value;
    }
}
