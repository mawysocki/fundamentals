package a_syntax.zz_reflection;

import java.lang.reflect.Field;

public class Runner {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        MyClass object = new MyClass();
        //Obiekt wywołuje getter wyświetlając wartość pola "value
        System.out.println(object.getValue());
//        object.value = 50; //Pole jest prywatne i nie posiada settera, więc nie można zmienic mu wartości

        //Za pomocą refleksji mamy dostęp do pola tej klasy
        Field value = MyClass.class.getDeclaredField("value");
        //Możemy sztucznie zmienić jej dostępność i ustawić wartość
        value.setAccessible(true);
        value.set(object, 200);
        System.out.println(object.getValue());
    }
}
