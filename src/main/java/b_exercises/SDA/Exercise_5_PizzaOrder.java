package b_exercises.SDA;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class Exercise_5_PizzaOrder {

    private static int orderPizzaMap(String[] order) {
        HashMap<String, Integer> menu = createMenu();
        int total = 0;
        for (String size : order) {
//            total = total + menu.get(size);
            total += menu.get(size);
        }
        return total;
    }
    private static int orderPizzaSwitch(String[] order) {
        HashMap<String, Integer> menu = createMenu();
        int total = 0;
        for (String size : order) {
            total += getPrice(size);
        }
        return total;
    }

    private static HashMap<String, Integer> createMenu() {
        HashMap<String, Integer> menu = new HashMap<>();
        menu.put("S", 25);
        menu.put("M", 30);
        menu.put("L", 35);
        menu.put("XL", 40);
        return menu;
    }

    private static int getPrice(String size) {
        int price = 0;
        switch (size) {
            case "S": price = 25;
                break;
            case "M": price = 30;
                break;
            case "L": price = 35;
                break;
            case "XL": price = 40;
                break;
            default: System.out.println("Nie ma takiego rozmiaru");

        }
        return price;
    }

    @Test
    public void test1() {
        String[] myOrder = {"S", "L", "L", "XL"};
        int total = orderPizzaMap(myOrder);
        Assert.assertEquals(total, 135);
    }

    @Test
    public void test2() {
        String[] myOrder = {"S", "S", "XL", "XL", "L"};
        int total = orderPizzaMap(myOrder);
        Assert.assertEquals(total, 165);
    }
}
