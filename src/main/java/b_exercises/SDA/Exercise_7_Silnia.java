package b_exercises.SDA;

import org.junit.Assert;
import org.junit.Test;

public class Exercise_7_Silnia {

    private int silniaA(int n) {
        int result = 1;
        if (n == 0 || n == 1) return result;

        for (int i = 2; i <= n; i++) {
            result *=i;
        }
        return result;
    }

    private int silniaB(int n) {
        int result = 1;
        if (n>0) {
            result=n*silniaB(n-1);
        }
        return result;
    }

    @Test
    public void testA1() {
        int n = 5;
        int expected = 120;
        Assert.assertEquals(expected, silniaA(n));
    }

    @Test
    public void testA2() {
        int n = 6;
        int expected = 720;
        Assert.assertEquals(expected, silniaA(n));
    }

    @Test
    public void testB1() {
        int n = 5;
        int expected = 120;
        Assert.assertEquals(expected, silniaB(n));
    }

    @Test
    public void testB2() {
        int n = 6;
        int expected = 720;
        Assert.assertEquals(expected, silniaB(n));
    }

    @Test
    public void testB3() {
        int n = 1;
        int expected = 1;
        Assert.assertEquals(expected, silniaB(n));
    }

    @Test
    public void testB4() {
        int n = 0;
        int expected = 1;
        Assert.assertEquals(expected, silniaB(n));
    }

}
