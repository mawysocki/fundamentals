package b_exercises.SDA;

import java.util.Scanner;

public class Exercise_1_MathOperations {
    public static void main(String[] args) {
        System.out.println("Podaj dwie liczby");
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();
        int y = s.nextInt();
        System.out.println("Suma x oraz y wynosi: " + (x+y));
        System.out.println("Różnica x oraz y wynosi: " + (x-y));
        System.out.println("Iloczyn x oraz y wynosi: " + (x*y));
        if (y != 0) {
            System.out.println("Dzielenie x przez y wynosi: " + (x/y));
        } else {
            System.out.println("Nie mozna dzielić przez 0");
        }

    }
}
