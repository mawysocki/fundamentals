package b_exercises.SDA;

import java.util.Random;
import java.util.Scanner;

public class Exercise_3_GuessingGame {
    public static void main(String[] args) {
        guessNumber(100);
    }

    public static void guessNumber(int max) {
        Random r = new Random();
        Scanner s = new Scanner(System.in);
        int counter = 1;
        int x = 0;
        int randomNumber = r.nextInt(max);
        System.out.println("Zgadnij liczbę");
        do {
            x = s.nextInt();
            if (randomNumber == x) {
                System.out.println(String.format("Gratulacje, trafiłeś po %s próbach", counter));
            } else {
                if (randomNumber > x) {
                    System.out.println("Szukana przez Ciebie liczba jest większa");
                } else {
                    System.out.println("Szukana liczba jest mniejsza od twojej");
                }
            }
            counter++;
        }
        while (randomNumber != x);
    }
}
