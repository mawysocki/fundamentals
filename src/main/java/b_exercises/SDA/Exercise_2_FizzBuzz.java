package b_exercises.SDA;

import org.junit.Assert;
import org.junit.Test;

public class Exercise_2_FizzBuzz {
    public static void main(String[] args) {
        int number = 15;
        String result = "";
        if (number % 3 == 0) result += "fizz";
        if (number % 5 == 0) result += "buzz";
        System.out.println(result);
    }

    public static String fizzBuzz(int number) {
        String result = "";
        if (number % 3 == 0) result += "fizz";
        if (number % 5 == 0) result += "buzz";
        return result;
    }


    @Test
    public void test1() {
        String text = fizzBuzz(6);
        Assert.assertEquals(text, "fizz");
    }

    @Test
    public void test2() {
        String text = fizzBuzz(20);
        Assert.assertEquals(text, "buzz");
    }

    @Test
    public void test3() {
        String text = fizzBuzz(15);
        Assert.assertEquals(text, "fizzbuzz");
    }

    @Test
    public void test4() {
        String text = fizzBuzz(11);
        Assert.assertEquals(text, "");
    }
}
