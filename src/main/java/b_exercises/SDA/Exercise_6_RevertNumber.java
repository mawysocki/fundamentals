package b_exercises.SDA;

import org.junit.Assert;
import org.junit.Test;

public class Exercise_6_RevertNumber {
    //Napisz program który pobiera liczbę całkowitą od użytkownika i zwraca liczbę odwrotną


    public int reverseNumber(int number) {  //597
        int dividedNumber = number;  //597
        int newNumber = 0;

        while (dividedNumber != 0) {
            newNumber *= 10; //0 70 790
            int modulo = dividedNumber % 10;  //7   9  5
            newNumber += modulo;  //7 79  795
            dividedNumber /= 10; //59 5  0
        }
        return newNumber;
    }

    @Test
    public void test1() {
        int input = 597;
        int expectedNumber = 795;
        Assert.assertEquals(expectedNumber, reverseNumber(input));
    }

    @Test
    public void test2() {
        int input = 5;
        int expectedNumber = 5;
        Assert.assertEquals(expectedNumber, reverseNumber(input));
    }

    @Test
    public void test3() {
        int input = 7432223;
        int expectedNumber = 3222347;
        Assert.assertEquals(expectedNumber, reverseNumber(input));
    }
}
