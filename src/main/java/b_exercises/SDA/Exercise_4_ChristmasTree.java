package b_exercises.SDA;

public class Exercise_4_ChristmasTree {
    public static void main(String[] args) {
        int height = 10;
        christmasTreeReversed(height);
        christmasTree(height);
    }

    public static void christmasTree(int height) {
        for (int x = 1; x <= height; x++) {
            for (int y = 1; y <=height-x ; y++) {
                System.out.print(" ");
            }
            for (int y = 1; y <= 2 * x - 1; y++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void christmasTreeReversed(int height) {
        for (int x = height; x >= 1; x--) {
            for (int y = 1; y <=height-x ; y++) {
                System.out.print(" ");
            }
            for (int y = 1; y <= 2 * x - 1; y++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
