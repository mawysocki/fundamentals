package b_exercises.HR.a_medium;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Mamy macierz nxn składającą się z liczb od 1 do n^2.
//Aby osiągnać magiczny kwadrat to:
//1) Każda z liczb musi być unikatowa
//2) Suma wierszy, kolumn oraz diagonal musi być zawsze taka sama
//Liczby można przekształcać z "x" -> "y". Koszt takiego przekształcenia to |x - y|
//Kwadrat po przekształceniu musi spełniać warunki magicznego kwadratu
//Wyznacz minimalny koszt przekształcenia macierzy w magiczny kwadrat
public class Exercise_MagicSquare {
    final int magicSum = 15;
    private boolean checkRowAndColumns(List<List<Integer>> matrix) {
        boolean isColumn = true;
        boolean isRow = true;
        for (int i = 0; i < matrix.size(); i++) {
            int colSum = 0;
            int rowSum = 0;
            for (int j = 0; j < matrix.size(); j++) {
                rowSum += matrix.get(i).get(j);
                colSum += matrix.get(j).get(i);
            }
            if (rowSum != magicSum || colSum != magicSum) {
                System.out.println(String.format("Row sum: %s, Col sum %s, i:%s", colSum, rowSum, i));
                return false;
            }
        }
        return isColumn && isRow;
    }

    private int buildMagicSquare(List<List<Integer>> matrix) {
        int cost = 0;
        checkRowAndColumns(matrix);
        return cost;
    }

    @Test
    public void test1() {
        List<Integer> line1 = new ArrayList<>(Arrays.asList(4, 9, 2));
        List<Integer> line2 = new ArrayList<>(Arrays.asList(3, 5, 7));
        List<Integer> line3 = new ArrayList<>(Arrays.asList(8, 1, 5));
        List<List<Integer>> matrix = new ArrayList<>(Arrays.asList(line1, line2, line3));
        int expectedOutput = 1;
        Assert.assertEquals(expectedOutput, buildMagicSquare(matrix));
    }

    @Test
    public void test2() {
        List<Integer> line1 = new ArrayList<>(Arrays.asList(4, 8, 2));
        List<Integer> line2 = new ArrayList<>(Arrays.asList(4, 5, 7));
        List<Integer> line3 = new ArrayList<>(Arrays.asList(6, 1, 6));
        List<List<Integer>> matrix = new ArrayList<>(Arrays.asList(line1, line2, line3));
        int expectedOutput = 4;
        Assert.assertEquals(expectedOutput, buildMagicSquare(matrix));
    }

}
