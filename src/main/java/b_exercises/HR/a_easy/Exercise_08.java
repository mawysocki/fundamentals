package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

//Zmień tekst tak aby powtarzające się litery były na początku i tyle razy ile występują a niepowtarzajace się na końcu
public class Exercise_08 {

    public  String convertString(String input) {
        char[] chars = input.toCharArray();
        List<Character> usedCharacters = new ArrayList<>();
        //LinkedHashMap zachowuje kolejność wstawiania elementów
        Map<Character, Integer> noOfUsage = new LinkedHashMap<>();

        for (char x: chars) {
            if (!usedCharacters.contains(x)) {
                usedCharacters.add(x);
                int counter = 0;
                //Zlicza ile razy dana litera pojawiła się w zbiorze
                for (char y : chars) {
                    if (x == y) {
                        counter++;
                    }
                }
                noOfUsage.put(x, counter);
            }
        }

        //Specjalna klasa pozwalająca tworzyć Stringi w bardziej prawilny sposób
        //c.d.n
        StringBuilder output = new StringBuilder();
        for (Character character : usedCharacters) {
            if (noOfUsage.get(character) == 1) { continue;}
            for (int i = 0; i< noOfUsage.get(character); i++) {
                output.append(character);
            }
        }

        for (Character character : usedCharacters) {
            if (noOfUsage.get(character) != 1) { continue;}
            for (int i = 0; i< noOfUsage.get(character); i++) {
                output.append(character);
            }
        }

        return output.toString();
    }

    //3 testy z różnymi przykładami
    @Test
    public void Test1() {
        String input = "vaibhav";
        String expectedoutput = "vvaaibh";
        String output = convertString(input);
        Assert.assertEquals(expectedoutput, output);
    }

    @Test
    public void Test2() {
        String input = "vxaibhav";
        String expectedoutput = "vvaaxibh";
        String output = convertString(input);
        Assert.assertEquals(expectedoutput, output);
    }

    @Test
    public void Test3() {
        String input = "malaga";
        String expectedoutput = "aaamlg";
        String output = convertString(input);
        Assert.assertEquals(expectedoutput, output);
    }
}
