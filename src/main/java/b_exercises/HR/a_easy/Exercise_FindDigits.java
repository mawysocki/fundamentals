package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Mając podaną liczbę N policz ile cyfr wchodzących w skład tej liczby jest jej dzielnikami
public class Exercise_FindDigits {

    private int findDigit(int number) {
        int dividedNumber = number;
        int rest = 0;
        int count = 0;
        while (dividedNumber != 0) {
            rest = dividedNumber % 10;
            if (rest == 0) {
                dividedNumber /= 10;
                continue;
            }
            if (number % rest == 0) {
                count++;
            }
            dividedNumber /= 10;
        }
        return count;
    }

    @Test
    public void test1() {
        int number = 12;
        int expectedResult = 2;
        Assert.assertEquals(expectedResult, findDigit(number));
    }

    @Test
    public void test2() {
        int number = 1012;
        int expectedResult = 3;
        Assert.assertEquals(expectedResult, findDigit(number));
    }

}
