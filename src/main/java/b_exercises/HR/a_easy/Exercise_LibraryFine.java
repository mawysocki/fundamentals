package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Bibliotek nalicza karę według reguł.
//1) Gdy książkę przyniesiesz o czasie bądź wczesniej to płacisz 0
//2) Gdy się spóźnisz ale oddasz w tym samym miesiacu to płacisz 15*różnicaDni
//3) Gdy się spóźnisz ale oddasz w tym samym roku to płacisz 500*różnicaMiesięcy
//3) Gdy się spóźnisz i oddasz w kolejnym roku to płacisz 10000
//Nie ma znaczenia czy miałeś oddać 1.01 czy 31.12 roku poprzedniego. Kara nalicza się tak samo
//Napisz algorytm, który to realizuje
public class Exercise_LibraryFine {


    public int returnBook(int returnedDay, int returnedMonth, int returnedYear, int dueDay, int dueMonth, int dueYear) {
        if (returnedYear > dueYear) {
            return yearPenalty();
        }
        if (returnedYear == dueYear) {
            if (returnedMonth > dueMonth) {
                return monthPenalty(returnedMonth, dueMonth);
            }
            if (returnedMonth == dueMonth && returnedDay > dueDay) {
                    return dayPenalty(returnedDay, dueDay);
            }
        }
        return 0;
    }

    private int yearPenalty() {
        return 10000;
    }

    private int monthPenalty(int returnedMonth, int dueMonth) {
        return 500 * (returnedMonth - dueMonth);
    }

    private int dayPenalty(int returnedDay, int dueDay) {
        return 15 * (returnedDay - dueDay);
    }

    @Test
    public void test1() {
        int returnedDay = 9;
        int returnedMonth = 6;
        int returnedYear = 2015;
        int dueDay = 6;
        int dueMonth = 6;
        int dueYear = 2015;
        int expectedPenalty = 45;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test2() {
        int returnedDay = 14;
        int returnedMonth = 7;
        int returnedYear = 2018;
        int dueDay = 5;
        int dueMonth = 7;
        int dueYear = 2018;
        int expectedPenalty = 135;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test3() {
        int returnedDay = 14;
        int returnedMonth = 7;
        int returnedYear = 2018;
        int dueDay = 25;
        int dueMonth = 7;
        int dueYear = 2018;
        int expectedPenalty = 0;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test4() {
        int returnedDay = 14;
        int returnedMonth = 7;
        int returnedYear = 2019;
        int dueDay = 25;
        int dueMonth = 7;
        int dueYear = 2018;
        int expectedPenalty = 10000;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test5() {
        int returnedDay = 31;
        int returnedMonth = 12;
        int returnedYear = 2022;
        int dueDay = 31;
        int dueMonth = 12;
        int dueYear = 2022;
        int expectedPenalty = 0;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test6() {
        int returnedDay = 1;
        int returnedMonth = 1;
        int returnedYear = 2015;
        int dueDay = 1;
        int dueMonth = 1;
        int dueYear = 2016;
        int expectedPenalty = 0;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test7() {
        int returnedDay = 1;
        int returnedMonth = 1;
        int returnedYear = 2016;
        int dueDay = 1;
        int dueMonth = 1;
        int dueYear = 2016;
        int expectedPenalty = 0;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test8() {
        int returnedDay = 1;
        int returnedMonth = 1;
        int returnedYear = 2018;
        int dueDay = 1;
        int dueMonth = 1;
        int dueYear = 2016;
        int expectedPenalty = 10000;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test9() {
        int returnedDay = 31;
        int returnedMonth = 1;
        int returnedYear = 2018;
        int dueDay = 1;
        int dueMonth = 1;
        int dueYear = 2018;
        int expectedPenalty = 450;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test10() {
        int returnedDay = 1;
        int returnedMonth = 2;
        int returnedYear = 2018;
        int dueDay = 1;
        int dueMonth = 1;
        int dueYear = 2018;
        int expectedPenalty = 500;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test11() {
        int returnedDay = 31;
        int returnedMonth = 12;
        int returnedYear = 3000;
        int dueDay = 1;
        int dueMonth = 1;
        int dueYear = 1;
        int expectedPenalty = 10000;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test12() {
        int returnedDay = 31;
        int returnedMonth = 12;
        int returnedYear = 3000;
        int dueDay = 1;
        int dueMonth = 1;
        int dueYear = 3000;
        int expectedPenalty = 5500;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test13() {
        int returnedDay = 31;
        int returnedMonth = 12;
        int returnedYear = 2999;
        int dueDay = 1;
        int dueMonth = 1;
        int dueYear = 3000;
        int expectedPenalty = 0;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

    @Test
    public void test14() {
        int returnedDay = 31;
        int returnedMonth = 1;
        int returnedYear = 1999;
        int dueDay = 1;
        int dueMonth = 2;
        int dueYear = 1999;
        int expectedPenalty = 0;
        Assert.assertEquals(expectedPenalty, returnBook(returnedDay, returnedMonth, returnedYear, dueDay, dueMonth, dueYear));
    }

}
