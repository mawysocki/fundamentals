package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exercise_KaprekarNumbers {

    private boolean isKaprekurNumber(int number) {
        long square = (long) number * number;
        String sSquare = String.valueOf(square);
        int length = sSquare.length() / 2;
        String leftString = sSquare.substring(0, length);
        String rightString = sSquare.substring(length);
        if (leftString.equals("")) {
            return false;
        }
        int leftInt = Integer.parseInt(leftString);
        int rightInt = Integer.parseInt(rightString);
        boolean isKaprekur = leftInt + rightInt == number;
        if (isKaprekur) {
            System.out.println("Liczba Kaprekur: " + number);
            System.out.printf("Kwadrat: %s,  Lewy: %s,  Prawy: %s", number, square, leftString, rightString);

        }

        return isKaprekur;
    }
    private List<Integer> findNumbers(int minRange, int maxRange) {
        List<Integer> result = new ArrayList<>();
        if (minRange <= 1) {
            result.add(1);
        }
        for (int i = minRange; i <= maxRange; i++) {
            if (isKaprekurNumber(i)) {
                result.add(i);
            }
        }
        return result;
    }

    @Test
    public void test1() {
        int minRange = 1;
        int maxRange = 100;
        List<Integer> results = new ArrayList<>(Arrays.asList(1, 9, 45, 55, 99));
        Assert.assertEquals(results, findNumbers(minRange, maxRange));
    }

    @Test
    public void test2() {
        int minRange = 100;
        int maxRange = 200;
        List<Integer> results = new ArrayList<>();
        Assert.assertEquals(results, findNumbers(minRange, maxRange));
    }

    @Test
    public void test3() {
        int minRange = 200;
        int maxRange = 300;
        List<Integer> results = new ArrayList<>(Arrays.asList(297));
        Assert.assertEquals(results, findNumbers(minRange, maxRange));
    }

    @Test
    public void test4() {
        int minRange = 300;
        int maxRange = 1000;
        List<Integer> results = new ArrayList<>(Arrays.asList(703, 999));
        Assert.assertEquals(results, findNumbers(minRange, maxRange));
    }

    @Test
    public void test5() {
        int minRange = 1000;
        int maxRange = 10000;
        List<Integer> results = new ArrayList<>(Arrays.asList(2223, 2728, 4950, 5050, 7272, 7777, 9999));
        Assert.assertEquals(results, findNumbers(minRange, maxRange));
    }

    @Test
    public void test6() {
        int minRange = 10000;
        int maxRange = 99999;
        List<Integer> results = new ArrayList<>(Arrays.asList(17344, 22222, 77778, 82656, 95121, 99999));
        Assert.assertEquals(results, findNumbers(minRange, maxRange));
    }



}
