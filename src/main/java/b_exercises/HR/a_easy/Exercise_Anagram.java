package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

//Mając dwa słowa A oraz B sprawdź czy są swoimi anagramami.
// (czyli czy składają się z tych samych i tej samej liczby liter)
public class Exercise_Anagram {

    private Map<Character, Integer> createMap(String text) {
        char[] chars1 = text.toUpperCase().toCharArray();
        Map<Character, Integer> map = new HashMap<>();
        for (char c : chars1) {
            if (!map.containsKey(c)) {
                map.put(c, 1);
            }
            else {
                map.put(c, map.get(c) + 1);
            }
        }
        return map;
    }
    private boolean isAnagram(String text1, String text2) {
        Map<Character, Integer> map1 = createMap(text1);
        Map<Character, Integer> map2 = createMap(text2);
        return map2.equals(map1);
    }

    @Test
    public void test1() {
        String text1 = "abba";
        String text2 = "baba";
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, isAnagram(text1, text2));
    }

    @Test
    public void test2() {
        String text1 = "anagram";
        String text2 = "margana";
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, isAnagram(text1, text2));
    }

    @Test
    public void test3() {
        String text1 = "xxyyzz";
        String text2 = "zyxzxy";
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, isAnagram(text1, text2));
    }

    @Test
    public void test4() {
        String text1 = "abcc";
        String text2 = "cbaa";
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, isAnagram(text1, text2));
    }


}
