package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Rozpoczynamy zabawę mając 100 punktów energii
//Dostajemy tablicę A z oznaczeniem chmurek czy są one zwykłe czy burzowe
//A = [0,0,1,2]
//Skaczemy po chmurkach w odległości K
//Każdy skok zabiera 1 punkt energii
//Jeśli wskoczymy na chmurkę burzową tracimy dodatkowe 2 punkty energii.
//Gra zaczyna się i kończy na chmurze o indeksie 0
//Policz ile energii zostanie Ci po powrocie na chmurę docelową
public class Exercise_JumpingCloud2 {

    private int jump(List<Integer> clouds, int step) {
        int energy = 100;
        int currentIndex = 0;
        do {
            currentIndex = (currentIndex + step) % clouds.size();
            energy--;
            if (clouds.get(currentIndex) == 1) {
                energy -= 2;
            }
        }
        while (currentIndex != 0 && energy >= 0);
        return energy;
    }

    @Test
    public void test1() {
        List<Integer> clouds = new ArrayList<>(Arrays.asList(0, 0, 1, 0));
        int step = 2;
        int expectedEnergy = 96;
        Assert.assertEquals(expectedEnergy, jump(clouds, step));
    }

    @Test
    public void test2() {
        List<Integer> clouds = new ArrayList<>(Arrays.asList(0, 0, 1, 0, 0, 1, 1, 0));
        int step = 2;
        int expectedEnergy = 92;
        Assert.assertEquals(expectedEnergy, jump(clouds, step));
    }


}
