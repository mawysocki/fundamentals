package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Znając zakres liczb od MIN do MAX sprawdź ile liczb znajduje się wewnątrz które sa kwadratami liczb naturalnych
public class Exercise_Squares {
    private int countSquares(int start, int end) {
        int counter = 0;
        int minSqr = (int)Math.sqrt(start);
        if (minSqr*minSqr == start) {counter++;}
        for (int i = minSqr+1; i <= (int)Math.sqrt(end); i++) {
            counter++;
        }
        return counter;
    }
    @Test
    public void test1() {
        int start = 24;
        int end = 49;
        int expectedResult = 3;
        Assert.assertEquals(expectedResult, countSquares(start, end));
    }

    @Test
    public void test2() {
        int start = 35;
        int end = 70;
        int expectedResult = 3;
        Assert.assertEquals(expectedResult, countSquares(start, end));
    }

    @Test
    public void test3() {
        int start = 100;
        int end = 1000;
        int expectedResult = 22;
        Assert.assertEquals(expectedResult, countSquares(start, end));
    }


}
