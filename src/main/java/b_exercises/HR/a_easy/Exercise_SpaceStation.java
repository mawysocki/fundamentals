package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

//Plany posiadają oznaczenia od 0 do n.
//Niektóre z nich posiadają swoje stacje kosmiczne
//Adresy ich podane są za pomocą tablicy A.
//Znajdź numer planety dla której występuje najdłuższa droga ze wszystkich dla najbliższej jej stacji.
//Każda odległość miedzy stacją x oraz x+1 wynosi 1.
//Przykład:
//Planety: 0,1,2,3,4
//Stacje: [0,4]
//Odpowiedź: 2. Od planety z indeksem 2 mamy odległość do stacji 0 oraz 4 równą 2.
public class Exercise_SpaceStation {

    private int findPaths(int n, List<Integer> stations) {
        HashMap<Integer, Integer> paths = new HashMap<>();
        for (int i = 0; i < n; i++) {
            int localMinPath = Integer.MAX_VALUE;
            for (Integer station : stations) {
                int path = Math.abs(station - i);
                if (path < localMinPath) {
                    localMinPath = path;
                }
                if (localMinPath==0) {
                    break;
                }
            }
            paths.put(i, localMinPath);
        }
        return findMaxDistance(paths);
    }

    private int findMaxDistance(HashMap<Integer, Integer> paths) {
        int max = -1;
        for (Integer integer : paths.values()) {
            if (integer > max) {
                max = integer;
            }
        }
        return max;
    }

    @Test
    public void test1() {
        int n = 5;
        List<Integer> stations = new ArrayList<>(Arrays.asList(0, 4));
        int maxDistance = 2;
        Assert.assertEquals(maxDistance, findPaths(n, stations));
    }

    @Test
    public void test2() {
        int n = 6;
        List<Integer> stations = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5));
        int maxDistance = 0;
        Assert.assertEquals(maxDistance, findPaths(n, stations));
    }

}
