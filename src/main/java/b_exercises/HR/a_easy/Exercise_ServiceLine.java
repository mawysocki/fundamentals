package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Mając podaną listę A szerkości pasa serwisowego poszczególnhych
//Oraz podaną listę B odcinków do weryfikacji,
// podaj jaka jest maksymalna dopuszczalna szerokość auta dla każdego podanego odcinka
//A=[2, 3, 1, 2, 3, 2, 3, 3]
//B=[[0, 3], [4,6], [6,7], [3,5] [0,7]]
//Na odcinku 0-3 najwęższy fragment jest dla A[2] = 1;
//Podaj wynik w postaci listy
public class Exercise_ServiceLine {

    private List<Integer> checkCars(List<Integer> serviceLineWidth, List<List<Integer>> cases) {
        List<Integer> result = new ArrayList<>();
        System.out.println("Szerokości: " + serviceLineWidth);
        for (List<Integer> singleCase : cases) {
            System.out.println("Sprawdzany odcinek: " + singleCase);
            int min = Integer.MAX_VALUE;
            int currentValue;
            for (int i = singleCase.get(0); i <= singleCase.get(1); i++) {
                currentValue = serviceLineWidth.get(i);
                if (currentValue < min) {
                    min = currentValue;
                }
            }
            result.add(min);
        }
        return result;
    }

    @Test
    public void test1() {
        ArrayList<Integer> serviceLineWidth = new ArrayList<>(Arrays.asList(2, 3, 1, 2, 3, 2, 3, 3));
        List<List<Integer>> cases = new ArrayList<>();
        cases.add(new ArrayList<>(Arrays.asList(0, 3)));
        cases.add(new ArrayList<>(Arrays.asList(4, 6)));
        cases.add(new ArrayList<>(Arrays.asList(6, 7)));
        cases.add(new ArrayList<>(Arrays.asList(3, 5)));
        cases.add(new ArrayList<>(Arrays.asList(0, 7)));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(1, 2, 3, 2, 1));
        Assert.assertEquals(expectedResult, checkCars(serviceLineWidth, cases));
    }

    @Test
    public void test2() {
        ArrayList<Integer> serviceLineWidth = new ArrayList<>(Arrays.asList(2, 3, 2, 1));
        List<List<Integer>> cases = new ArrayList<>();
        cases.add(new ArrayList<>(Arrays.asList(0, 1)));
        cases.add(new ArrayList<>(Arrays.asList(1, 3)));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(2, 1));
        Assert.assertEquals(expectedResult, checkCars(serviceLineWidth, cases));
    }

    @Test
    public void test3() {
        ArrayList<Integer> serviceLineWidth = new ArrayList<>(Arrays.asList(1, 2, 2, 2, 1));
        List<List<Integer>> cases = new ArrayList<>();
        cases.add(new ArrayList<>(Arrays.asList(2, 3)));
        cases.add(new ArrayList<>(Arrays.asList(1, 4)));
        cases.add(new ArrayList<>(Arrays.asList(2, 4)));
        cases.add(new ArrayList<>(Arrays.asList(2, 4)));
        cases.add(new ArrayList<>(Arrays.asList(2, 3)));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(2, 1, 1, 1, 2));
        Assert.assertEquals(expectedResult, checkCars(serviceLineWidth, cases));
    }


}
