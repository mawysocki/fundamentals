package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//5 osób dostaje reklamę nowego produktu w pierwszym dniu od jego wydania
//Połowie z nich produkt się podoba floor(5/2)
//Osoby którym się podoba pokazują to swoim 3 znajomym każdy.
//Policz ile osób w sumie polubi produkt po n dni
public class Exercise_Advertisement {

    private int countLikes(int days) {
        int sum = 0;
        int shared = 5;
        for (int i = 1; i <= days; i++) {
            int dayLike = shared/2;
            sum+=dayLike;
            shared= dayLike *3;
        }
        return sum;
    }

    @Test
    public void test0() {
        int days = 1;
        int expectedLikes = 2;
        Assert.assertEquals(expectedLikes, countLikes(days));
    }

    @Test
    public void test2() {
        int days = 3;
        int expectedLikes = 9;
        Assert.assertEquals(expectedLikes, countLikes(days));
    }

    @Test
    public void test3() {
        int days = 5;
        int expectedLikes = 24;
        Assert.assertEquals(expectedLikes, countLikes(days));
    }


}
