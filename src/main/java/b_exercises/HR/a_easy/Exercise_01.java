package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Mając tablicę liczb, zwróc sumę wszystkich elementów
//Przykład: ar=[1,2,3,4], więc zwraca 10.
public class Exercise_01 {
    private int sumA(List<Integer> number) {
        int sum = 0;
        for (Integer integer : number) {
            sum += integer;
        }
        return sum;
    }

    private int sumB(List<Integer> number) {
        return number.stream()
                .reduce(0, Integer::sum);
    }

    @Test
    public void testA() {
        List<Integer> number = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        int expectedResult = 10;
        Assert.assertEquals(expectedResult, sumA(number));
    }

    @Test
    public void testB() {
        List<Integer> number = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        int expectedResult = 10;
        Assert.assertEquals(expectedResult, sumB(number));
    }


}
