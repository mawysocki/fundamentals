package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Masz rozdać bochenki chleba swoim poddanym.
//Każdy z poddanych ma już pewną liczbę bochenków co jest zawarte w tablicy
//Rozdaj bochenki tak, aby każdy z poddanych miał ich parzystą liczbę
//
public class Exercise_Bread {


    private boolean isEven(List<Integer> input) {
        int sum = 0;
        for (int i = 0; i < input.size(); i++) {
            sum += input.get(i);
        }
        return sum % 2 == 0;
    }

    private boolean isEven(Integer input) {
        return input % 2 == 0;
    }

    public String giveBread(List<Integer> input) {
        if (!isEven(input)) {
            return "NO";
        }
        Integer loaves = 0;
        for (int i = 0; i < input.size() - 1; i++) {
            if (!isEven(input.get(i))) {
                input.set(i, input.get(i) + 1);
                input.set(i + 1, input.get(i + 1) + 1);
                loaves += 2;
            }
        }
        return loaves.toString();
    }

    @Test
    public void test1() {
        List<Integer> input = new ArrayList<>(Arrays.asList(2, 3, 4, 5, 6));
        String expectedResult = "4";
        Assert.assertEquals(expectedResult, giveBread(input));
    }

    @Test
    public void test2() {
        List<Integer> input = new ArrayList<>(Arrays.asList(1, 2));
        String expectedResult = "NO";
        Assert.assertEquals(expectedResult, giveBread(input));
    }

//    @Test
//    public void test1() {
//        int[] input = {2, 3, 4, 5,6};
//        int expectedResult = 4;
//        Assert.assertEquals(expectedResult, giveBread(input));
//    }
//
//    @Test
//    public void test2() {
//        int[] input = {1, 2};
//        int expectedResult = -1;
//        Assert.assertEquals(expectedResult, giveBread(input));
//    }

//    private boolean isEven(int[] input) {
//        int sum = 0;
//        for (int i = 0; i < input.length; i++) {
//            sum += input[i];
//        }
//        return sum % 2 == 0;
//    }
//
//    private boolean isEven(Integer input) {
//        return input % 2 == 0;
//    }
//    public int giveBread(int[] input) {
//
//        if (!isEven(input)) { return -1;}
//        int loaves = 0;
//        for (int i = 0; i < input.length-1; i++) {
//            if (!isEven(input[i])) {
//                input[i]++;
//                input[i+1]++;
//                loaves +=2;
//            }
//        }
//        return loaves;
//    }
}
