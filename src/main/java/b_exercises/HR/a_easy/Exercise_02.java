package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Mając listę liczb, odwróć ją kolejnością
//Przykład a = [1, 2, 3] => b = [3, 2, 1]
public class Exercise_02 {
    private ArrayList<Integer> reverseArray(List<Integer> input) {
        ArrayList<Integer> output = new ArrayList<>();
        for (int i = input.size()-1; i >= 0 ; i--) {
            output.add(input.get(i));
        }
        return output;
    }

    @Test
    public void test1() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(3, 2, 1));
        Assert.assertEquals(expectedResult, reverseArray(input));
    }

    @Test
    public void test2() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(3, -1, 0, -1, 9));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(9, -1, 0, -1, 3));
        Assert.assertEquals(expectedResult, reverseArray(input));
    }
}

