package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Zawodnicy biorą udział w konkursie wiedzy na różne tematy.
//Każdy zawodnik reprezentowany jest przez String, który składa się z ciągu 0 i 1.
//Każda 1 oznacza znajomość jakiegoś tematu. 0 oznacza brak wiedzy na dany temat.
//Zawodnicy łaczą się w pary, aby pokryć jak największą pulę tematów.
//Jeśli dany temat jest znanany przez przynajmniej jedenego z uczestników to jest uznawany jako znany przez drużynę
//Sprawdż ile tematów maksymalnie jest są w stanie ogarnąć dane drużyny oraz ile drużyn jest w takiej sytuacji
//Input: 10101, 11110, 00010 - oznacza 3 zawodników i 5 tematów
//Połączenie zawodnika 1 i 2 daje znajomość wszystkich 5 tematów. Jest to jedyna kombinacja drużyn dla 5.
//Pozostałem kombinacje tj 1 i 3 oraz 2 i 3 są w stanie ogarnąc 4 tematy, więc nie wliczają się do łacznej stawki
//Output: (5,1)
public class Exercise_ACMfinals {

    private List<Integer> acmTeam(List<String> topics) {
        List<Integer> result = new ArrayList<>();
        int maxTopics = 0;
        int teamsWithMaxTopics = 0;
        int currentFamilarity;
        for (int first = 0; first < topics.size()-1; first++) {
            for (int second = first+1; second < topics.size(); second++) {
                currentFamilarity = checkFamilarity(topics.get(first), topics.get(second));
                if (currentFamilarity > maxTopics) {
                    maxTopics = currentFamilarity;
                    teamsWithMaxTopics = 1;
                } else if (currentFamilarity == maxTopics) {
                    teamsWithMaxTopics++;
                }

            }
        }
        result.add(maxTopics);
        result.add(teamsWithMaxTopics);
        return result;
    }

    private int checkFamilarity(String s1, String s2) {
        int familarity = 0;
        char[] s1Array = s1.toCharArray();
        char[] s2Array = s2.toCharArray();
        for (int i = 0; i < s1Array.length; i++) {
            if (s1Array[i] == '1' || s2Array[i] == '1') {
                familarity++;
            }
        }
        return familarity;
    }

    @Test
    public void test1() {
        List<String> topics = new ArrayList<>(Arrays.asList("10101", "11100", "11010", "00101"));
        List<Integer> teams = new ArrayList<>(Arrays.asList(5,2));
        Assert.assertEquals(teams, acmTeam(topics));
    }

    @Test
    public void test2() {
        List<String> topics = new ArrayList<>(Arrays.asList("11101", "10101", "11001", "10111", "10000", "01110"));
        List<Integer> teams = new ArrayList<>(Arrays.asList(5,6));
        Assert.assertEquals(teams, acmTeam(topics));
    }
}



