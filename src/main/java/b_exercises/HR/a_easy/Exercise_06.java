package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Maria rozgrywa mecze koszykówki. Spisuje punkty po każdym meczu. Dane wejściowe to punkty z kolejnych meczy.
//Za każdym razem sprawdzane jest czy w danym meczu padł rekord najliczniejszych goli bądź najmniej licznych.
//Napisz metodę, która zwróci listę dwuelementową, gdzie pierwsza to liczba pobietych rekordów max, a drugi min
//Game     Score   Minimum  Maximum   Min Max
//     0      12     12       12       0   0
//     1      24     12       24       0   1
//     2      10     10       24       1   1
//     3      28     10       28       2   1
public class Exercise_06 {

    private int[] findNumbers(int[] scores) {
        int theSmallest = scores[0];
        int countSmall = 0;
        int theHighest = scores[0];
        int countHigh = 0;
        for (int x : scores) {
            if (x > theHighest) {
                theHighest = x;
                countHigh++;
            }
            if (x < theSmallest) {
                theSmallest = x;
                countSmall++;
            }
        }
        return new int[]{countHigh, countSmall};
    }

    @Test
    public void test1() {
        int[] scores = {10, 5, 20, 20, 4, 5, 2, 25, 1};
        int[] records = {2, 4};
        Assert.assertArrayEquals(records, findNumbers(scores));
    }
    @Test
    public void test2() {
        int[] scores = {3, 4, 21, 36, 10, 28, 35, 5, 24, 42};
        int[] records = {4, 0};
        Assert.assertArrayEquals(records, findNumbers(scores));
    }

}
