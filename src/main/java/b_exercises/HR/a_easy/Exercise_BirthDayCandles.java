package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Na torcie masz świeczki różnej wysokości. Każda wartość elementu symbolizuje wysokość świeczki.
//Znajdź ile razy występuje najwyższa z nich
//arr = [4, 4, 1, 3]
//Najwyższa świeczka ma wysokość 4 i są 2 sztuki, więc odpowiedź to 2.
public class Exercise_BirthDayCandles {

    private int birthdayCakeCandles(List<Integer> list) {
        int max = 0;
        int maxOccurance = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > max) {
                max = list.get(i);
                maxOccurance = 0;
            }
            if (max == list.get(i)) {
               maxOccurance++;
            }
        }
        return maxOccurance;
    }

    @Test
    public void test1() {
        List<Integer> list = new ArrayList<>(Arrays.asList(4, 4, 1, 3));
        int expectedResult = 2;
        Assert.assertEquals(expectedResult, birthdayCakeCandles(list));
    }

    @Test
    public void test2() {
        List<Integer> list = new ArrayList<>(Arrays.asList(3, 1, 2, 3, 3, 1, 3));
        int expectedResult = 4;
        Assert.assertEquals(expectedResult, birthdayCakeCandles(list));
    }

    @Test
    public void test3() {
        List<Integer> list = new ArrayList<>(Arrays.asList(5, 5, 6, 6, 2, 1, 5, 6));
        int expectedResult = 3;
        Assert.assertEquals(expectedResult, birthdayCakeCandles(list));
    }


}
