package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Masz mapę topograficzną w postaci tablicy A.
//Każda z wartości oznacza wysokość wzniesienia
//Znajdź najwyższe wzniesienia na mapie
//Punkt uważany jest za najwyższy gdy wszystkie jest sąsiadujące elementy są od niego niższe
//Nie wliczają się terany na granicy mapy
//Zwróć wynik w postaci takiej samej tablicy jak na wejściu tyle, że znalezione szczyty zamienione sa na -1
public class Exercise_CavityMap {

    private boolean isEdge(int x, int y, int size) {
        return x==0 || x==size-1 || y==0 || y==size-1;
    }

    private boolean checkNeigbours(int[][] map, int x, int y) {
        boolean isLeft = map[x][y] > map[x-1][y];
        boolean isRight = map[x][y] > map[x+1][y];
        boolean isUp = map[x][y] > map[x][y-1];
        boolean isDown = map[x][y] > map[x][y+1];
        return isLeft && isRight && isUp && isDown;
    }

    private int[][] createMap(int[][] map) {
        int[][] newMap = new int[map.length][map.length];

        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map.length; y++) {
                if (isEdge(x,y, map.length))
                {
                    newMap[x][y] = map[x][y];
                    continue;
                }
                if (checkNeigbours(map, x, y)) {
                    newMap[x][y] = -1;
                }
                else {
                    newMap[x][y] = map[x][y];
                }
            }
        }
        return newMap;
    }



    @Test
    public void test1() {
        int[][] map = {{9,8,9},{1,9,1},{1,1,1}};
        int[][] expectedMap = {{9,8,9},{1,-1,1},{1,1,1}};
        Assert.assertArrayEquals(expectedMap, createMap(map));
    }

    @Test
    public void test2() {
        int[][] map = {{1,1,1,2},{1,9,1,2},{1,8,9,2,}, {1,2,3,4}};
        int[][] expectedMap = {{1,1,1,2},{1,-1,1,2},{1,8,-1,2,}, {1,2,3,4}};
        Assert.assertArrayEquals(expectedMap, createMap(map));
    }


}
