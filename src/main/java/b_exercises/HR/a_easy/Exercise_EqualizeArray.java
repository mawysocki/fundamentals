package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//Mając podaną tablicę znajdź ile minimalnie należy usunąć liczb, aby pozostały tylko z taką samą wartością
//A = [1, 2, 2, 3]  => 2. Nalezy usunąć liczby 1 oraz 3 aby zostały same trójki
public class Exercise_EqualizeArray {

    private int getNumber(List<Integer> input) {
        Collections.sort(input);
        int maxcount = 1;
        int prevNumber = input.get(0);
        int currentCount = 1;
        for (int i = 1; i < input.size(); i++) {
            System.out.println("Poprzednia: " + prevNumber);
            int currentNumber = input.get(i);
            System.out.println("Obecna: " + currentNumber);
            if (currentNumber == prevNumber) {
                currentCount++;
                if (currentCount > maxcount) {
                    maxcount = currentCount;
                }
            } else {
                prevNumber = currentNumber;
                currentCount = 1;
            }

        }
        System.out.println("Max count: " + maxcount);
        return input.size() - maxcount;
    }

    @Test
    public void test1() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 2, 2, 3));
        int expectedResult = 2;
        Assert.assertEquals(expectedResult, getNumber(input));
    }

    @Test
    public void test2() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 3, 3, 3, 2, 5, 2));
        int expectedResult = 4;
        Assert.assertEquals(expectedResult, getNumber(input));
    }


    @Test
    public void test3() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(3, 3, 2, 1, 3));
        int expectedResult = 2;
        Assert.assertEquals(expectedResult, getNumber(input));
    }

    @Test
    public void test4() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(37, 32, 97, 35, 76, 62));
        int expectedResult = 5;
        Assert.assertEquals(expectedResult, getNumber(input));
    }
}
