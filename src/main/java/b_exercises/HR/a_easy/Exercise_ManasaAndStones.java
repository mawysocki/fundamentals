package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

//Manasa idzie w góry i znajduje kamienie na których znajdują się liczby.
//Liczby te różnią się względem poprzednich kamieni o wartość A lub B
//Wiedząc, że mamy sprawdzić N kamieni wypisz wszystkie możliwe sumy dla kamienia N.
//Pierwszy kamień jest zerowy
//Przykład. N=3, A=10, B=100. Mamy sprawdzić trzeci kamień w różnych kombinacjach.
//Obliczenia: 1) 0+10+10 = 20, 2) 0+10+100 = 110, 3) 0+100+10 = 110, 4) 0+100+100=200
//Wynik: 20, 110, 200 - wartości powinny być unikatowe
public class Exercise_ManasaAndStones {

    private List<Integer> findStones(int n, int a, int b) {

        List<Integer> stones = new ArrayList<>();
        int sum;
        int min = Math.min(a,b);
        int max = Math.max(a,b);
        int add = max - min;
        sum = (n-1) * min;

        System.out.println("First sum: " + sum);
        stones.add(sum);
        for (int i = 0; i < n-1; i++) {
            sum += add;
            if (!stones.contains(sum)) {
                stones.add(sum);
            }
        }
        return stones;
    }

    @Test
    public void test1() {
        int n = 3;
        int a = 1;
        int b = 2;

        List<Integer> expected = new ArrayList<>(Arrays.asList(2,3,4));
        Assert.assertEquals(expected, findStones(n,a,b));
    }

    @Test
    public void test2() {
        int n = 4;
        int a = 10;
        int b = 100;

        List<Integer> teams = new ArrayList<>(Arrays.asList(30, 120, 210, 300));
        Assert.assertEquals(teams, findStones(n,a,b));
    }

    @Test
    public void test3() {
        int n = 7;
        int a = 9;
        int b = 11;

        List<Integer> teams = new ArrayList<>(Arrays.asList(54, 56, 58, 60, 62, 64, 66));
        Assert.assertEquals(teams, findStones(n,a,b));
    }

    @Test
    public void test4() {
        int n = 4;
        int a = 8;
        int b = 16;

        List<Integer> teams = new ArrayList<>(Arrays.asList(24, 32, 40, 48));
        Assert.assertEquals(teams, findStones(n,a,b));
    }

    @Test
    public void test5() {
        int n = 316;
        int a = 1;
        int b = 1;

        List<Integer> teams = new ArrayList<>(Arrays.asList(315));
        Assert.assertEquals(teams, findStones(n,a,b));
    }




}
