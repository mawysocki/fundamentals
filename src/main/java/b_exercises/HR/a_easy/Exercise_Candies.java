package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Przy okrągłym stole siedzi L osób i jest do rozdania C cukierów
//Losujemy K pozycję krzesła od którego zaczynamy liczenie i dajemy kolejnym osobom cukierki
//Sprawdź kto dostanie ostatni cukierek
public class Exercise_Candies {

    public int getSeat(int people, int cukes, int k) {
        int modulo = (cukes + k-1) % people;
        if (modulo == 0) {
            return people;
        }
        return modulo;
    }


    @Test
    public void test01() {
        int expectedSeat = 2;
        Assert.assertEquals(expectedSeat, getSeat(5, 2, 1));
    }

    @Test
    public void test02() {
        int expectedSeat = 3;
        Assert.assertEquals(expectedSeat, getSeat(5, 2, 2));
    }

    @Test
    public void test03() {
        int expectedSeat = 6;
        Assert.assertEquals(expectedSeat, getSeat(7, 19, 2));
    }

    @Test
    public void test04() {
        int expectedSeat = 3;
        Assert.assertEquals(expectedSeat, getSeat(3, 7, 3));
    }

    @Test
    public void test05() {
        int expectedSeat = 5;
        Assert.assertEquals(expectedSeat, getSeat(5, 5, 1));
    }

    @Test
    public void test05B() {
        int expectedSeat = 5;
        Assert.assertEquals(expectedSeat, getSeat(5, 15, 1));
    }

    @Test
    public void test05C() {
        int expectedSeat = 2;
        Assert.assertEquals(expectedSeat, getSeat(5, 15, 3));
    }

    @Test
    public void test05D() {
        int expectedSeat = 4;
        Assert.assertEquals(expectedSeat, getSeat(5, 15, 5));
    }

    @Test
    public void test05E() {
        int expectedSeat = 5;
        Assert.assertEquals(expectedSeat, getSeat(5, 16, 5));
    }

    @Test
    public void test06() {
        int expectedSeat = 1;
        Assert.assertEquals(expectedSeat, getSeat(1, 4, 1));
    }

    @Test
    public void test07() {
        int expectedSeat = 10;
        Assert.assertEquals(expectedSeat, getSeat(11, 4, 7));
    }

    @Test
    public void test08() {
        int expectedSeat = 3;
        Assert.assertEquals(expectedSeat, getSeat(11, 4, 11));
    }

    @Test
    public void test09() {
        int expectedSeat = 4;
        Assert.assertEquals(expectedSeat, getSeat(11, 6, 10));
    }
    @Test
    public void test10A() {
        int expectedSeat = 10;
        Assert.assertEquals(expectedSeat, getSeat(10, 2, 9));
    }
    @Test
    public void test10B() {
        int expectedSeat = 1;
        Assert.assertEquals(expectedSeat, getSeat(10, 2, 10));
    }
    @Test
    public void test11() {
        int expectedSeat = 1;
        Assert.assertEquals(expectedSeat, getSeat(1, 1, 1));
    }
    @Test
    public void test12() {
        int expectedSeat = 49;
        Assert.assertEquals(expectedSeat, getSeat(100, 50, 100));
    }
    @Test
    public void test13() {
        int expectedSeat = 5;
        Assert.assertEquals(expectedSeat, getSeat(5, 4, 2));
    }
    @Test
    public void test13B() {
        int expectedSeat = 1;
        Assert.assertEquals(expectedSeat, getSeat(5, 4, 3));
    }
}
