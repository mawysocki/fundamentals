package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Mamy książkę z zadaniami. Mamy podaną tablicę zadań dla każdego rozdziału ARR
//Każda strona mieści K zadań
//Każdy rozdział zaczyna się od nowej strony
//Numeracja zadań w każdym rozdziale zaczyna się od nowa
//Sprawdź ile razy wystąpi X tak, że numer zadania będzie równy numerowi strony
//Przykład:
//ARR=[4,3] - w pierwszy rozdziale są 4 zadania, a w drugim 3
//Pierwszy rozdział mieści się na dwóch stronach.
//Pierwsze zadanie znajduje się na pierwszej stronie więc jest szczęśliwą liczbą: X++
//Drugi rozdział zaczyna się od 3 strony. Zadanie nr 3 z tego rozdzialu znajduje się również na tej stronie: X++
//Odpowiedź znaleziono 2 takie liczby

public class Exercise_ExerciseWorkbook {
    public int findPages(int[] chapters, int onPage) {
        int specialPages = 0;
        int currentPage = 1;
        int pagesPerChapter;
        int currentChapter = 1;
        System.out.println("On page: " + onPage);
        for (int chapter: chapters) {
            System.out.println("Current chapter: " + currentChapter);
            pagesPerChapter = chapter / onPage;
            if (chapter%onPage !=0) {
                pagesPerChapter++;
            }
            System.out.printf("Tasks: %s, pages: %s", chapter, pagesPerChapter);
            System.out.println();
            for (int i = 1; i <= pagesPerChapter; i++) {
                for (int currentTask = 1 + (onPage * i - onPage); currentTask <= onPage*i && currentTask<=chapter; currentTask++) {
                    System.out.println("Current Page: " + currentPage + " current task: " + currentTask);
                    if (currentTask == currentPage) {
                        specialPages++;
                        System.out.println("Special page: " + specialPages);
                    }
                }
                currentPage++;
            }
            currentChapter++;

        }

        return specialPages;
    }

    @Test
    public void test0() {
        int[] chapters = {4, 3};
        int onPage = 3;
        int expectedPages = 2;
        Assert.assertEquals(expectedPages, findPages(chapters, onPage));
    }

    @Test
    public void test1() {
        int[] chapters = {4, 2};
        int onPage = 3;
        int expectedPages = 1;
        Assert.assertEquals(expectedPages, findPages(chapters, onPage));
    }

    @Test
    public void test2() {
        int[] chapters = {4, 2, 6, 1, 10};
        int onPage = 3;
        int expectedPages = 4;
        Assert.assertEquals(expectedPages, findPages(chapters, onPage));
    }

    @Test
    public void test3() {
        int[] chapters = {3,8,15,11,14,1,9,2,24,31};
        int onPage = 5;
        int expectedPages = 8;
        Assert.assertEquals(expectedPages, findPages(chapters, onPage));
    }




}
