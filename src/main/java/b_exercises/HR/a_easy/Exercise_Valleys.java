package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Zapalony alpinista zliczał wszystkie swoje kroki.
//U - krok do góry, D - krok do dołu
//Wspinaczkę zawsze zaczyna od poziomu morza
//Jeśli idzie w górę od poziomu morza to zwiedza góry, jeśli w dół to doliny
//Bazując na podanej przez niego ścieżce policz ile pokonał dolin
//Przykład: path = [DDUUUUDD]
// poszedł 2 poziomy w dół czyli zaliczył jedną dolinę
// potem zrównał sie poziomem i poszedł na górę
public class Exercise_Valleys {


    public int findValleys(String path) {
        int valleysCounter = 0;
        int currentLevel = 0;
        int previousLevel;
        char[] steps = path.toCharArray();
        for (int i = 0; i < steps.length; i++) {
            previousLevel = currentLevel;
            currentLevel += countStep(steps[i]);
            if (previousLevel == 0 && currentLevel == -1) {
                valleysCounter++;
            }
        }
        return valleysCounter;
    }

    private int countStep(char step) {
        if (step == 'U') {
            return 1;
        } else return -1;
    }
    @Test
    public void test0() {
        String path = "";
        int expectedValleys = 0;
        Assert.assertEquals(expectedValleys, findValleys(path));
    }

    @Test
    public void test1() {
        String path = "DDUUUUDD";
        int expectedValleys = 1;
        Assert.assertEquals(expectedValleys, findValleys(path));
    }
    @Test
    public void test2() {
        String path = "DDUUDDUDUUUD";
        int expectedValleys = 2;
        Assert.assertEquals(expectedValleys, findValleys(path));
    }

    @Test
    public void test3() {
        String path = "UUUUDDDDUUDD";
        int expectedValleys = 0;
        Assert.assertEquals(expectedValleys, findValleys(path));
    }
    @Test
    public void test4() {
        String path = "UDUDUDUDUD";
        int expectedValleys = 0;
        Assert.assertEquals(expectedValleys, findValleys(path));
    }
    @Test
    public void test5() {
        String path = "DUDUDUDUDUDU";
        int expectedValleys = 6;
        Assert.assertEquals(expectedValleys, findValleys(path));
    }

}
