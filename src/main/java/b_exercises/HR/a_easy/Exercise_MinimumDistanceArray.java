package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Znajdź najmniejszą odległość między dwiema takimi samymi liczbami w tablicy
public class Exercise_MinimumDistanceArray {

    private int findDistance(List<Integer> input) {
        int minDistance = Integer.MAX_VALUE;
        for (int i = 0; i < input.size()-1; i++) {
            for (int j = i+1; j < input.size(); j++) {
                if (input.get(i).equals(input.get(j))) {
                    int distance = j - i;
                    if (distance < minDistance) {
                        minDistance = distance;
                    }
                }
            }
        }

        if (minDistance == Integer.MAX_VALUE) {
            return -1;
        }
        else {
            return minDistance;
        }
    }

    @Test
    public void test1() {
        List<Integer> input = new ArrayList<>(Arrays.asList(3,2,1,2,3));
        int expectedDistance = 2;
        Assert.assertEquals(expectedDistance, findDistance(input));
    }

    @Test
    public void test2() {
        List<Integer> input = new ArrayList<>(Arrays.asList(7,1,3,4,1,7));
        int expectedDistance = 3;
        Assert.assertEquals(expectedDistance, findDistance(input));
    }

    @Test
    public void test3() {
        List<Integer> input = new ArrayList<>(Arrays.asList(1,2,3,4,10));
        int expectedDistance = -1;
        Assert.assertEquals(expectedDistance, findDistance(input));
    }


}
