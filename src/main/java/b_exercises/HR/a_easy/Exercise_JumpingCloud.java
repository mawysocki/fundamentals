package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exercise_JumpingCloud {

    private int jump(List<Integer> clouds) {
        int jumps = 0;
        int currentIndex = 0;
        while (currentIndex != clouds.size() - 1) {
            boolean inRange = currentIndex+2 < clouds.size();
            if (inRange && clouds.get(currentIndex + 2) == 0) {
                currentIndex += 2;
            } else {
                currentIndex += 1;
            }
            jumps++;
        }
        return jumps;
    }

    @Test
    public void test1() {
        List<Integer> clouds = new ArrayList<>(Arrays.asList(0, 1, 0, 0, 0, 1, 0));
        int expectedSteps = 3;
        Assert.assertEquals(expectedSteps, jump(clouds));
    }

    @Test
    public void test2() {
        List<Integer> clouds = new ArrayList<>(Arrays.asList(0, 0, 1, 0, 0, 1, 0));
        int expectedSteps = 4;
        Assert.assertEquals(expectedSteps, jump(clouds));
    }

    @Test
    public void test3() {
        List<Integer> clouds = new ArrayList<>(Arrays.asList(0, 0, 0, 1, 0, 0));
        int expectedSteps = 3;
        Assert.assertEquals(expectedSteps, jump(clouds));
    }


}
