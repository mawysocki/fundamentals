package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Na uniwersytecie  wystawiane są oceny od 0 do 100
//Student oblewa jeśli zdobył mniej niż 40 punktów
//Profesor zaokrągla oceny według nastepujących reguł:
//Jeśli różnica między oceną a najbliższą, kolejną wielokrotnością 5 jest mniejsza niż 3
//to zaokrąglij ocenę w górę do kolejnej wielokrotności 5
//Jeśli wartość jest mniejsza niż 38 to nie zaokrąglaj, ponieważ taki student i tak nie zda
//Przykłady
// ocena 84 zaokrąglamy bo 85 - 84 < 3
// ocena 29 nie zaokrąglamy bo po zaokrągleniu i tak byłoby mniej niż 40
// ocena 57 nie zaokrąglamy bo 60 - 57 >= 3)

public class Exercise_GradingStudents {

    private List<Integer> gradingStudents(List<Integer> input) {
        List<Integer> grades = new ArrayList<>();
        for (int grade : input) {
            if (grade < 38) {
                grades.add(grade);
            } else {
                grades.add(roundGrade(grade));
            }

        }
        return grades;
    }

    private int roundGrade(int grade) {
        if (grade % 5 >= 3) {
            return grade - grade % 5 + 5;
        }
        return grade;
    }

    @Test
    public void test1() {
        List<Integer> input = new ArrayList<>(Arrays.asList(84, 29, 57, 38, 37, 1, 91, 92, 93, 94, 95));
        List<Integer> expectedResult = new ArrayList<>(Arrays.asList(85, 29, 57, 40, 37, 1, 91, 92, 95, 95, 95));
        Assert.assertEquals(expectedResult, gradingStudents(input));
    }


}
