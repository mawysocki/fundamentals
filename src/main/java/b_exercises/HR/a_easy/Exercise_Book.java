package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Mając książkę z liczbą stron "n" policz ile razy musisz minimalnie przekręcić kartkę, aby osiągnąc stronę "p"
//Zwróc wartość z najkrótszą drogą w zalezności czy idziesz od przodu czy od tyłu
//Uwaga: Strona nr 1 jest zawsze po prawej stronie, bo początek książki zawiera pustą stronę
public class Exercise_Book {

    private boolean hasBlankPage(int totalPages) {
        return totalPages % 2 == 0;
    }

    private int getFromStart(int desiredPage) {
        int currentPage = 1;
        int count = 0;
        while (currentPage < desiredPage) {
            currentPage += 2;
            count++;
        }
        return count;
    }

    private int getFromEnd(int totalPages, int desiredPage) {
        int count = 0;
        int currentPage = totalPages;
        if (!hasBlankPage(totalPages)) {
            currentPage--;
        }
        while (currentPage > desiredPage) {
            System.out.println("Actual page: " + currentPage);
            currentPage -= 2;
            count++;
        }
        return count;
    }

    private int countPages(int totalPages, int desiredPage) {
        int min = -1;
        if (totalPages / 2 < desiredPage) {
            min = getFromEnd(totalPages, desiredPage);
        }
        else {
            min = getFromStart(desiredPage);
        }

        return min;
    }

    @Test
    public void test1() {
        int totalPages = 5;
        int desiredPage = 3;
        int minimumPagesToTurn = 1;
        Assert.assertEquals(minimumPagesToTurn, countPages(totalPages, desiredPage));
    }

    @Test
    public void test2() {
        int totalPages = 6;
        int desiredPage = 2;
        int minimumPagesToTurn = 1;
        Assert.assertEquals(minimumPagesToTurn, countPages(totalPages, desiredPage));
    }

    @Test
    public void test3() {
        int totalPages = 5;
        int desiredPage = 4;
        int minimumPagesToTurn = 0;
        Assert.assertEquals(minimumPagesToTurn, countPages(totalPages, desiredPage));
    }

    @Test
    public void test4() {
        int totalPages = 6;
        int desiredPage = 4;
        int minimumPagesToTurn = 1;
        Assert.assertEquals(minimumPagesToTurn, countPages(totalPages, desiredPage));
    }

    @Test
    public void test5() {
        int totalPages = 57;
        int desiredPage = 46;
        int minimumPagesToTurn = 5;
        Assert.assertEquals(minimumPagesToTurn, countPages(totalPages, desiredPage));
    }
}
