package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;

//Napisz program który ze zbioru liczb znajdzie dwie takie liczby, aby ich suma była równa liczbie z wejścia
//Przykład: zbiór: {2, 9, 25, 117, 10}, input: 35, wynik: {25, 10}

public class Exercise_09 {

    private int[] findPair(int[] array, int input) {
        int[] pair = new int[2];
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < array.length; i++) {
            int second = input - array[i];
            if (map.containsKey(second)) {
                pair[0] = second;
                pair[1] = array[i];
                break;
            }
            else {
                map.put(array[i], i);
            }
        }
        return pair;
    }

    @Test
    public void test1() {
        int[] array = {2, 9, 25, 117, 10, 55};
        int input = 35;
        int[] pair = {25, 10};
        Assert.assertArrayEquals(pair, findPair(array, input));
    }

    @Test
    public void test2() {
        int[] array = {99, 35, 11, 257, 10, 33};
        int input = 290;
        int[] pair = {257, 33};
        Assert.assertArrayEquals(pair, findPair(array, input));
    }

}
