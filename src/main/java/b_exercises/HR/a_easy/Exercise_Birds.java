package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

//Mając tablicę obserwowanych ptaktów, gdzie każdy gatunek ma swoje ID
//napisz metodę zwracającą ID najczęściej występującego gatunku.
//Jeśli dwa gatunki mają tę samą liczebność, weź ten o mniejszym ID
public class Exercise_Birds {

    private int findTheLowest(HashMap<Integer, Integer> map) {
        int theMost = Integer.MIN_VALUE;
        int myID = Integer.MIN_VALUE;;
        for (Integer id : map.keySet()) {
            if (map.get(id) > theMost || (map.get(id) == theMost && id < myID)) {
                theMost = map.get(id);
                myID = id;
            }
        }
        return myID;
    }

    private int findID(ArrayList<Integer> input) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (Integer id : input) {
            if (!map.containsKey(id)) {
                map.put(id, 1);
            }
            else {
                map.put(id, map.get(id)+1);
            }
        }
        return findTheLowest(map);
    }

    @Test
    public void test1() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 1, 2, 2, 3));
        int expectedResult = 1;
        Assert.assertEquals(expectedResult, findID(input));
    }

    @Test
    public void test2() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 4, 3, 2, 1, 3, 4));
        int expectedResult = 3;
        Assert.assertEquals(expectedResult, findID(input));
    }

    @Test
    public void test3() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(4, 5, 3, 5, 3, 2, 5, 2, 4, 3, 4));
        int expectedResult = 3;
        Assert.assertEquals(expectedResult, findID(input));
    }

    @Test
    public void test4() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(4, 5, 3, 5, 3, 2, 5, 2, 4, 3, 4, 2));
        int expectedResult = 2;
        Assert.assertEquals(expectedResult, findID(input));
    }
}
