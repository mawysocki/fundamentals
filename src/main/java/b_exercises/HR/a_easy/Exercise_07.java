package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

//Ze zbioru X policz ile jest par, których suma podzielna jest przez liczbę Z
//Przykład X = [1, 2, 3, 4, 5, 6],  Z=5
//Pary: {1,4}, {2,3}, {4,6} suma tych par podzielna jest przez 5.
//Wynik: 3
public class Exercise_07 {
    private int countPairs(ArrayList<Integer> input, int divisible) {
        int counter = 0;
        for (int i = 0; i < input.size(); i++) {
            for (int j = i+1; j < input.size(); j++) {
                int sum = input.get(i) + input.get(j);
                if (sum % divisible == 0) {
                    counter++;
                }
            }
        }

        return counter;
    }

    @Test
    public void test1() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        int divisible = 5;
        int expectedResult = 3;
        Assert.assertEquals(expectedResult, countPairs(input, divisible));
    }

    @Test
    public void test2() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 3, 2, 6, 1, 2));
        int divisible = 3;
        int expectedResult = 5;
        Assert.assertEquals(expectedResult, countPairs(input, divisible));
    }

    @Test
    public void test3() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(19, 31, 21, 16, 11, 21));
        int divisible = 11;
        int expectedResult = 0;
        Assert.assertEquals(expectedResult, countPairs(input, divisible));
    }

}
