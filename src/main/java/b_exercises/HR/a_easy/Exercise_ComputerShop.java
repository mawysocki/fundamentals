package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//W sklepie chcemy zakupić klawiaturę oraz pendriva
//Mamy do wykorzystania budżet "X" oraz dwie listy jako cennik "A" dla klawiatury
//Oraz cennik "B" dla pendriva
//Znajdź kombinacje dla którego łaczna cena będzie maksymalna i zwróć tę cenę
//albo zwróć -1 jeśli nie da się kupić obydwu rzeczy
public class Exercise_ComputerShop {
    private int buyItems(List<Integer> keyboards, List<Integer> pendrives, int budget) {
        int max = -1;
        for (Integer keyboard : keyboards) {
            for (Integer pendrive : pendrives) {
                int sum = keyboard + pendrive;
                if (sum <= budget && sum > max) {
                    max = sum;
                }
            }
        }
        return max;
    }

    @Test
    public void test1() {
        List<Integer> keyboards = new ArrayList<>(Arrays.asList(3, 1));
        List<Integer> pendrives = new ArrayList<>(Arrays.asList(5, 2, 8));
        int budget = 10;
        int expectedValue = 9;
        Assert.assertEquals(expectedValue, buyItems(keyboards, pendrives, budget));
    }

    @Test
    public void test2() {
        List<Integer> keyboards = new ArrayList<>(Arrays.asList(51, 92, 33));
        List<Integer> pendrives = new ArrayList<>(Arrays.asList(41, 81, 11));
        int budget = 75;
        int expectedValue = 74;
        Assert.assertEquals(expectedValue, buyItems(keyboards, pendrives, budget));
    }

    @Test
    public void test3() {
        List<Integer> keyboards = new ArrayList<>(Arrays.asList(5, 9));
        List<Integer> pendrives = new ArrayList<>(Arrays.asList(4, 8, 11));
        int budget = 8;
        int expectedValue = -1;
        Assert.assertEquals(expectedValue, buyItems(keyboards, pendrives, budget));
    }
}
