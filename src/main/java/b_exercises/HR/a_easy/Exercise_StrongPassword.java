package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Silne hasło wymaga trzymania się następujących reguł:
//1 - Musi mieć co najmniej 6 znaków długości
//2 - Musi mieć co najmniej 1 dużą literę
//3 - Musi mieć co najmniej 1 małą literę
//4 - Musi mieć co najmniej 1 cyfrę
//5 - Musi mieć co najmniej 1 znak specjalny '!', '@','#','$','%','^','&','*','(',')','-','+'

//Dla podanego hasła podaj ile jeszcze znaków trzeba dodać, żeby hasło można było uznać za mocne
//Input (String), Output (int)

public class Exercise_StrongPassword {

    private int checkPassword(String password) {
        char[] charArray = password.toCharArray();
        int missing = 0;
        boolean isDigit = false;
        boolean isLower = false;
        boolean isUpper = false;
        boolean isSpecial = false;
        for (char c: charArray) {
            if (!isDigit) {
                isDigit = Character.isDigit(c);
            }
            if (!isLower) {
                isLower = Character.isLowerCase(c);
            }
            if (!isUpper) {
                isUpper = Character.isUpperCase(c);
            }
            if (!isSpecial) {
                isSpecial = checkSpecial(c);
            }

        }

        if (!isDigit) {
            missing++;
        }
        if (!isLower) {
            missing++;
        }
        if (!isUpper) {
            missing++;
        }
        if (!isSpecial) {
            missing++;
        }

        if (charArray.length + missing < 6) {
            missing = 6 - charArray.length;
        }
        return missing;
    }

    private boolean checkSpecial(char c) {
        char[] specials = { '!', '@','#','$','%','^','&','*','(',')','-','+'};
        for (char special : specials) {
            if (special == c) {
                return true;
            }
        }
        return false;
    }

    @Test
    public void test1() {
        String password = "Ab1";
        int missingCharacters = 3;
        Assert.assertEquals(missingCharacters, checkPassword(password));
    }
    @Test
    public void test2() {
        String password = "#HackerRank";
        int missingCharacters = 1;
        Assert.assertEquals(missingCharacters, checkPassword(password));
    }

    @Test
    public void test3() {
        String password = "#Hacker32Rank";
        int missingCharacters = 0;
        Assert.assertEquals(missingCharacters, checkPassword(password));
    }

    @Test
    public void test4() {
        String password = "";
        int missingCharacters = 6;
        Assert.assertEquals(missingCharacters, checkPassword(password));
    }

    @Test
    public void test5() {
        String password = "!@#$%&";
        int missingCharacters = 3;
        Assert.assertEquals(missingCharacters, checkPassword(password));
    }

}
