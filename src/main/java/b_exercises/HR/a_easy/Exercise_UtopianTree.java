package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Posadzono drzewko które ma dwa cykle wzrostu w ciągu roku
//Na wiosnę rośnie ono podwójnie
//W lato rośnie zawsze o 1 metr
//Jakiego wzrostu będzie po n cyklach?
public class Exercise_UtopianTree {
    public int countGrowth(int cycles) {
        int growth = 1;
        for (int i = 1; i <= cycles; i++) {
            if (i %2 == 1) {
                growth *= 2;
            } else {
                growth +=1;
            }
        }
        return growth;
    }

    @Test
    public void test0() {
        int cycles = 0;
        int expectedGrowth = 1;
        Assert.assertEquals(expectedGrowth, countGrowth(cycles));
    }

    @Test
    public void test1() {
        int cycles = 1;
        int expectedGrowth = 2;
        Assert.assertEquals(expectedGrowth, countGrowth(cycles));
    }

    @Test
    public void test3() {
        int cycles = 3;
        int expectedGrowth = 6;
        Assert.assertEquals(expectedGrowth, countGrowth(cycles));
    }
    @Test
    public void test4() {
        int cycles = 4;
        int expectedGrowth = 7;
        Assert.assertEquals(expectedGrowth, countGrowth(cycles));
    }

}
