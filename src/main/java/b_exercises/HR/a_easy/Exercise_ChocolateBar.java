package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

//Mając zbiór liczb całkowitych "x" oraz parametry "y" i "z" gdzie
//"y" to liczba użystanych liczb ze zbioru
//a "z" to suma na którą mają składać się te liczby
//Napisz metodę zwracająca liczbę możliwych kombinacji do otrzymania danej sumy w określonym zbiorze

//TO BE DONE!
public class Exercise_ChocolateBar {

    private int findSum(ArrayList<Integer> input, int expectedSum, int numberCounter) {
        System.out.println("Old sum: " + expectedSum);
        System.out.println("Old numberCounter: " + numberCounter);

        int count = 0;
        ArrayList<Integer> copy = new ArrayList<>(input);
        System.out.println("Full array: " + copy);
        for (int i = 0; i < input.size(); i++) {
            System.out.println("Size: " + input.size());
            System.out.println("Index: " + i);

            Integer remove = copy.remove(i);
            int newSum = expectedSum - remove;
            int newCounter = numberCounter - 1;

            System.out.println("New sum: " + newSum);
            System.out.println("New numberCounter: " + newCounter);


            if (newCounter > 0) {
                count += findSum(copy, newSum, newCounter);
            } else if (newCounter == 0) {
                if (newSum == 0) {
                    return 1;
                }

            } else {
                return 0;
            }

            System.out.println("Index: " + i);
            System.out.println("Removed " + copy);
            copy = new ArrayList<>(input);

        }
        return count;
    }

    @Test
    public void test1() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(2, 2, 1, 3, 2));
        int expectedSum = 4;
        int numberCounter = 2;
        int expectedResult = 2;
        Assert.assertEquals(expectedResult, findSum(input, expectedSum, numberCounter));
    }

    @Test
    public void test2() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 2, 1, 3, 2));
        int expectedSum = 3;
        int numberCounter = 2;
        int expectedResult = 2;
        Assert.assertEquals(expectedResult, findSum(input, expectedSum, numberCounter));
    }

    @Test
    public void test3() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 1, 1, 1, 1));
        int expectedSum = 3;
        int numberCounter = 2;
        int expectedResult = 0;
        Assert.assertEquals(expectedResult, findSum(input, expectedSum, numberCounter));
    }

    @Test
    public void test4() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(4));
        int expectedSum = 4;
        int numberCounter = 1;
        int expectedResult = 1;
        Assert.assertEquals(expectedResult, findSum(input, expectedSum, numberCounter));
    }
}
