package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class Exercise_DoubleIndex {

    private List<Integer> getDoubleIndex(ArrayList<Integer> input) {
        List indexList = new LinkedList();
        List sortedList = new ArrayList(input);
        System.out.println("Lista startowa: " + input);
        Collections.sort(sortedList);
        for (Object integer : sortedList) {
            System.out.println("x: " + integer);
            int singleIndex = -1;
            int doubleIndex = -1;
            for (int i = 0; i < input.size(); i++) {
                if (Objects.equals(input.get(i), integer)) {
                    singleIndex = i + 1;
                    System.out.printf("p(%s): \n", singleIndex);
                    break;
                }

            }
            for (int j = 0; j < input.size(); j++) {
                if (input.get(j) == singleIndex) {
                    doubleIndex = j + 1;
                    System.out.printf("r(%s): \n", doubleIndex);
                    break;
                }
            }

            indexList.add(doubleIndex);

        }
        System.out.println(indexList);
        return indexList;
    }

    @Test
    public void test1() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(4, 3, 5, 1, 2));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(1, 3, 5, 4, 2));
        Assert.assertEquals(expectedResult, getDoubleIndex(input));
    }

    @Test
    public void test2() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(2, 3, 1));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(2, 3, 1));
        Assert.assertEquals(expectedResult, getDoubleIndex(input));
    }

    @Test
    public void test3() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(5, 2, 1, 3, 4));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(4, 2, 5, 1, 3));
        Assert.assertEquals(expectedResult, getDoubleIndex(input));
    }

}
