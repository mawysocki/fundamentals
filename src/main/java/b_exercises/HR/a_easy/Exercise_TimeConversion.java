package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Napisz metodę, która przekonwertuje czas 12 godzinny na 24 godzinny
//Zwróć uwagę, że
// 12:00:00AM -> 00:00:00
// 12:00:00PM -> 12:00:00
//Przykład:  wejście = 07:05:45PM  wynik = 19:05:45

public class Exercise_TimeConversion {
    public String timeConversion(String s) {
        String[] split = s.split(":");

        if (split[2].endsWith("AM")) {
            split[2] = split[2].replace("AM", "");
            if (split[0].equals("12")) {
                split[0] = "00";
            }
        } else {
            split[2] = split[2].replace("PM", "");
            if (!split[0].equals("12")) {
                split[0] = String.valueOf((Integer.valueOf(split[0]) + 12));
            }
        }
        return String.format("%s:%s:%s", split[0], split[1], split[2]);
    }

    @Test
    public void test1() {
        String input = "07:05:45PM";
        String expectedResult = "19:05:45";
        Assert.assertEquals(expectedResult, timeConversion(input));
    }

    @Test
    public void test2() {
        String input = "07:05:45AM";
        String expectedResult = "07:05:45";
        Assert.assertEquals(expectedResult, timeConversion(input));
    }

    @Test
    public void test3() {
        String input = "12:00:00AM";
        String expectedResult = "00:00:00";
        Assert.assertEquals(expectedResult, timeConversion(input));
    }

    @Test
    public void test4() {
        String input = "12:00:00PM";
        String expectedResult = "12:00:00";
        Assert.assertEquals(expectedResult, timeConversion(input));
    }

    @Test
    public void test5() {
        String input = "12:01:00AM";
        String expectedResult = "00:01:00";
        Assert.assertEquals(expectedResult, timeConversion(input));
    }

    @Test
    public void test6() {
        String input = "12:01:00PM";
        String expectedResult = "12:01:00";
        Assert.assertEquals(expectedResult, timeConversion(input));
    }


}
