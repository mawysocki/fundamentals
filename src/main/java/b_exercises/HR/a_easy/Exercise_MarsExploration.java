package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

//Łazik z Marsa wysyła komunikat SOS
//Przez kosmiczne promieniowanie na Ziemię przychodzi zakłócony komunikat S
//Zwróć ile liter w komunikacie zostało zmienionych.
//Długość komunikatu jest zawsze wielokrotności wiadomości SOS
public class Exercise_MarsExploration {
    private Map<Integer, Character> getMap() {
        Map<Integer, Character> map = new HashMap<>();
        map.put(0, 'S');
        map.put(1, 'O');
        map.put(2, 'S');
        return map;
    }
    private int decryptMessage(String message) {
        int changes = 0;
        Map<Integer, Character> map = getMap();
        for (int i = 0; i < message.length()/3; i++) {
            for (int j = 0; j < 3; j++) {
                boolean equals = map.get(j).equals(message.charAt(j + i * 3));
                if (!equals)
                    changes++;
            }
            System.out.println();
        }
        return changes;
    }

    @Test
    public void test1() {
        String message = "SOSSPSSQSSOR";
        int expectedNumber = 3;
        Assert.assertEquals(expectedNumber, decryptMessage(message));
    }
    @Test
    public void test2() {
        String message = "SOSSOSSOS";
        int expectedNumber = 0;
        Assert.assertEquals(expectedNumber, decryptMessage(message));
    }

    @Test
    public void test3() {
        String message = "SOSSOT";
        int expectedNumber = 1;
        Assert.assertEquals(expectedNumber, decryptMessage(message));
    }

}
