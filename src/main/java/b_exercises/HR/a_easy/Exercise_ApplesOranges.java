package b_exercises.HR.a_easy;


import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

//Mamy działkę z domem i dwoma drzewami: jabłoń i pomarańcza
//Jabłoń stoi w miejscu a, natomiast pomarańcza w miejscu b
//Stoi tu również dom który zaczyna się w miejscu s i kończy w punkcie t
//Owoce spadają z drzew z pewnym przesunięciem.
//Gdy przesuniecie jest dodatnie to spadają w prawo, a jak ujemne to w lewo
//Napisz metodę, która policzy ile jabłek i pomarańczy wylądowało na dachu, który znajduje się przedziale <s, t>
//Przykład:
//Jabłoń jest w miejscu 4, pomarańcza w miejscu 12
//Dom stoje pomiędzy punktami 7 oraz 10
//Jabłka spadają z nastepującymi wektorami apples=[2, 3, -4]
//Pomarańcze spadają z nastepującymi wektorami oranges=[3, -2, -4]
//Jabłko drugie spadło z wektorem 3 więc wylądowało w punkcie 4+3=7 (czyli na dachu)
//Pomarańcza trzecia spadła z wektorem -4 więc wylądowała 12-4=8 (czyli też na dachu)
//Odpowiedź: [1, 1]

public class Exercise_ApplesOranges {

    private boolean isOnRoof(int tree, int fruit, int houseStart, int houseEnd) {
        return tree + fruit >= houseStart && tree + fruit <= houseEnd;
    }

    private int countFruit(ArrayList<Integer> fruits, int tree, int houseStart, int houseEnd) {
        int onRoof = 0;
        for (Integer fruit: fruits) {
            if (isOnRoof(tree, fruit, houseStart, houseEnd)) {
                onRoof++;
            }
        }
        return onRoof;
    }
    private ArrayList<Integer> countApplesAndOranges(int appleTree, int orangeTree, int houseStart, int houseEnd, ArrayList<Integer> apples, ArrayList<Integer> oranges) {
        int applesOnRoof = countFruit(apples, appleTree, houseStart, houseEnd);
        int orangesOnRoof = countFruit(oranges, orangeTree, houseStart, houseEnd);
        return new ArrayList<>(Arrays.asList(applesOnRoof, orangesOnRoof));
    }

    @Test
    public void test1() {
        int appleTree = 4;
        int orangeTree = 12;
        int houseStart = 7;
        int houseEnd = 10;
        ArrayList<Integer> apples = new ArrayList<>(Arrays.asList(2, 3, -4));
        ArrayList<Integer> oranges = new ArrayList<>(Arrays.asList(3, -2, -4));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(1, 2));
        Assert.assertEquals(expectedResult, countApplesAndOranges(appleTree, orangeTree, houseStart, houseEnd, apples, oranges));
    }

    @Test
    public void test2() {
        int appleTree = 1;
        int orangeTree = 12;
        int houseStart = 4;
        int houseEnd = 10;
        ArrayList<Integer> apples = new ArrayList<>(Arrays.asList(2, 3, -4, 1, -1, 1, 7, 4, 3));
        ArrayList<Integer> oranges = new ArrayList<>(Arrays.asList(3, -2, -4, 1, -1, 2, 0, 9, -1, -3));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(4, 3));
        Assert.assertEquals(expectedResult, countApplesAndOranges(appleTree, orangeTree, houseStart, houseEnd, apples, oranges));
    }


}
