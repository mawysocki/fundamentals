package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

//Z macierzy liczba wyciągnij klepsydry w następujacej postaci
//a b c
//  d
//e f g
//Dla tablicy 6x6 jest ich 16.
//Znajdź największą sumę klepsydr
public class Exercise_Hourglass {

    private int findHourGlass(int[][] arr) {
        ArrayList<Integer> sum  = new ArrayList<>();
        for (int i = 0; i <= arr.length-3; i++) {
            for (int j = 0; j <= arr.length-3; j++) {
                sum.add(calculateHourGalss(i, j, arr));
            }
        }
        Collections.sort(sum);
        return sum.get(sum.size()-1);
    }

    private int calculateHourGalss(int i, int j, int[][] arr) {
        int sum = 0;
        sum+= arr[i][j];
        sum+= arr[i][j+1];
        sum+= arr[i][j+2];
        sum+= arr[i+1][j+1];
        sum+= arr[i+2][j];
        sum+= arr[i+2][j+1];
        sum+= arr[i+2][j+2];
        return sum;
    }

    @Test
    public void test1() {
        int expectedResult = 28;
        int[][] arr = new int[6][6];
        arr[0][0] = -9;   arr[1][0] = 0;  arr[2][0] = -9;  arr[3][0] = 0;  arr[4][0] = 0;  arr[5][0] = 0;
        arr[0][1] = -9;   arr[1][1] = -9; arr[2][1] = -9;  arr[3][1] = 0;  arr[4][1] = 0;  arr[5][1] = 0;
        arr[0][2] = -9;   arr[1][2] = 0;  arr[2][2] = -9;  arr[3][2] = 8;  arr[4][2] = 0;  arr[5][2] = 1;
        arr[0][3] = 1;    arr[1][3] = 4;  arr[2][3] = 1;   arr[3][3] = 6;  arr[4][3] = -2; arr[5][3] = 2;
        arr[0][4] = 1;    arr[1][4] = 3;  arr[2][4] = 2;   arr[3][4] = 6;  arr[4][4] = 0;  arr[5][4] = 4;
        arr[0][5] = 1;    arr[1][5] = 2;  arr[2][5] = 3;   arr[3][5] = 0;  arr[4][5] = 0;  arr[5][5] = 0;
        Assert.assertEquals(expectedResult, findHourGlass(arr));
    }

    @Test
    public void test2() {
        int expectedResult = 19;
        int[][] arr = new int[6][6];
        arr[0][0] = 1;   arr[1][0] = 0;  arr[2][0] = 1;  arr[3][0] = 0;  arr[4][0] = 0;  arr[5][0] = 0;
        arr[0][1] = 1;   arr[1][1] = 1; arr[2][1] = 1;  arr[3][1] = 0;  arr[4][1] = 0;  arr[5][1] = 0;
        arr[0][2] = 1;   arr[1][2] = 0;  arr[2][2] = 1;  arr[3][2] = 2;  arr[4][2] = 0;  arr[5][2] = 1;
        arr[0][3] = 0;    arr[1][3] = 0;  arr[2][3] = 0;   arr[3][3] = 4;  arr[4][3] = 2; arr[5][3] = 2;
        arr[0][4] = 0;    arr[1][4] = 0;  arr[2][4] = 0;   arr[3][4] = 4;  arr[4][4] = 0;  arr[5][4] = 4;
        arr[0][5] = 0;    arr[1][5] = 0;  arr[2][5] = 0;   arr[3][5] = 0;  arr[4][5] = 0;  arr[5][5] = 0;
        Assert.assertEquals(expectedResult, findHourGlass(arr));
    }


}
