package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Bobby posiada N pieniędzy, które może wydać na batona który kosztuje C
//Z każdego batona można zabrać opakowanie.
//Jesli Bobby zbieże M opakowań może wymienic go na batona.
//Policz ile batonów może zjeść Bobby za otrzymane pieniądze
public class Exercise_ChocolateFeast {

    private int countButtons(int money, int buttonCost, int wrappersForButton) {
        int currentButtons = 0;
        int currentWrappers = 0;
        int eatenButtons = 0;
        currentButtons = money / buttonCost;
        eatenButtons += currentButtons;
        currentWrappers += currentButtons;
        while (currentWrappers >= wrappersForButton) {
            currentButtons = currentWrappers / wrappersForButton;
            currentWrappers -= currentButtons * wrappersForButton;
            eatenButtons += currentButtons;
            currentWrappers += currentButtons;
        }
        return eatenButtons;
    }

    @Test
    public void test1() {
        int money = 10;
        int buttonCost = 2;
        int wrappersForButton = 5;
        int eatenButtons = 6;
        Assert.assertEquals(eatenButtons, countButtons(money, buttonCost, wrappersForButton));
    }

    @Test
    public void test2() {
        int money = 12;
        int buttonCost = 4;
        int wrappersForButton = 4;
        int eatenButtons = 3;
        Assert.assertEquals(eatenButtons, countButtons(money, buttonCost, wrappersForButton));
    }

    @Test
    public void test3() {
        int money = 6;
        int buttonCost = 2;
        int wrappersForButton = 2;
        int eatenButtons = 5;
        Assert.assertEquals(eatenButtons, countButtons(money, buttonCost, wrappersForButton));
    }

    @Test
    public void test4() {
        int money = 7;
        int buttonCost = 3;
        int wrappersForButton = 2;
        int eatenButtons = 3;
        Assert.assertEquals(eatenButtons, countButtons(money, buttonCost, wrappersForButton));
    }

}
