package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Napisz metodę liczącą największy wspólny dzielnik dla dwóch liczb
public class Exercise_NWD {

    private int nwd(int x, int y) {
        int a = x;
        int b = y;
        int mod = a % b;
        if (mod == 0) {
            return b;
        } else {
            a = b;
            b = mod;
            return nwd(a, b);
        }
    }


    @Test
    public void test1() {
        int x = 15;
        int y = 20;
        int nwd = 5;
        Assert.assertEquals(nwd, nwd(x, y));
    }

    @Test
    public void test2() {
        int x = 282;
        int y = 78;
        int nwd = 6;
        Assert.assertEquals(nwd, nwd(x, y));
    }


}
