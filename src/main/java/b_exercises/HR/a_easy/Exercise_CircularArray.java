package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

//Mając podaną tablicę A liczb przesuwaj liczby o K pozycji.
//Zwróć tablicę wyników bazując na otrzymanej tablicy indeksów Q
//Przykład:
//A=[5,8,9]  K=2, Q=[1,0,2]
//Po dwóch przesunięciach tablicy A otrzymujemy tablicę
//B=[8,9,5] bazując na indeksach z tablicy otrzymujemy wynik 9,8,5
public class Exercise_CircularArray {

    private List<Integer> getIndexedResult(List<Integer> result, List<Integer> indexes) {
        List indexedResult = new LinkedList();
        for (Integer index : indexes) {
            indexedResult.add(result.get(index));
        }
        System.out.println("Wynik końcowy: ");
        System.out.println(indexedResult);
        return indexedResult;
    }

    public List rotateArray(List<Integer> input, int rotation, List<Integer> indexes) {
        List rotatedResult = input;
        System.out.println("Ustawienie początkowe: ");
        System.out.println(rotatedResult);
        int realRotation = rotation % input.size();
        for (int r = 1; r <= realRotation; r++) {
            System.out.println("Runda: " + r);
            int lastIndex = rotatedResult.size()-1;
            int temp = (int) rotatedResult.get(lastIndex);
            rotatedResult.remove(lastIndex);
            rotatedResult.add(0, temp);
            System.out.println(rotatedResult);
        }

        return getIndexedResult(rotatedResult, indexes);
    }


    @Test
    public void test1() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 2, 3));
        int rotation = 2;
        ArrayList<Integer> indexes = new ArrayList<>(Arrays.asList(0, 1, 2));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(2, 3, 1));
        Assert.assertEquals(expectedResult, rotateArray(input, rotation, indexes));
    }


    @Test
    public void test2() {
        ArrayList<Integer> input = new ArrayList<>(List.of(5, 8, 9));
        int rotation = 2;
        ArrayList<Integer> indexes = new ArrayList<>(List.of(1, 0, 2));
        ArrayList<Integer> expectedResult = new ArrayList<>(List.of(9, 8, 5));
        Assert.assertEquals(expectedResult, rotateArray(input, rotation, indexes));
    }

    @Test
    public void test3() {
        ArrayList<Integer> input = new ArrayList<>(List.of(0));
        int rotation = 2;
        ArrayList<Integer> indexes = new ArrayList<>(List.of(0));
        ArrayList<Integer> expectedResult = new ArrayList<>(List.of(0));
        Assert.assertEquals(expectedResult, rotateArray(input, rotation, indexes));
    }


    @Test
    public void test4() {
        ArrayList<Integer> input = new ArrayList<>(List.of(9, 5, 0, 8, 9));
        int rotation = 3;
        ArrayList<Integer> indexes = new ArrayList<>(List.of(1, 0, 2, 4, 4, 2, 3));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(8, 0, 9, 5, 5, 9, 9));
        Assert.assertEquals(expectedResult, rotateArray(input, rotation, indexes));
    }

    @Test
    public void test5() {
        ArrayList<Integer> input = new ArrayList<>(List.of(5, 8, 9));
        int rotation = 0;
        ArrayList<Integer> indexes = new ArrayList<>(List.of(1, 0, 2));
        ArrayList<Integer> expectedResult = new ArrayList<>(List.of(8, 5, 9));
        Assert.assertEquals(expectedResult, rotateArray(input, rotation, indexes));
    }

    @Test
    public void test6() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 2, 3));
        int rotation = 3;
        ArrayList<Integer> indexes = new ArrayList<>(Arrays.asList(0, 1, 2));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(1, 2, 3));
        Assert.assertEquals(expectedResult, rotateArray(input, rotation, indexes));
    }

    @Test
    public void test7() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 2, 3));
        int rotation = 999999;
        ArrayList<Integer> indexes = new ArrayList<>(Arrays.asList(0, 1, 2));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(1, 2, 3));
        Assert.assertEquals(expectedResult, rotateArray(input, rotation, indexes));
    }

    @Test
    public void test8() {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(1, 2, 3));
        int rotation = 100000;
        ArrayList<Integer> indexes = new ArrayList<>(Arrays.asList(0, 1, 2));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(3, 1, 2));
        Assert.assertEquals(expectedResult, rotateArray(input, rotation, indexes));
    }

}
