package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

//Mając tablicę liczb int[] A znajdź takie liczby X, dla których każdy element jest podzielny przez to X
//Oraz takie gdzie każde x jest dzielnikiem dla wszystkich liczb z tablicy int[] B
//Przykład:
//A=[2, 6]
//B=[24, 36]
//X = 6 i 12 ponieważ dzielą się ona przez wszystkie liczby z tablicy A
//Oraz każdy z X dzieli wszystkie liczby z tablicy B

public class Exercise_05 {

    //Sprawdza czy liczba jest podzielna przez każdą ze zbioru
    private boolean checkIfAllAreFactors(int number, ArrayList<Integer> input) {
        boolean isFactor = true;
        for (Integer y : input) {
            if (number % y != 0) {
                isFactor = false;
                break;
            }
        }
        return isFactor;
    }

    //Sprawdza czy liczba dzieli wszystkie liczby ze zbioru
    private boolean checkIsFactorForAll(int number, ArrayList<Integer> input) {
        boolean isFactor = true;
        for (Integer y : input) {
            if (y % number != 0) {
                isFactor = false;
                break;
            }
        }
        return isFactor;
    }


    //Sprawdza czy spełnia wszystkie warunki dzielenia (Dzieli wszystko i jest dzielnikiem dla wszystkich)
    private boolean isCorrect(int number, ArrayList<Integer> inputA, ArrayList<Integer> inputB) {
        return (checkIfAllAreFactors(number, inputA) && checkIsFactorForAll(number, inputB));
    }

    //Sprawdza czy liczba jest unikalna ze znalezionych
    private boolean isUnique(int number, ArrayList<Integer> output) {
        return !output.contains(number);
    }

    //Mnoży między sobą wszystkie kombinacje liczb wystepujących w zbiorze, aby uzyskać ich wielokrotności
    private ArrayList<Integer> multiplyAll(int number, ArrayList<Integer> input) {
        ArrayList<Integer> output = new ArrayList<>();
        int multiply = number;
        for (int i = 0; i < input.size(); i++) {
            //Mnożenie dwóch takich samych liczb nie ma sensu
            if (number == input.get(i)) {
                continue;
            } else {
                //Przemnaża liczbę n przez wszystkie n+1, n+2..
                multiply *= input.get(i);
                //Sprawdza czy nowa liczba jest większa od największej liczby z tego zbioru
                // a = [2,3,8] 2*3 < 8
                if (multiply > input.get(input.size() - 1)) {
                    output.add(multiply);
                }
            }
        }
        return output;
    }
    //Kandydaci na właściwe liczby są dodawane do listy jeśli spełniajaw wymogi.
    //Najpierw poddaje się każdą z nich weryfikacji na poprawność i unikalność
    private void addToCollection(ArrayList<Integer> newInput,  ArrayList<Integer> inputA, ArrayList<Integer> inputB, ArrayList<Integer> output){
        for (Integer i: newInput) {
            if (isCorrect(i, inputA,inputB) && isUnique(i, output)) {
                output.add(i);
                System.out.println(String.format("Dodano %s do zbioru: %s", i, output));
            }
        }
    }

    //Gdy wyczerpiemy zestaw liczb z przemnożenia wszystkich ze zbioru A
    //pozostaje jeszcze sprawdzić wielokrotności liczba "potwierdzonych" wzglęem zbioru B
    //Wytypowuje wszystkie wielokrotności dla znalezionych wcześniej odpowiedzi
    //X=12  B=[48,96] => 24, 48
    private ArrayList<Integer> findBetween(int lowestB, ArrayList<Integer> output) {
        ArrayList<Integer> newoutput = new ArrayList<>();
        for (Integer i : output) {
            for (int j = 2; j <= lowestB/i; j++) {
                newoutput.add(i*j);
            }
        }
        return newoutput;
    }

    //Główna metoda zwracająca wszystkie liczby wymagane w zadaniu
    private ArrayList<Integer> getTotalX(ArrayList<Integer> inputA, ArrayList<Integer> inputB) {
        ArrayList<Integer> output = new ArrayList<>();
        //Sortuje obydwie kolekcje aby mieć pewność kolejności
        Collections.sort(inputA);
        Collections.sort(inputB);
        //Indywidualnie sprawdzana jest największa liczba ze zbioru A
        //Jest dobrym kandydatem jeśli dzielą się przez nią wszystkie pozostałe liczby ze zbioru A
        int theBiggest = inputA.get(inputA.size() - 1);
        if (isCorrect(theBiggest, inputA, inputB) && isUnique(theBiggest, output)) {
            output.add(inputA.get(inputA.size() - 1));
        }

        //Przemnaża ze sobą wszystkie liczby ze zbioru A tworzą kombinacje wielokrotności
        //Wypluwając podzbiory poddawane są one analizie i umieszczone w wynikach gdy przejdą weryfikację
        for (int i = 0; i < inputA.size(); i++) {
            addToCollection(multiplyAll(inputA.get(i), inputA), inputA, inputB, output);    ;
        }
        //W tym miejscu wyszukiwane sa pozostałe liczby które są wielokrotnościami znalezionych już wyników
        //oraz wciąż spełniają wymagania
        addToCollection(findBetween(inputB.get(0), output), inputA,inputB, output);

        Collections.sort(output);
        return output;
    }

    @Test
    public void test1() {
        ArrayList<Integer> inputA = new ArrayList<>(Arrays.asList(2, 6));
        ArrayList<Integer> inputB = new ArrayList<>(Arrays.asList(36, 24));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(6, 12));
        Assert.assertEquals(expectedResult, getTotalX(inputA, inputB));
    }

    @Test
    public void test2() {
        ArrayList<Integer> inputA = new ArrayList<>(Arrays.asList(2, 3, 6));
        ArrayList<Integer> inputB = new ArrayList<>(Arrays.asList(36, 24));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(6, 12));
        Assert.assertEquals(expectedResult, getTotalX(inputA, inputB));
    }

    @Test
    public void test3() {
        ArrayList<Integer> inputA = new ArrayList<>(Arrays.asList(3, 4));
        ArrayList<Integer> inputB = new ArrayList<>(Arrays.asList(24, 48));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(12, 24));
        Assert.assertEquals(expectedResult, getTotalX(inputA, inputB));
    }

    @Test
    public void test4() {
        ArrayList<Integer> inputA = new ArrayList<>(Arrays.asList(2, 4));
        ArrayList<Integer> inputB = new ArrayList<>(Arrays.asList(16, 32, 96));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(4, 8, 16));
        Assert.assertEquals(expectedResult, getTotalX(inputA, inputB));
    }

    @Test
    public void test5() {
        ArrayList<Integer> inputA = new ArrayList<>(Arrays.asList(2, 3, 5));
        ArrayList<Integer> inputB = new ArrayList<>(Arrays.asList(120, 360, 600));
        ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(30, 60, 120));
        Assert.assertEquals(expectedResult, getTotalX(inputA, inputB));
    }

}
