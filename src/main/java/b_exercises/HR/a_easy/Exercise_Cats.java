package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Dwa koty oraz mysz znajdują się w pewnych punktach na linii (x, y z)
//Określ który kot jest bliżej myszy
//Jesli pierwszy kot to zwróć "Cat A"
//Jeśli kot drugi to zwróć "Cat B"
//Jeśli będą w tej samej odległości to będę ze sobą walczyć i mysz ucieknie
//Wtedy zwróć "Mouse C";
public class Exercise_Cats {
    final String A = "Cat A";
    final String B = "Cat B";
    final String C = "Mouse C";

    private String catchMouse(int x, int y, int z) {
        String result = null;
        if (Math.abs(z - x) == Math.abs(z - y)) {return C;}
        if (Math.abs(z - x) < Math.abs(z - y)) {return A;}
        if (Math.abs(z - x) > Math.abs(z - y)) {return B;}
        return result;
    }

    @Test
    public void test1() {
        int a = 2;
        int b = 5;
        int c = 3;
        Assert.assertEquals(A, catchMouse(a, b, c));
    }

    @Test
    public void test2() {
        int a = 2;
        int b = 5;
        int c = 4;
        Assert.assertEquals(B, catchMouse(a, b, c));
    }

    @Test
    public void test3() {
        int a = 2;
        int b = 6;
        int c = 4;
        Assert.assertEquals(C, catchMouse(a, b, c));
    }


    @Test
    public void test4() {
        int a = 2;
        int b = 2;
        int c = 10;
        Assert.assertEquals(C, catchMouse(a, b, c));
    }

    @Test
    public void test5() {
        int a = 2;
        int b = 3;
        int c = 1;
        Assert.assertEquals(A, catchMouse(a, b, c));
    }

    @Test
    public void test6() {
        int a = 2;
        int b = 3;
        int c = 4;
        Assert.assertEquals(B, catchMouse(a, b, c));
    }

}
