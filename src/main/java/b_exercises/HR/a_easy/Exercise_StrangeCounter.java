package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Dziwny licznik zaczyna od 1 sekundy ale wartości 3.
//Każda kolejna sekunda więcej to o 1 wartość mniej: t++ -> v--
//Gdy wartość dojdzie do 1 to kolejna sekunda rozpoczyna się z podwojoną wartością początkową z poprzedniej serii.
//Dalsze odliczanie działa analogicznie
public class Exercise_StrangeCounter {

    private long weirdCounter(long finish) {
        long value = 3;
        long timer = 1;
        while (timer + value <= finish) {
            timer += value;
            value *=2;
        }
        value -= finish - timer;
        System.out.println(value);
        return value;
    }

    @Test
    public void test0A() {
        int t = 1;
        int expected = 3;
        Assert.assertEquals(expected, weirdCounter(t));
    }

    @Test
    public void test0B() {
        int t = 2;
        int expected = 2;
        Assert.assertEquals(expected, weirdCounter(t));
    }

    @Test
    public void test1() {
        int t = 4;
        int expected = 6;
        Assert.assertEquals(expected, weirdCounter(t));
    }

    @Test
    public void test2() {
        int t = 17;
        int expected = 5;
        Assert.assertEquals(expected, weirdCounter(t));
    }


}
