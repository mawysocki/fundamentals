package b_exercises.HR.a_easy;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Szyfr Cezara
//Polega on na przesunięciu każdej litery w alfabecie o K pozycji
//Przykład K=3  Wiadomość: "BADA" -> EDGD"
//Zaszyfruj podaną wiadomość
public class Exercise_CaesarCipher {
    private List<String> getAlphabet() {
        return List.of("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
    }

    private String convert(String letter, int encryption) {
        List<String> alphabet = getAlphabet();
        int oldIndex = alphabet.indexOf(letter);
        int newIndex = (oldIndex + encryption) % alphabet.size();
        return alphabet.get(newIndex);
    }

    private String encrypt(String input, int encryption) {
        StringBuilder s = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            String letter = Character.toString(c);
            boolean isAlpha = StringUtils.isAlpha(letter);
            boolean isUpperCase = StringUtils.isAllUpperCase(letter);
            if (isAlpha) {
                letter = convert(letter.toLowerCase(), encryption);
                if (isUpperCase) {
                    letter = letter.toUpperCase();
                }
            }
            s.append(letter);


        }
        return s.toString();
    }

    @Test
    public void test0() {
        String input = "middle-Outz";
        int encryption = 0;
        String expectedMessage = "middle-Outz";
        Assert.assertEquals(expectedMessage, encrypt(input, encryption));
    }

    @Test
    public void test1() {
        String input = "middle-Outz";
        int encryption = 2;
        String expectedMessage = "okffng-Qwvb";
        Assert.assertEquals(expectedMessage, encrypt(input, encryption));
    }

    @Test
    public void test2() {
        String input = "Always-Look-on-the-Bright-Side-of-Life";
        int encryption = 5;
        String expectedMessage = "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj";
        Assert.assertEquals(expectedMessage, encrypt(input, encryption));
    }

    @Test
    public void test3() {
        String input = "!@#$%^&*()ABC";
        int encryption = 2;
        String expectedMessage = "!@#$%^&*()CDE";
        Assert.assertEquals(expectedMessage, encrypt(input, encryption));
    }
    @Test
    public void test4() {
        String input = "!@#$%^&*()ABC";
        int encryption = 54;
        String expectedMessage = "!@#$%^&*()CDE";
        Assert.assertEquals(expectedMessage, encrypt(input, encryption));
    }

    @Test
    public void test5() {
        String input = "ABCdefGHI";
        int encryption = 79;
        String expectedMessage = "BCDefgHIJ";
        Assert.assertEquals(expectedMessage, encrypt(input, encryption));
    }

    @Test
    public void test6() {
        String input = "1X7T4VrCs23k4vv08D6yQ3S19G4rVP188M9ahuxB6j1tMGZs1m10ey7eUj62WV2exLT4C83zl7Q80M";
        int encryption = 79;
        String expectedMessage = "1Y7U4WsDt23l4ww08E6zR3T19H4sWQ188N9bivyC6k1uNHAt1n10fz7fVk62XW2fyMU4D83am7R80N";
        Assert.assertEquals(expectedMessage, encrypt(input, encryption));
    }
}
