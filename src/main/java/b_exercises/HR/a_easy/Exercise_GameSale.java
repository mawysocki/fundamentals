package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Sklep z grami wprowadził promocję:
//Za pierszą grę płącisz P. Druga gra jest tańsza o pierwszej o D
//Za każdą kolejną płacisz mniej póki nie osiągniesz ceny M.
//Oblicz ile gier mozesz kupić posiadając budżet S.
public class Exercise_GameSale {

    private int buyGames(int budget, int normalPrice, int discount, int minPrice) {
        int boughtGames = 0;
        int currentPrice = normalPrice;
        while (budget >= currentPrice) {
            budget -= currentPrice;
            boughtGames++;
            if (currentPrice - discount >= minPrice) {
                currentPrice -= discount;
            }
            else {
                currentPrice = minPrice;
            }

        }
        return boughtGames;
    }

    @Test
    public void test1() {
        int budget = 70;
        int normalPrice = 20;
        int discount = 3;
        int minPrice = 6;
        int expetedNumber = 5;
        Assert.assertEquals(expetedNumber, buyGames(budget, normalPrice, discount, minPrice));
    }

    @Test
    public void test2() {
        int budget = 80;
        int normalPrice = 20;
        int discount = 3;
        int minPrice = 6;
        int expetedNumber = 6;
        Assert.assertEquals(expetedNumber, buyGames(budget, normalPrice, discount, minPrice));
    }

    @Test
    public void test3() {
        int budget = 85;
        int normalPrice = 20;
        int discount = 3;
        int minPrice = 6;
        int expetedNumber = 7;
        Assert.assertEquals(expetedNumber, buyGames(budget, normalPrice, discount, minPrice));
    }


}
