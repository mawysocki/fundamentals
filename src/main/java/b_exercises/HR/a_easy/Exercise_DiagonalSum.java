package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Mając dwuwymiarową tablicę liczb policz wartość bezwzględną z różnicy pomiędzy sumą liczb na obydwu diagonalach
//Przykład:
//1 2 3
//4 5 6
//9 8 9
//Diagonala1 = 1 + 5 + 9 = 15.
//Diagonala2 = 3 + 5 + 9 = 17.
//Wartość bewzględna z różnicy |15-17| = 2.

public class Exercise_DiagonalSum {
    public int diagonalDifference(List<List<Integer>> arr) {
        int d1 = 0;
        int d2 = 0;
        for (int i = 0; i < arr.size(); i++) {
            d1 += arr.get(i).get(i);
        }
        for (int i = 0; i < arr.size(); i++) {
            d2 += arr.get(i).get(arr.size()-i-1);
        }
        return Math.abs(d1-d2);
    }

    @Test
    public void test1() {
        List<Integer> first = new ArrayList<>(Arrays.asList(11, 2, 4));
        List<Integer> second = new ArrayList<>(Arrays.asList(4, 5, 6));
        List<Integer> third = new ArrayList<>(Arrays.asList(10, 8, -12));
        List<List<Integer>> arr = new ArrayList<>(Arrays.asList(first, second, third));
        int expectedResult = 15;
        Assert.assertEquals(expectedResult, diagonalDifference(arr));
    }

}
