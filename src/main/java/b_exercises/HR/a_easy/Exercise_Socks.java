package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

//Mamy zbiór skarpetek w postaci listy, które muszą być sparowane według kolorów.
//Każda liczba to inny kolor.
//Napisz metodę która zwróci liczbę możliwych par
//Przykład ar = [1, 2, 1, 2, 1, 3, 2]
//Mamy 3 skarpety o kolorze 1, więc powstanie z nich para
//Mamy 3 skarpetki o koloerze 2, więc powstanie z nich para
//Mamy 1 skarpetkę o kolorze 3, więc nie będzie pary
//Mamy dwie pary oraz po jednej skarpetce o kolorach: 1, 2, 3
//Opowiedź: 2

public class Exercise_Socks {

    int pairs = 0;
    public int sockMerchant(List<Integer> ar) {
        for (int i = 1; i < ar.size(); i++) {
            if (ar.get(0).equals(ar.get(i))) {
                pairs++;
                ar.remove(i);
                ar.remove(0);
                break;
            }
            if (i == ar.size() - 1) {
                ar.remove(0);
            }
        }
        if (ar.size() <= 1) {
            return pairs;
        } else {
            return sockMerchant(ar);
        }
    }

    @Test
    public void test1() {
        List<Integer> socks = new ArrayList<>(Arrays.asList(1, 2, 1, 2));
        int expectedPairs = 2;
        Assert.assertEquals(expectedPairs, sockMerchant(socks));
    }

    @Test
    public void test2() {
        List<Integer> socks = new ArrayList<>(Arrays.asList(1, 2, 1, 2, 1, 2, 3));
        int expectedPairs = 2;
        Assert.assertEquals(expectedPairs, sockMerchant(socks));

    }

    @Test
    public void test3() {
        List<Integer> socks = new ArrayList<>(Arrays.asList(10, 20, 20, 10, 10, 30, 50, 10, 20));
        int expectedPairs = 3;
        Assert.assertEquals(expectedPairs, sockMerchant(socks));

    }

    @Test
    public void test4() {
        List<Integer> socks = new ArrayList<>(Arrays.asList(1, 1, 3, 1, 2, 1, 3, 3, 3, 3));
        int expectedPairs = 4;
        Assert.assertEquals(expectedPairs, sockMerchant(socks));

    }
}

