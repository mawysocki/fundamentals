package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Alice i Bob tworzą swoje projekty, które będą oceniane przez sędziego w 3 kategoriach od 1 do 100
//Każde z nich dostanie 3 oceny tworząc listę 3 elementową
//Alice i Bob konkurują ze sobą przyznając sobie punkty zwycięstwa jeśli ktoś jest lepszy w danej kateogrii. Gdy jest remis to ignorujemy
//Napisz metodę zwracające wynik rywalizacji w postaci listy dwuelementowej
//gdzie Alice jest na pierwszym miejscu, a Bob na drugim
//Przykład alice = [10, 5, 1] bob = [2, 3, 2]  result = [2, 1] -> 10>2, 5>3, 1<2

public class Exercise_AliceBobCompetition {

    private List<Integer> compareTriples(List<Integer> alice, List<Integer> bob) {
        // Write your code here
        int alicePoints = 0;
        int bobPoints = 0;
        for (int i=0; i< alice.size(); i++) {
            if (alice.get(i) == bob.get(i)) {
                continue;
            } else if (alice.get(i) > bob.get(i)) {
                alicePoints++;
            } else {
                bobPoints++;
            }
        }
        return new ArrayList<>(Arrays.asList(alicePoints, bobPoints));
    }

    @Test
    public void test1() {
        List<Integer> alice = new ArrayList<>(Arrays.asList(1, 2, 3));
        List<Integer> bob = new ArrayList<>(Arrays.asList(3, 2, 1));
        List<Integer> expectedResult = new ArrayList<>(Arrays.asList(1, 1));
        Assert.assertEquals(expectedResult, compareTriples(alice, bob));
    }

    @Test
    public void test2() {
        List<Integer> alice = new ArrayList<>(Arrays.asList(10, 2, 30));
        List<Integer> bob = new ArrayList<>(Arrays.asList(30, 2, 30));
        List<Integer> expectedResult = new ArrayList<>(Arrays.asList(0, 1));
        Assert.assertEquals(expectedResult, compareTriples(alice, bob));
    }

    @Test
    public void test3() {
        List<Integer> alice = new ArrayList<>(Arrays.asList(10, 2, 30));
        List<Integer> bob = new ArrayList<>(Arrays.asList(10, 2, 30));
        List<Integer> expectedResult = new ArrayList<>(Arrays.asList(0, 0));
        Assert.assertEquals(expectedResult, compareTriples(alice, bob));
    }

    @Test
    public void test4() {
        List<Integer> alice = new ArrayList<>(Arrays.asList(10, 2, 30));
        List<Integer> bob = new ArrayList<>(Arrays.asList(30, 21, 33));
        List<Integer> expectedResult = new ArrayList<>(Arrays.asList(0, 3));
        Assert.assertEquals(expectedResult, compareTriples(alice, bob));
    }

    @Test
    public void test5() {
        List<Integer> alice = new ArrayList<>(Arrays.asList(40, 22, 39));
        List<Integer> bob = new ArrayList<>(Arrays.asList(30, 21, 33));
        List<Integer> expectedResult = new ArrayList<>(Arrays.asList(3, 0));
        Assert.assertEquals(expectedResult, compareTriples(alice, bob));
    }


}
