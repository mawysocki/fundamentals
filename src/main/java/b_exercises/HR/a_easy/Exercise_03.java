package b_exercises.HR.a_easy;

import org.junit.Test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Mając tablice liczb całkowitych policz jaką częścią zbioru są liczby dodatnie, ujemne, a ile zero
//Wydrukuj wynik z 6 miejscami po przecinku
public class Exercise_03 {

    public double[] plusMinus(List<Integer> arr) {
        double[] ratio = new double[3];
        for (Integer number : arr) {
            if (number == 0) {
                ratio[2]++;
            } else {
                if (number > 0) {
                    ratio[0]++;
                } else {
                    ratio[1]++;
                }
            }
        }
        for (int i = 0; i < ratio.length; i++) {
            ratio[i] /= arr.size();
        }
        return ratio;
    }

    public void printResults(double[] ratio) {
        DecimalFormat f = new DecimalFormat("0.000000");
        System.out.println(f.format(ratio[0]));
        System.out.println(f.format(ratio[1]));
        System.out.println(f.format(ratio[2]));
    }

    @Test
    public void test1() {
        List<Integer> list = new ArrayList<>(Arrays.asList(-4, 3, -9, 0, 4, 1));
        printResults(plusMinus(list));
    }
}
