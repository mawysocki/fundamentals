package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//Given an array of integers, find the longest subarray
//where the absolute difference between any two elements
//is less than or equal to 1.
//input: 1 2 2 3 1 2
//output: 5
public class Exercise_10 {
    public int findArraySize(List<Integer> input) {
        Collections.sort(input);
        displaySorted(input);
        int min = Integer.MIN_VALUE + 1;
        int max = Integer.MIN_VALUE + 2;
        List<List<Integer>> myList = new ArrayList<>();
        int index = -1;
        for (Integer i : input) {
            int abs = Math.abs(i - max);
            System.out.println("abs: " + abs);
            if (abs > 1) {
                if (myList.size() - index == 2) {
                    index++;
                }
                index++;
                min = i;
                max = i;
                myList.add(new ArrayList<>());

                myList.get(index).add(i);
            }
            if (abs == 1) {
                myList.add(new ArrayList<>());
                if (max == min) {
                    myList.get(index).add(i);
                    myList.get(index + 1).add(i);
                    max = i;
                } else {
                    index++;
                    min = max;
                    max = i;
                    myList.get(index).add(i);
                    myList.get(index + 1).add(i);

                }
            }
            if (abs == 0) {
                myList.get(index).add(i);
                if (max != min) {
                    myList.get(index + 1).add(i);
                }

            }

            System.out.println(i);
        }
        System.out.println("min: " + min);
        System.out.println("max: " + max);
        int theBiggest = 0;
        for (List<Integer> integers : myList) {
            if (integers.size() > theBiggest) {
                theBiggest = integers.size();
                displaySorted(integers);
            }
        }
        System.out.println("The biggest: " + theBiggest);
        return theBiggest;
    }

    private void displaySorted(List<Integer> sortedList) {
        System.out.println("Full list: ");
        for (Integer integer : sortedList) {
            System.out.print(integer + " ");
        }
        System.out.println();
    }

    @Test
    public void test00() {
        ArrayList<Integer> inputA = new ArrayList<>();
        int expectedResult = 0;
        Assert.assertEquals(expectedResult, findArraySize(inputA));
    }

    @Test
    public void test0() {
        ArrayList<Integer> inputA = new ArrayList<>(Collections.singletonList(0));
        int expectedResult = 1;
        Assert.assertEquals(expectedResult, findArraySize(inputA));
    }

    @Test
    public void test1() {
        ArrayList<Integer> inputA = new ArrayList<>(Arrays.asList(1, 1, 1, 1));
        int expectedResult = 4;
        Assert.assertEquals(expectedResult, findArraySize(inputA));
    }

    @Test
    public void test2() {
        ArrayList<Integer> inputB = new ArrayList<>(Arrays.asList(4, 6, 5, 3, 3, 1));
        int expectedResult = 3;
        Assert.assertEquals(expectedResult, findArraySize(inputB));
    }

    @Test
    public void test3() {
        ArrayList<Integer> inputC = new ArrayList<>(Arrays.asList(1, 2, 2, 3, 1, 2));
        int expectedResult = 5;
        Assert.assertEquals(expectedResult, findArraySize(inputC));
    }

    @Test
    public void test4() {
        ArrayList<Integer> inputD = new ArrayList<>(Arrays.asList(9, 8, 6, 8, 7, 3, 3, 3, 6, 7, 8, 8));
        int expectedResult = 6;
        Assert.assertEquals(expectedResult, findArraySize(inputD));
    }

    @Test
    public void test5A() {
        ArrayList<Integer> inputD = new ArrayList<>(Arrays.asList(4, 4, 5, 5, 97, 97, 97));
        int expectedResult = 4;
        Assert.assertEquals(expectedResult, findArraySize(inputD));
    }

    @Test
    public void test5() {
        ArrayList<Integer> inputD = new ArrayList<>(Arrays.asList(4, 97, 5, 97, 97, 4, 97, 4, 97, 97, 97, 97, 4, 4, 5, 5, 97, 5, 97, 99, 4, 97, 5, 97, 97, 97, 5, 5, 97, 4, 5, 97, 97, 5, 97, 4, 97, 5, 4, 4, 97, 5, 5, 5, 4, 97, 97, 4, 97, 5, 4, 4, 97, 97, 97, 5, 5, 97, 4, 97, 97, 5, 4, 97, 97, 4, 97, 97, 97, 5, 4, 4, 97, 4, 4, 97, 5, 97, 97, 97, 97, 4, 97, 5, 97, 5, 4, 97, 4, 5, 97, 97, 5, 97, 5, 97, 5, 97, 97, 97));
        int expectedResult = 50;
        Assert.assertEquals(expectedResult, findArraySize(inputD));
    }
}
