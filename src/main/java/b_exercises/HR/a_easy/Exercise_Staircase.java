package b_exercises.HR.a_easy;

//Za pomocą # narysuj schodki o wysokości zależnej od podanego piętra

public class Exercise_Staircase {

    private static void printStairs(int floors) {
        for (int i = 1; i <= floors; i++) {
            for (int j = 1; j <= floors-i; j++) {
                System.out.print(" ");
            }
            for (int j = 1; j<= i; j++) {
                System.out.print("#");
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        printStairs(7);
    }
}
