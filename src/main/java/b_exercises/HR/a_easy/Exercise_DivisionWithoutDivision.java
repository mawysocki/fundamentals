package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Napisz działanie które dzieli dwie liczby całkowite nie używając dzielenia, mnożenia ani modulo
public class Exercise_DivisionWithoutDivision {
    private int divide(int a, int b) {
        int result = 0;
        int signA = a >= 0 ? 1 : -1;
        int signB = b >= 0 ? 1 : -1;
        a *= signA;
        b *= signB;
        while (a > b) {
            result++;
            a -= b;
        }
         return result * signA * signB;
    }

    private int divideRecursion(int a, int b) {
        int count = 0;
        int signA = a >= 0 ? 1 : -1;
        int signB = b >= 0 ? 1 : -1;
        a *= signA;
        b *= signB;
        if (a >= b) {
            count++;
            count += divideRecursion(a-b, b);
        }
        return count * signA * signB;
    }
    @Test
    public void test1A() {
        int expected = 2;
        Assert.assertEquals(expected, divide(5, 2));
    }
    @Test
    public void test1B() {
        int expected = 2;
        Assert.assertEquals(expected, divideRecursion(5, 2));
    }

    @Test
    public void test2A() {
        int expected = 4;
        Assert.assertEquals(expected, divide(13, 3));
    }
    @Test
    public void test2B() {
        int expected = 4;
        Assert.assertEquals(expected, divideRecursion(13, 3));
    }
    @Test
    public void test3A() {
        int expected = -2;
        Assert.assertEquals(expected, divide(-5, 2));
    }
    @Test
    public void test3B() {
        int expected = -2;
        Assert.assertEquals(expected, divideRecursion(-5, 2));
    }
    @Test
    public void test4A() {
        int expected = -5;
        Assert.assertEquals(expected, divide(-17, 3));
    }

    @Test
    public void test4B() {
        int expected = -5;
        Assert.assertEquals(expected, divideRecursion(-17, 3));
    }

    @Test
    public void test5A() {
        int expected = 0;
        Assert.assertEquals(expected, divide(-1, 3));
    }

    @Test
    public void test5B() {
        int expected = 0;
        Assert.assertEquals(expected, divideRecursion(-1, 3));
    }
    @Test
    public void test6A() {
        int expected = -3;
        Assert.assertEquals(expected, divide(10, -3));
    }

    @Test
    public void test6B() {
        int expected = -3;
        Assert.assertEquals(expected, divideRecursion(10, -3));
    }
    @Test
    public void test7A() {
        int expected = 3;
        Assert.assertEquals(expected, divide(-16, -5));
    }

    @Test
    public void test7B() {
        int expected = 3;
        Assert.assertEquals(expected, divideRecursion(-16, -5));
    }

}
