package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Sprawdź czy String A jest palindromem. Czyli czy jego zapis od przodu i tyłu jest taki sam
public class Exercise_Palindrome {

    private boolean isPalindrom(String text) {
        char[] chars = text.toUpperCase().toCharArray();
        int l = chars.length;
        boolean isPalindrom = true;
        for (int i = 0; i < l / 2; i++) {
            if (chars[i] != chars[l - i - 1])
                return false;

        }
        return isPalindrom;
    }

    public boolean isPalindrom2(String text) {
        return new StringBuilder(text).reverse().toString().equalsIgnoreCase(text.toLowerCase());
    }

    @Test
    public void test0A() {
        String text = "abba";
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, isPalindrom(text));
    }

    @Test
    public void test1A() {
        String text = "madam";
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, isPalindrom(text));
    }

    @Test
    public void test2A() {
        String text = "kajak";
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, isPalindrom(text));
    }

    @Test
    public void test3A() {
        String text = "Rats live on no evil star";
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, isPalindrom(text));
    }

    @Test
    public void test4A() {
        String text = "No palindrome";
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, isPalindrom(text));
    }

    @Test
    public void test0B() {
        String text = "abba";
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, isPalindrom2(text));
    }
    @Test
    public void test1B() {
        String text = "madam";
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, isPalindrom2(text));
    }

    @Test
    public void test2B() {
        String text = "kajak";
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, isPalindrom2(text));
    }

    @Test
    public void test3B() {
        String text = "Rats live on no evil star";
        boolean expectedResult = true;
        Assert.assertEquals(expectedResult, isPalindrom2(text));
    }

    @Test
    public void test4B() {
        String text = "No palindrome";
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, isPalindrom2(text));
    }

}
