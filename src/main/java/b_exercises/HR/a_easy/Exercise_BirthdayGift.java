package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Mamy za zadanie kupić prezenty w dwóch kolorach jak najtaniej
//Wiemy, że potrzeba B prezentów koloru czarnego i W prezentów koloru białego
//Prezenty typu B kosztują BC, natomiast prezenty typu W kosztują WC
//Prezenty można wymieniać na inny typ kosztem Z
//Policz jak najtaniej można kupić zestawy prezentowe
public class Exercise_BirthdayGift {

    public long buyGifts(int black, int white, int blackCost, int whiteCost, int convertCost) {
        long cost = 0;
        long blackTotal = 0;
        long whiteTotal = 0;
        if (Math.abs(blackCost-whiteCost) <= convertCost) {
            blackTotal = (long)black * blackCost;
            whiteTotal = (long)white * whiteCost;
            cost = blackTotal + whiteTotal;
        } else {
            if (blackCost > whiteCost) {
                blackTotal = (long)black * (whiteCost+convertCost);
                whiteTotal = (long)white * whiteCost;
                cost = blackTotal + whiteTotal;
            }
            else {
                blackTotal = (long)black * blackCost;
                whiteTotal = (long)white * (blackCost + convertCost);
                cost = blackTotal + whiteTotal;
            }
        }
        return cost;
    }

    @Test
    public void test01() {
        int black = 10;
        int white = 10;
        int blackCost = 1;
        int whiteCost = 1;
        int convertCost = 1;
        int expectedPrice = 20;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test02() {
        int black = 5;
        int white =9;
        int blackCost = 2;
        int whiteCost = 3;
        int convertCost = 4;
        int expectedPrice = 37;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test03() {
        int black = 3;
        int white =6;
        int blackCost = 9;
        int whiteCost = 1;
        int convertCost = 1;
        int expectedPrice = 12;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test04() {
        int black = 7;
        int white = 7;
        int blackCost = 4;
        int whiteCost = 2;
        int convertCost = 1;
        int expectedPrice = 35;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test05() {
        int black = 3;
        int white =3;
        int blackCost = 1;
        int whiteCost = 9;
        int convertCost = 2;
        int expectedPrice = 12;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test06() {
        int black = 3;
        int white =5;
        int blackCost = 3;
        int whiteCost = 4;
        int convertCost = 1;
        int expectedPrice = 29;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test07() {
        int black = 27984;
        int white =1402;
        int blackCost = 619246;
        int whiteCost = 615589;
        int convertCost = 247954;
        long expectedPrice = 18192035842L;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test08() {
        int black = 95677;
        int white = 39394;
        int blackCost = 86983;
        int whiteCost = 311224;
        int convertCost = 588538;
        long expectedPrice = 20582630747L;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test09() {
        int black = 68406;
        int white = 12718;
        int blackCost = 532909;
        int whiteCost = 315341;
        int convertCost = 201009;
        long expectedPrice = 39331944938L;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test10() {
        int black = 15242;
        int white = 95521;
        int blackCost = 712721;
        int whiteCost = 628729;
        int convertCost = 396706;
        long expectedPrice = 70920116291L;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test11() {
        int black = 21988;
        int white = 67781;
        int blackCost = 645748;
        int whiteCost = 353796;
        int convertCost = 333199;
        long expectedPrice = 38179353700L;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test12() {
        int black = 22952;
        int white = 80129;
        int blackCost = 502975;
        int whiteCost = 175136;
        int convertCost = 340236;
        long expectedPrice = 25577754744L;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test13() {
        int black = 38577;
        int white = 3120;
        int blackCost = 541637;
        int whiteCost = 657823;
        int convertCost = 735060;
        long expectedPrice = 22947138309L;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test14() {
        int black = 5943;
        int white = 69851;
        int blackCost = 674453;
        int whiteCost = 392925;
        int convertCost = 381074;
        long expectedPrice = 31454478354L;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test15() {
        int black = 62990;
        int white = 61330;
        int blackCost = 310144;
        int whiteCost = 312251;
        int convertCost = 93023;
        long expectedPrice = 38686324390L;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }

    @Test
    public void test16() {
        int black = 11152;
        int white = 43844;
        int blackCost = 788543;
        int whiteCost = 223872;
        int convertCost = 972572;
        long expectedPrice = 18609275504L;
        Assert.assertEquals(expectedPrice, buyGifts(black,white, blackCost, whiteCost, convertCost));
    }
}
