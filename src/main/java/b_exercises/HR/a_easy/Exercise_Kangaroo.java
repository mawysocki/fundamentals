package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Mamy 2 kangury, które skaczą po osi liczb naturalnych
//Każdy z nich posiada parametry: x=miejsce startowe, v=prędkość (stała)
//Napisz metodę która sprawdzi czy kangury skaczące po tyle samo razy są w stanie się kiedyś spotkać
//Przykład1:
//Kangur1 zaczyna w pozycji 2 i skacze z prędkością 1
//Kangur2 zaczyna w pozycji 1 i skacze z prędkością 2
//Po 1 skoku obydwa kangury znajdują się w pozycji 3, więc odpowiedź brzmi YES
//Przykład2:
//Kangur1 zaczyna w pozycji 0 i skacze z prędkością 2
//Kangur2 zaczyna w pozycji 5 i skacze z prędkością 3
//Kangur2 jest dalej względem kangura1 oraz ma większą prędkość, więc nigdy nie zostanie dogoniony. Opowiedź: NO

public class Exercise_Kangaroo {


    private String kangaroo(int x1, int v1, int x2, int v2) {
        int y1 = x1;
        int y2 = x2;
        if (x1 == x2 && v1 == v2) return "YES";
        if (x1 != x2 && v1 == v2) return "NO";

        if (v1 > v2) {
            while (y1 < y2) {
                y1 += v1;
                y2 += v2;
                if (y1 == y2) {
                    return "YES";
                }
            }
            return "NO";
        } else {
            while (y2 < y1) {
                y1 += v1;
                y2 += v2;
                if (y1 == y2) {
                    return "YES";
                }
            }
            return "NO";
        }
    }

    @Test
    public void test0() {
        String expectedResult = "YES";
        Assert.assertEquals(expectedResult, kangaroo(2, 4, 2, 4));
    }

    @Test
    public void test1() {
        String expectedResult = "NO";
        Assert.assertEquals(expectedResult, kangaroo(0, 4, 2, 4));
    }

    @Test
    public void test2() {
        String expectedResult = "YES";
        Assert.assertEquals(expectedResult, kangaroo(0, 3, 4, 2));
    }

    @Test
    public void test3() {
        String expectedResult = "NO";
        Assert.assertEquals(expectedResult, kangaroo(0, 2, 5, 3));
    }

    @Test
    public void test4() {
        String expectedResult = "YES";
        Assert.assertEquals(expectedResult, kangaroo(2, 1, 1, 2));
    }

}
