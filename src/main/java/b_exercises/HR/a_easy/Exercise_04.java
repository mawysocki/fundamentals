package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Mając zbiór 5 elementowy znajdź największą i najmniejszą sumę składającą się z 4 elementów tego zbioru
//Zwróć wynik w postaci tablicy dwuelementowej
//Przykład: arr = [1,3,5,7,9]  result = [16, 24]
public class Exercise_04 {

    private List<Integer> miniMaxSum(List<Integer> input) {
        int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
        int sum = 0;
        for (int i = 0; i < input.size(); i++) {
            sum += input.get(i);
            if (input.get(i) > max) { max = input.get(i); }
            if (input.get(i) < min) { min = input.get(i); }
        }
        return new ArrayList<>(Arrays.asList(sum-max, sum-min));
    }

    @Test
    public void test1() {
        List<Integer> input = new ArrayList<>(Arrays.asList(1, 3, 5, 7, 9));
        List<Integer> expectedResult = new ArrayList<>(Arrays.asList(16, 24));
        Assert.assertEquals(expectedResult, miniMaxSum(input));
    }

    @Test
    public void test2() {
        List<Integer> input = new ArrayList<>(Arrays.asList(1, 1, 1, 1, 2));
        List<Integer> expectedResult = new ArrayList<>(Arrays.asList(4, 5));
        Assert.assertEquals(expectedResult, miniMaxSum(input));
    }


}
