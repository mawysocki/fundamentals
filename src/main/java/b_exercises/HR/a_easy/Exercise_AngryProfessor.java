package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

//Profesor postanowić odwoływać zajęcia jeśli liczba studentów przybyłych na czas nie jest równa przynajmniej K
//Za spóźnionego studenta uważa się tego którego czas przybycia jest większy od 0
//Zweryfikuj zwracając "YES" albo "NO" w zależności od tego czy dane zajęcia zostaną odwołane
public class Exercise_AngryProfessor {

    public String checkStudents(int threshold, List<Integer> students) {
        int onTime = 0;
        for (Integer student : students) {
            if (student <= 0) {
                onTime++;
            }
        }
        if (onTime >= threshold) {
            return "NO";
        } else {
            return "YES";
        }
    }

    @Test
    public void test1() {
        String expectedValue = "YES";
        int threshold = 3;
        List<Integer> students = List.of(-1, -3, 4, 2);
        Assert.assertEquals(expectedValue, checkStudents(threshold, students));
    }

    @Test
    public void test2() {
        String expectedValue = "NO";
        int threshold = 2;
        List<Integer> students = List.of(0, -1, 2, 1);
        Assert.assertEquals(expectedValue, checkStudents(threshold, students));
    }


}
