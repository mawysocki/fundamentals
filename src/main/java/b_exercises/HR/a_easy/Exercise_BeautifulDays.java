package b_exercises.HR.a_easy;

import org.junit.Assert;
import org.junit.Test;

//Mając podany zakres dni od START do END sprawdź ile jest wyjątkowych dni w tym okresie
//Wyjątkowy dzień to taki gdy od niego odejmiemy jego odwrotność i podzielimy przez K
//Przykład START=19 END=21  k=6
//L1 = (19-91)/6 = -12 - tak
//L2 = (20-02)/6 = 3 - tak
//Ld = (21-12)/6 = 1,5 - nie
//Odpowiedź: 2
public class Exercise_BeautifulDays {

    public int findDays(int start, int end, int divisor) {
        int days = 0;
        for (int day = start; day <= end; day++) {
            days += isBeautiful(day, divisor);
        }
        return days;
    }

    private int isBeautiful(int day, int divisor) {
        int reversed = reverseNumber(day);

        boolean isBeautiful = (day-reversed) % divisor == 0;
        if (isBeautiful) {
            return 1;
        }
        return 0;
    }

    public int reverseNumber(int number) {  //597
        int dividedNumber = number;  //597
        int newNumber = 0;

        while (dividedNumber != 0) {
            newNumber *= 10; //0 70 790
            int modulo = dividedNumber % 10;  //7   9  5
            newNumber += modulo;  //7 79  795
            dividedNumber /= 10; //59 5  0
        }
        return newNumber;
    }

    @Test
    public void test() {
        int start = 20;
        int end = 23;
        int divisor = 6;
        int expectedDays = 2;
        Assert.assertEquals(expectedDays, findDays(start, end, divisor));
    }

    @Test
    public void test2() {
        int start = 13;
        int end = 45;
        int divisor = 3;
        int expectedDays = 33;
        Assert.assertEquals(expectedDays, findDays(start, end, divisor));
    }

    @Test
    public void test3() {
        int start = 1;
        int end = 10;
        int divisor = 5;
        int expectedDays = 9;
        Assert.assertEquals(expectedDays, findDays(start, end, divisor));
    }

    @Test
    public void test4() {
        int start = 1;
        int end = 2000000;
        int divisor = 1000000000;
        int expectedDays = 2998;
        Assert.assertEquals(expectedDays, findDays(start, end, divisor));
    }


}
