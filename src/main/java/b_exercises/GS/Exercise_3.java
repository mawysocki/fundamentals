package b_exercises.GS;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

//Find the first non-repeating character in an array.
public class Exercise_3 {

    public char getFirstNonRepeatingCharacter(char[] array) {
        //LinkedHashMap zachowuje kolejność wstawiania elementów
        LinkedHashMap<Character, Boolean> map = new LinkedHashMap<>();

        //Przejdź przez wszystkie znaki w tablicy
        for (char c : array) {
            //Jeśli znak nie był jeszcze użyty to nie istnieje w mapie
            if (!map.containsKey(c)) {
                //Dodaj znak do mapy z oznaczeniem true jako unikalny
                map.put(c, true);
                //Jeśli wszedł do else to znaczy, ze taki znak już występował
            } else {
                //Zmień mu flagę na false, bo znaczy, że nie jest unikalny
                map.put(c, false);
            }
        }

        //Przejdź po wszystkich kluczach z mapy
        //Klucze są w kolejności dodawania czyli według znaków w tablicy
        for (Character character : map.keySet()) {
            //Jeśli znajdzie wartość true dla jakiegoś klucza (znaku) to znaczy, że ten znak jest unikalny
            //Zwraca pierwszy znaleziony znak i kończy pętlę
            if (map.get(character)) {
                return character;
            }
        }
        return 'x';
    }

    @Test
    public void test1() {
        char[] array = {'a', 'b', 'c', 'a', 'b', 'd', 'a'};
        char result = 'c';
        Assert.assertEquals(result, getFirstNonRepeatingCharacter(array));
    }

    @Test
    public void test2() {
        char[] array = {'x', 'z', 'z', 'x', 'z', 's', 's', 'c', 'x', 'a', 'c'};
        char result = 'a';
        System.out.println(getFirstNonRepeatingCharacter(array));
        Assert.assertEquals(result, getFirstNonRepeatingCharacter(array));
    }
}
