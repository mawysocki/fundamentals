package b_exercises.GS;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

//Given array of students and their marks in different subjects.
// Find maximum average of the student. {“James”, “70”}, {“Fernando”, “90”}, {“Nick”, “60”}, {“James”, “10”}
public class Exercise_7 {

    private String findStudent(String[][] students) {
        HashMap<String, List<Integer>> degrees = new HashMap<>();
        for (String[] student: students) {
            if (degrees.containsKey(student[0])) {
                degrees.get(student[0]).add(Integer.valueOf(student[1]));
            }
            else {
                degrees.put(student[0], new ArrayList<>(Arrays.asList(Integer.valueOf(student[1]))));
            }
        }
        String theBest = "";
        int theHighest = 0;
        for (String name: degrees.keySet()) {
            List<Integer> value = degrees.get(name);
            int avg = 0;
            for (Integer integer : value) {
                avg += integer;
            }
            avg /= value.size();
            if (avg > theHighest) {
                theHighest = avg;
                theBest = name;
            }
        }
        return theBest;
    }

    @Test
    public void test1() {
        String[][] students = {{"James", "70"}, {"Fernando", "90"}, {"Rick", "60"}, {"James", "10"}, {"Fernando", "30"}};
        String expectedName = "Fernando";
        Assert.assertEquals(expectedName, findStudent(students));
    }

    @Test
    public void test2() {
        String[][] students = {{"James", "70"}, {"Fernando", "90"}, {"Rick", "60"}, {"James", "10"}, {"Fernando", "30"}, {"James", "90"}, {"James", "95"}};
        String expectedName = "James";
        Assert.assertEquals(expectedName, findStudent(students));
    }

}
