package b_exercises.GS;

import org.junit.Assert;
import org.junit.Test;

//In long text, given two words, find closest distance between them
public class Exercise_1 {
    // Function to calculate the minimum
// distance between w1 and w2 in s
    public int finddistance(String sentence, String word1, String word2) {
        int minDistance = Integer.MAX_VALUE;
        String[] words = sentence.toLowerCase() .split(" ");
        for (int i = 0; i < words.length; i++) {
            if (words[i].equals(word1.toLowerCase())) {
                int distance = findSecondWord(i, word2, words);
                if (distance < minDistance) {
                    minDistance = distance;
                }
            }
        }
        return minDistance;
    }

    private int findSecondWord(int indexWordA, String word, String[] words) {
        int minDistance = Integer.MAX_VALUE;
        for (int i = 0; i < words.length; i++) {
            if (words[i].equals(word.toLowerCase())) {
                int distance = Math.abs(indexWordA - i)-1;
                if (distance < minDistance) {
                    minDistance = distance;
                }
            }
        }
        return minDistance;
    }



    @Test
    public void test1() {
        String s = "geeks for geeks contribute practice";
        String w1 = "geeks";
        String w2 = "practice";
        int expectedDistance = 1;
        Assert.assertEquals(expectedDistance, finddistance(s, w1, w2));
    }

    @Test
    public void test2() {
        String s = "Wake me up in wake me up in Paris";
        String w1 = "wake";
        String w2 = "in";
        int expectedDistance = 0;
        Assert.assertEquals(expectedDistance, finddistance(s, w1, w2));
    }

    @Test
    public void test3() {
        String s = "abc ab ABC ab up in wake dd up ab DD";
        String w1 = "abc";
        String w2 = "dd";
        int expectedDistance = 4;
        Assert.assertEquals(expectedDistance, finddistance(s, w1, w2));
    }
}
