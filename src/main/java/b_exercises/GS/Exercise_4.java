package b_exercises.GS;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Exercise_4 {
    //    Form the largest possible number from the array of number
    public long createTheLargesNumber(int[] array) {
        StringBuilder number = new StringBuilder();
        Arrays.stream(array).boxed().sorted(Collections.reverseOrder()).forEach(num -> number.append(num));
        return Long.parseLong(String.valueOf(number));
    }

    public long createTheLargesNumber2(int[] array) {
        System.out.println(mySort(array));
        List<Integer> integers = mySort(array);
        return createNumber(integers);
    }

    private List<Integer> mySort(int[] array) {
        List<Integer> sorted = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            System.out.println("From array: " + array[i]);
            if (sorted.size() == 0) {
                System.out.println("First: " + array[i]);
                sorted.add(array[i]);
                continue;
            }
            for (int j = 0; j < sorted.size(); j++) {
                System.out.println(String.format("%s, %s, %s > %s = %s", i, j, array[i], sorted.get(j), array[i] > sorted.get(j)));
                if (array[i] > sorted.get(j)) {
                    sorted.add(j, array[i]);
                    break;
                }

                if (j == sorted.size() - 1) {
                    System.out.println(String.format("Add %s on the end", array[i]));
                    sorted.add(array[i]);
                    break;
                }
            }
            System.out.println(sorted);
        }
        return sorted;
    }

    private long createNumber(List<Integer> l) {
        long number = 0;
        for (int i = 0; i < l.size() ; i++) {
            number *= 10;
            number += l.get(i);

        }
        return number;
    }
    @Test
    public void test1A() {
        int[] array = {1, 4, 2, 9, 7, 8};
        long result = 987421;
        Assert.assertEquals(result, createTheLargesNumber(array));
    }

    @Test
    public void test2A() {
        int[] array = {1, 1, 2, 9, 9, 7, 2, 1};
        long result = 99722111;
        Assert.assertEquals(result, createTheLargesNumber(array));
    }

    @Test
    public void test1B() {
        int[] array = {1, 4, 2, 9, 7, 8};
        long result = 987421;
        Assert.assertEquals(result, createTheLargesNumber2(array));
    }

    @Test
    public void test2B() {
        int[] array = {1, 1, 2, 9, 9, 7, 2, 1};
        long result = 99722111;
        Assert.assertEquals(result, createTheLargesNumber2(array));
    }
}
