package b_exercises.GS;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Implement a function that returns positions of all the words that start with some prefix (not case sensitive) ( without using regex )
//example : { document : "aa aaa AaC a bb", prefix: "aa" } -> should return [0,3,7]
public class Exercise_9 {

    public List<Integer> findIndex(String text, String prefix) {
        ArrayList<Integer> indexes = new ArrayList<>();

        String[] words = text.toLowerCase().split(" ");

        int index = 0;
        for (String word : words) {
            if (word.startsWith(prefix.toLowerCase())) {
                indexes.add(index);
            }
            index += word.length();
            index++;
        }

        return indexes;
    }

    @Test
    public void test1() {
        String text = "aa aaa AaC a bb";
        String prefix = "aa";
        List<Integer> indexses = Arrays.asList(0, 3, 7);

        Assert.assertEquals(indexses, findIndex(text, prefix));
    }

    @Test
    public void test2() {
        String text = "cdf aaBa ABaC aB Abb bab aB";
        String prefix = "aB";
        List<Integer> indexses = Arrays.asList(9, 14, 17, 25);

        System.out.println(indexses);
        Assert.assertEquals(indexses, findIndex(text, prefix));
    }
}
