package b_exercises.GS;

import org.junit.Assert;
import org.junit.Test;

//Znajdź drugą najmniejszą liczbę z tablicy liczb
public class Exercise_11 {
    private int findNumber(int[] numbers) {
        int theSmallest = Integer.MAX_VALUE-1;
        int theSecondSmallest = Integer.MAX_VALUE;
        if (numbers.length < 2) {return 0;}

        for (int number: numbers) {
            if (number < theSmallest) {
                theSecondSmallest = theSmallest;
                theSmallest = number;
            } else if (number > theSmallest && number < theSecondSmallest) {
                theSecondSmallest = number;
            }
        }
        return theSecondSmallest;
    }

    @Test
    public void test1() {
        int[] numbers = {1, 2};
        int theSecondSmallest = 2;
        Assert.assertEquals(theSecondSmallest, findNumber(numbers));
    }
    @Test
    public void test2() {
        int[] numbers = {1};
        int theSecondSmallest = 0;
        Assert.assertEquals(theSecondSmallest, findNumber(numbers));
    }

    @Test
    public void test3() {
        int[] numbers = {};
        int theSecondSmallest = 0;
        Assert.assertEquals(theSecondSmallest, findNumber(numbers));
    }

    @Test
    public void test4() {
        int[] numbers = {0, 0, 1, 1};
        int theSecondSmallest = 1;
        Assert.assertEquals(theSecondSmallest, findNumber(numbers));
    }

    @Test
    public void test5() {
        int[] numbers = {2, 0, 2, 1, 0};
        int theSecondSmallest = 1;
        Assert.assertEquals(theSecondSmallest, findNumber(numbers));
    }

}
