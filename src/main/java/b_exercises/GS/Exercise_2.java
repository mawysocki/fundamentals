package b_exercises.GS;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class Exercise_2 {
//    In string find first letter that is unique.
//    Don't allocate dynamic memory.


    //Bruteforce - przejście po wszystkich elementach i porównanie każdego z każdym
    public char findUniqueLetterA(String text) {
        char[] txt = text.replaceAll(" ", "").toLowerCase().toCharArray();
        boolean isUnique = true;
        for (int i = 0; i < txt.length; i++) {
            isUnique = true;
            for (int j = i+1; j < txt.length; j++) {
                if (txt[i] == txt[j]) {
                    isUnique = false;
                    continue;
                }
            }
            if (isUnique) {
                return txt[i];
            };
        }
        return ' ';
    }

    //Przejście przez każdy element i rozdzielenie na kilka elementów według tego znaku
    //Jeśli podzieli się na 2 częsci to znaczy, ze jest tylko 1 znak
    public char findUniqueLetterB(String text) {
        char[] txt = text.replaceAll(" ", "").toLowerCase().toCharArray();
        for (char c : txt) {
            if (text.split(String.valueOf(c)).length == 2)
                return c;

        }
        return ' ';
    }

    //Zliczanie za pomocą ostatniego indeksu występowania danego znaku
    //Jeśli ostatni index jest równy aktualnemu indeksowi idąc od początku to znaczy, ze jest jedyny
    public char findUniqueLetterC(String text) {
        char[] txt = text.replaceAll(" ", "").toLowerCase().toCharArray();
        for (int i = 0; i < txt.length; i++) {
            if (text.lastIndexOf(txt[i]) == i)
                return txt[i];
        }

        return ' ';
    }

    //Zliczanie za pomocą zewnętrznej blibioteki z klasy StringUtils
    public char findUniqueLetterD(String text) {
        char[] txt = text.replaceAll(" ", "").toLowerCase().toCharArray();
        for (char c : txt) {
            if (StringUtils.countMatches(text, c) == 1)
                return c;

        }
        return ' ';
    }

    //Wartość ustawiona globalnie, aby można było modyfikować dla wszystkich testów na raz
    final String text = "My text is medium short";
    final char expectedLetter = 'y';

    //Testy dla 4 różnych implementacji rozwiązania
    @Test
    public void testA() {
        Assert.assertEquals(Character.toLowerCase(expectedLetter), findUniqueLetterA(text));
    }

    @Test
    public void testB() {
        Assert.assertEquals(Character.toLowerCase(expectedLetter), findUniqueLetterB(text));
    }

    @Test
    public void testC() {
        Assert.assertEquals(Character.toLowerCase(expectedLetter), findUniqueLetterC(text));
    }

    @Test
    public void testD() {
        Assert.assertEquals(Character.toLowerCase(expectedLetter), findUniqueLetterD(text));
    }
}
