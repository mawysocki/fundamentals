package b_exercises.CL.java_3_inheritance.exercise_6;

import b_exercises.CL.java_3_inheritance.exercise_4.Employee;

public class SalariedEmployee extends Employee {

    public static final double workingHours = 190;
    public SalariedEmployee(int id, String firstName, String lastName, double wage) {
        super(id, firstName, lastName, wage);
    }

    public double calculatePayment() {
        return getWage() * workingHours;
    }
}
