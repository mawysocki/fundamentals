package b_exercises.CL.java_3_inheritance.exercise_6;

import b_exercises.CL.java_3_inheritance.exercise_5.HourlyEmployee;
import org.junit.Assert;

//Stwórz klasę SalariedEmployee reprezentującą pracownika, z którym pracodawca ma umowę miesięczną. Klasa powinna:
//
//dziedziczyć po klasie Employee,
//mieć dodatkową metodę calculatePayment(), która będzie zwracała kwotę do wypłacenia pracownikowi za miesiąc
//Ddla uproszczenia możemy założyć, że w każdym miesiącu jest 190 godzin pracujących – użyj final static.
public class Runner {

    public static void main(String[] args) {
        double basicWage = 10;
        double raiseInPercent = 100;
        double expectedSalary = 3800;
        SalariedEmployee s1 = new SalariedEmployee(3, "Buggs", "Bunny", basicWage);
        s1.raiseWage(raiseInPercent);
        Assert.assertEquals(expectedSalary, s1.calculatePayment(), 0);

    }
}
