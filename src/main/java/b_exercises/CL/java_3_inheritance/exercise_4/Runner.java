package b_exercises.CL.java_3_inheritance.exercise_4;

import org.junit.Assert;

//Stwórz klasę Employee, która posiada:
//
//publiczne atrybuty:
//id – atrybut zawierający informację o numerze identyfikacyjnym pracownika,
//firstName – atrybut określający imię pracownika,
//lastName – atrybut określający nazwisko pracownika,
//wage – atrybut określający stawkę godzinową pracownika,
//konstruktor przyjmujący: id, imię, nazwisko i stawkę za godzinę,
//metodę raiseWage(percent), której rolą będzie zwiększenie wartości atrybutu wage o podany procent.
//Pamiętaj o sprawdzeniu czy podana wartość jest większa lub równa 0.
public class Runner {

    public static void main(String[] args) {
        double basicWage = 10;
        double raiseInPercent = 10;
        double expectedWage = 11;
        Employee e1 = new Employee(1, "Adam", "Newman", basicWage);
        e1.raiseWage(raiseInPercent);
        Assert.assertEquals(expectedWage, e1.getWage(),0);
    }

}
