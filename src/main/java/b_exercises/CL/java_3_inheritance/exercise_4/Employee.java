package b_exercises.CL.java_3_inheritance.exercise_4;

public class Employee {

    private final int id;
    private final String firstName;
    private final String lastName;



    private double wage;
    public Employee(int id, String firstName, String lastName, double wage) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.wage = wage;
    }

    public void raiseWage(double percent) {
        if (percent >= 0) {
            wage *= (1 + percent/100);
        }
    }

    public double getWage() {
        return wage;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
