package b_exercises.CL.java_3_inheritance.exercise_1;

import b_exercises.CL.java_3_constructor.exercise_2.Calculator;

public class AdvanceCalculator extends Calculator {

    public double pow(double num1, double num2) {
        double result = Math.pow(num1, num2);
        String message = String.format("%s ^ %s equals %s", num1, num2, result);
        addToHistory(message);
        return Math.pow(num1, num2);
    }

    public double root(double num1, double num2) {
        double result = Math.pow(num1, 1/num2);
        String message = String.format("%s root of %s equals %s", num1, num2, result);
        addToHistory(message);
        return Math.pow(num1, num2);
    }
}
