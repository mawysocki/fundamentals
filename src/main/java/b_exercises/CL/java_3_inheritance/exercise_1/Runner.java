package b_exercises.CL.java_3_inheritance.exercise_1;

import b_exercises.CL.java_3_constructor.exercise_2.Calculator;

//Stwórz klasę AdvancedCalculator, która dziedziczy po klasie Calculator. Klasa powinna implementować następujące metody:
//
//pow(num1, num2) – metoda ma zwracać num1 do potęgi num2. Dodatkowo w tablicy operacji ma zapamiętać napis: "num1^num2 equals result".
//root(num1, num2) – metoda ma wyliczyć pierwiastek num2 stopnia z num1. Dodatkowo w tablicy operacji ma zapamiętać napis: "num2 root of num1 equals result".
public class Runner {
    public static void main(String[] args) {
        AdvanceCalculator calc = new AdvanceCalculator();
        calc.pow(2,3);
        calc.root(8,3);
        calc.printOperations();
    }
}
