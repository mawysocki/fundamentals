package b_exercises.CL.java_3_inheritance.exercise_3;

import java.awt.*;

//Stwórz klasę Circle, która spełnia następujące wymogi:
//
//dziedziczy po klasie definiującej kształt (Shape),
//ma dodatkowy atrybut radius,
//posiada konstruktor, przyjmujący zmienne określające wartości x, y, color i radius,
//nadpisuje metodę kształtu – getDescription(),
//ma metodę o nazwie getArea(), zwracającą pole powierzchni typu double,
//posiada metodę o nazwie getCircuit(), zwracającą obwód typu double.
//Przetestuj metodę getDistance(Shape shape) na obiektach typu Circle.
public class Runner {
    public static void main(String[] args) {
        Circle circle = new Circle(0, 0, Color.RED, 10);
        double area = circle.getArea();
        double circuit = circle.getCircuit();
        System.out.println(circle.getDescription());
        System.out.println(String.format("Area: %s, circuit: %s", area, circuit));
    }
}
