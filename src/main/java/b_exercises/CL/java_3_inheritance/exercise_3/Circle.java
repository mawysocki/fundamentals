package b_exercises.CL.java_3_inheritance.exercise_3;

import b_exercises.CL.java_3_inheritance.exercise_2.Shape;

import java.awt.*;

public class Circle extends Shape {
    private double radius;
    public Circle(int x, int y, Color color, double radius) {
        super(x, y, color);
        this.radius = radius;
    }

    @Override
    public String getDescription() {
        return String.format("%s & radius: %s", super.getDescription(), radius);
    }

    public double getArea() {
        return Math.PI * Math.pow(radius,2);
    }
    public double getCircuit() {
        return 2*Math.PI * radius;
    }
}
