package b_exercises.CL.java_3_inheritance.exercise_5;

import b_exercises.CL.java_3_inheritance.exercise_4.Employee;

public class HourlyEmployee extends Employee {
    public HourlyEmployee(int id, String firstName, String lastName, double wage) {
        super(id, firstName, lastName, wage);
    }

    public double calculatePayment(int hours) {
        return hours * getWage();
    }
}
