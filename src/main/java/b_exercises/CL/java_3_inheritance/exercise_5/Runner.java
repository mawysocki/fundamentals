package b_exercises.CL.java_3_inheritance.exercise_5;

import org.junit.Assert;

//Stwórz klasę HourlyEmployee, reprezentującą pracownika, któremu pracodawca płaci za godziny. Klasa ma spełniać następujące wymogi:
//
//ma dziedziczyć po klasie Employee,
//posiadać dodatkową metodę calculatePayment(hours), która będzie zwracała kwotę do wypłacenia pracownikowi za liczbę wypracowanych godzin.
public class Runner {
    public static void main(String[] args) {

        double basicWage = 10;
        double raiseInPercent = 20;
        int workingHours = 120;
        double expectedSalary = 1440;
        HourlyEmployee h1 = new HourlyEmployee(2, "Duck", "Duffy", basicWage);
        h1.raiseWage(raiseInPercent);
        Assert.assertEquals(expectedSalary, h1.calculatePayment(workingHours), 0);
    }
}
