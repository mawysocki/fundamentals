package b_exercises.CL.java_3_inheritance.exercise_2;

import org.junit.Assert;
import java.awt.*;

//Stwórz klasę Shape, która będzie posiadała:
//
//prywatne atrybuty x i y (określające środek tego kształtu) oraz color,
//konstruktor, przyjmujący zmienne określające wartości x, y i color,
//metodę o nazwie getDescription(), wypisującą informacje o tym kształcie, zwracającą wartość typu String,
//metodę o nazwie getDistance(Shape shape), zwracającą odległość od środka innego kształtu, wynik ma być typu double.
public class Runner {
    public static void main(String[] args) {
        Shape s1 = new Shape(0, 0, Color.RED);
        Shape s2 = new Shape(3, 4, Color.BLACK);
        double distance = s1.getDistance(s2);
        double expectedDistance = 5.0;
        Assert.assertEquals(expectedDistance, distance, 0);
        System.out.println("Distance: " + distance);
    }
}
