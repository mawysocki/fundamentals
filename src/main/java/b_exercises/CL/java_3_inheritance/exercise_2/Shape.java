package b_exercises.CL.java_3_inheritance.exercise_2;

import java.awt.*;

public class Shape {

    private int x;



    private int y;



    private Color color;
    public Shape(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Color getColor() {
        return color;
    }
    public String getDescription() {
        return String.format("Center point: x:%s, y%s & color: %s", x, y, color);
    }

    public double getDistance(Shape shape) {
        double square = shape.x * shape.x + shape.y * shape.y;
        return Math.sqrt(square);
    }
}
