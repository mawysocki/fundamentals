package b_exercises.CL.java_1_homework.c_exceptions;

import java.util.NoSuchElementException;

//Stwórz metodę o sygnaturze: public static int indexOf(int[] elements, int value).
public class Exercise_5 {

    public static void main(String[] args) {
        int[] arr = {12, 35, 16, 17, 25, 999};
        try {
            System.out.println(indexOf(arr, 12));
            System.out.println(indexOf(arr, 35));
            System.out.println(indexOf(arr, 112));
        } catch (NoSuchElementException e) {
            System.out.println("Element does not exist");
        }

    }

    public static int indexOf(int[] elements, int value) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == value) {
                //return przerywa też działanie pętli
                return i;
            }
        }
        throw new NoSuchElementException();
    }
}
