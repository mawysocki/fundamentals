package b_exercises.CL.java_1_homework.c_exceptions;

//Main02.java umieść metodę o sygnaturze: public static String safeGet(String[] strTab, int index).
//Uzupełnij ciało metody tak, aby zwracała element tablicy strTab zawarty pod indeksem index,
//dodaj obsługę odpowiedniego wyjątku,
//przetestuj działanie programu.
public class Exercise_2 {

    public static void main(String[] args) {
        String[] arr = {"AB", "CD", "EF", "XY"};
        System.out.println(safeGet(arr, 0));
        System.out.println(safeGet(arr, arr.length-1));
        System.out.println(safeGet(arr, arr.length));
        System.out.println(safeGet(arr, -1));
    }
    public static String safeGet(String[] strTab, int index) {
        try {
            return strTab[index];
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Incorrect index");
        }
        return "";
    }
}
