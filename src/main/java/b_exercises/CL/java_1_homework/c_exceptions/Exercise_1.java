package b_exercises.CL.java_1_homework.c_exceptions;

//Stwórz metode o sygnaturze public static void average(String a, String b)
//uzupełnij ciało metody tak, aby dokonała konwersji zmiennych a i b na typ liczbowy int i wykonała dzielenie zmiennej a przez zmienną b,
//zabezpiecz program przed możliwymi błędami,
//dodaj sekcję, która wykona się zawsze niezależnie od tego, czy wystąpi błąd, czy nie,
//pamiętaj o błędzie ArithmeticException,
//zmień typ zmiennych a i b na double i sprawdź jakie otrzymasz wyniki w przypadku dzielenia przez 0.
public class Exercise_1 {

    public static void main(String[] args) {
        //Rozszerzyłem metodę o dodatkowy parametr, aby wybierać rodzaj konwersji
        average("123", "12", false);
        average("123", "12", true);
        average("123", "ABC", false);
        average("123", "ABC", true);
        average("123", "0", false);
        average("123", "0", true);
    }
    public static void average(String a, String b, boolean isDouble) {
        int num1, num2;
        double numA, numB;
        try {
            if (!isDouble) {
                num1 = Integer.parseInt(a);
                num2 = Integer.parseInt(b);
                System.out.println(num1/num2);
            }
            else {
                numA = Integer.parseInt(a);
                numB = Integer.parseInt(b);
                if (numB == 0.0) {
                    //Dzielenie przez 0 w przypadku liczb zmienno przecinkowych nie rzuca błędu tylko wyświetla komunikat "Infinity"
                    //Aby dzielenie przez 0 było obsłużone w ten sam sposób należy w bloku try wyrzuć ręcznie wyjątek, aby został później złapany
                    throw new ArithmeticException();
                }
                System.out.println("Result: " + numA/numB);
            }
        } catch (NumberFormatException e) {
            System.out.println("String cannot be convert to number");
        } catch (ArithmeticException e) {
            System.out.println("Cannot divide by 0");
        }
        finally {
        }
    }
}
