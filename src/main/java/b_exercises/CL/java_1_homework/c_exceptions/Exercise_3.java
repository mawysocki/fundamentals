package b_exercises.CL.java_1_homework.c_exceptions;

//Stwórz metodę o sygnaturze: public static int getLength(String str).
//uzupełnij ciało metody tak, aby zwracała długość napisu str,
//przetestuj działanie metody przekazując do niej parametr o wartości null,
//zabezpiecz program przed wystąpieniem wyjątku NullPointerException.
public class Exercise_3 {

    public static void main(String[] args) {
        System.out.println(getLength("ABCDEF"));
        System.out.println(getLength(""));
        System.out.println(getLength(null));
    }

    public static int getLength(String str) {
        if (str != null) {
            return str.length();
        } else {
            System.out.println("String is null");
            return 0;
        }
    }
}
