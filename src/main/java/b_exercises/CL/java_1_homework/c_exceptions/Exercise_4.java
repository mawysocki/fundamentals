package b_exercises.CL.java_1_homework.c_exceptions;

//Stwórz metodę o sygnaturze: public static int toInt(String str).
//uzupełnij ciało metody tak, aby zwracała napis str zamieniony na typ int,
//obsłuż możliwe wyjątki.
public class Exercise_4 {
    public static void main(String[] args) {
        System.out.println(toInt("123"));
        System.out.println(toInt("12345678912345678"));
        System.out.println(toInt("ABC"));
        System.out.println(toInt(""));
        System.out.println(toInt(null));

    }
    public static int toInt(String str) {
        try {
            return Integer.parseInt(str);
        }
        catch (NumberFormatException e) {
            System.out.println("Cannot convert to int");
            return 0;
        }
    }
}
