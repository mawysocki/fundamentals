package b_exercises.CL.java_1_homework.b_arrays;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

//metodę o sygnaturze: public static int[] returnUnique(int[] arr)
//Uzupełnij ciało metody w taki sposób by z tablicy arr wybrała tylko unikalne wartości, które następnie mają być zwrócone z metody.
//Przykład dla tablicy [1,1,2,3,3,4], powinniśmy otrzymać [1,2,3,4].
public class Exercise_1A {

    @Test
    public void uniqueArrayTest1() {
        int[] arr = {1, 1, 2, 3, 3, 4};
        int[] expected = {1, 2, 3, 4};
        int[] result = returnUniqueArray(arr);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void uniqueArrayTest2() {
        int[] arr = {3, 6, 1, 5, 3, 6, 3, 4, 5, 4};
        int[] expected = {1, 3, 4, 5, 6};
        int[] result = returnUniqueArray(arr);
        Assert.assertArrayEquals(expected, result);
    }

    private static int[] returnUniqueArray(int[] arr) {
        Arrays.sort(arr);
        int newNumber = arr[0];
        int[] result = {newNumber};
        for (int i : arr) {
            if (i != newNumber) {
                newNumber = i;
                result = addToArray(result, i);
            }
        }
        return result;
    }

    private static int[] addToArray(int[] oldArray, int newValue) {
        //Nowa tablica musi być większa od starej, aby dało się dodać nową wartość
        int[] newArray = new int[oldArray.length + 1];

        //Ręczne kopiowanie zawartości tablicy
        //Można skorzystać z gotowca System.arraycopy(oldArray, 0, newArray, 0, oldArray.length);
        for (int i = 0; i < oldArray.length; i++) {
            newArray[i] = oldArray[i];
        }
        newArray[newArray.length - 1] = newValue;
        return newArray;
    }
}
