package b_exercises.CL.java_1_homework.b_arrays;

import org.junit.Assert;
import org.junit.Test;

//metodę o sygnaturze: public static boolean contains(int[] arr, int element)
//uzupełnij ciało metody tak, by sprawdzała, czy w tablicy arr istnieje element,
//jeżeli tak ma zwracać true,
//jeżeli nie ma zwracać false.
public class Exercise_3 {

    @Test
    public void containsTest1() {
        int[] arr = {1, 1, 2, 3, 3, 4};
        int element = 3;
        Assert.assertTrue(contains(arr, element));
    }

    @Test
    public void containsTest2() {
        int[] arr = {1, 1, 2, 3, 3, 4};
        int element = 6;
        Assert.assertFalse(contains(arr, element));
    }

    public static boolean contains(int[] arr, int element) {
        for (int i : arr) {
            if (i == element) {
                return true;
                //Dla pierwszego znalezionego elementu zwraca go przerywając od razu dalsze działanie pętli jak i całej metody
            }
        }
        return false;
    }

}
