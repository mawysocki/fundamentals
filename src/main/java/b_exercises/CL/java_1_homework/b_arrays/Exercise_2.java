package b_exercises.CL.java_1_homework.b_arrays;

import org.junit.Assert;
import org.junit.Test;

//Uzupełnij ciało metody tak, aby dla otrzymanej tablicy jej zawartość została skopiowana do tymczasowej tablicy, której rozmiar ma być 2-krotnie większy od tablicy podanej.
//Uzupełnij brakujące elementy tymczasowej tablicy elementami tablicy pierwotnej w odwrotnej kolejności.
//Metoda ma zwrócić skopiowaną i uzupełnioną tablicę. Przykład: dla tablicy int arr[] = {1,2,3}; ma zostać zwrócona tablica int arrTmp[] = {1,2,3,3,2,1};
public class Exercise_2 {

    @Test
    public void appendTest1() {
        int[] arr = {1, 2, 3};
        int[] expected = {1, 2, 3, 3, 2, 1};
        int[] result = append(arr);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void appendTest2() {
        int[] arr = {1, 2, 2, 1, 3, 3};
        int[] expected = {1, 2, 2, 1, 3, 3, 3, 3, 1, 2, 2, 1};
        int[] result = append(arr);
        Assert.assertArrayEquals(expected, result);
    }

    private int[] append(int[] arr) {
        int[] newArray = new int[arr.length*2];
        for (int i = 0; i < newArray.length; i++) {
            if (i < arr.length) {
                newArray[i] = arr[i];
            }
            else {
                newArray[i] = arr[newArray.length-i - 1];
            }
        }
        return newArray;
    }
}
