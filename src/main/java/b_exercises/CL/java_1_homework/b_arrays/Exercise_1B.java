package b_exercises.CL.java_1_homework.b_arrays;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//metodę o sygnaturze: public static ArrayList<Integer> returnUnique(int[] arr)
//Uzupełnij ciało metody w taki sposób by z tablicy arr wybrała tylko unikalne wartości, które następnie mają być zwrócone z metody.
//Przykład dla tablicy [1,1,2,3,3,4], powinniśmy otrzymać [1,2,3,4].
//Wariant z wykorzystaniem listy
public class Exercise_1B {

    @Test
    public void uniqueListTest1() {
        int[] arr = {1, 1, 2, 3, 3, 4};
        List<Integer> expected = Arrays.asList(1, 2, 3, 4);
        List<Integer> result = returnUniqueList(arr);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void uniqueArrayTest2() {
        int[] arr = {3, 6, 1, 5, 3, 6, 3, 4, 5, 4};
        List<Integer> expected = Arrays.asList(1, 3, 4, 5, 6);
        List<Integer> result = returnUniqueList(arr);
        Assert.assertEquals(expected, result);
    }

    private static List<Integer> returnUniqueList(int[] arr) {
        Arrays.sort(arr);
        int newNumber = arr[0];
        List<Integer> result = new ArrayList<>();
        result.add(newNumber);
        for (int i : arr) {
            if (i != newNumber) {
                newNumber = i;
                result.add(newNumber);
            }
        }
        return result;
    }

}
