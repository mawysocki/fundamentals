package b_exercises.CL.java_1_homework.a_methods;

import org.junit.Assert;
import org.junit.Test;

//Stwórz metodę do obliczania wieku psa na psie lata
//Metoda powinna przyjmować wiek psa parametr
//dla pierwszych dwóch lat, każdy rok życia psa jest równy 10.5 ludzkiego roku,
//powyżej dwóch lat, każdy rok życia psa, to 4 ludzkie lata,
//funkcja powinna zwrócić wiek psa.
public class Exercise_1 {

    @Test
    public void dogTest1() {
        double realAge = 1.5;
        double expectedAge = 15.75;
        Assert.assertEquals(expectedAge, doAge(realAge), 0.0);
    }

    @Test
    public void dogTest2() {
        double realAge = 5;
        double expectedAge = 33;
        Assert.assertEquals(expectedAge, doAge(realAge), 0.0);
    }

    @Test
    public void dogTest3() {
        double realAge = 8;
        double expectedAge = 45;
        Assert.assertEquals(expectedAge, doAge(realAge), 0.0);
    }
    public static double doAge(double age) {
        double dogAge;
        if (age <= 2) {
            dogAge = age * 10.5;
        }
        else {
            dogAge = 2*10.5 + 4 * (age - 2);
        }
        return dogAge;
    }
}
