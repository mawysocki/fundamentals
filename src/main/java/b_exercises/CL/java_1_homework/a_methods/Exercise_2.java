package b_exercises.CL.java_1_homework.a_methods;

import org.junit.Assert;
import org.junit.Test;

//Stwórz metodę która sprawdza czy liczba A jest podzielna przez liczbę B
//Metoda powinna zwracać true bądź false
public class Exercise_2 {

    @Test
    public void divisionTest1() {
        int a = 12;
        int b = 3;
        Assert.assertTrue(divisible(a, b));
    }

    @Test
    public void divisionTest2() {
        int a = 12;
        int b = 5;
        Assert.assertFalse(divisible(a, b));
    }

    public static boolean divisible(int a, int b) {
        return a % b == 0;
    }
}
