package b_exercises.CL.java_3_homework_exercise_4;

import b_exercises.CL.java_2_homework.Book;
import b_exercises.CL.java_2_homework.Library;
import b_exercises.CL.java_3_homework_exercise_3.Book2;
import org.junit.Assert;

public class Runner {
    public static void main(String[] args) {
        User2 bookUser = new User2(1, "Anna", "Czytelniczka", new Book2[] {Library.bookA, Library.bookB, Library.bookC});
        int expectedBooks = 3;
        Assert.assertEquals(expectedBooks, bookUser.getNumberOfBooks());
        bookUser.addBook(Library.bookD);
        expectedBooks = 4;
        Assert.assertEquals(expectedBooks, bookUser.getNumberOfBooks());
        bookUser.returnBook(Library.bookB);
        expectedBooks = 3;
        Assert.assertEquals(expectedBooks, bookUser.getNumberOfBooks());
        bookUser.returnAllBooks();
        expectedBooks = 0;
        Assert.assertEquals(expectedBooks, bookUser.getNumberOfBooks());
    }
}
