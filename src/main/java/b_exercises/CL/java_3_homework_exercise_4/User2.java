package b_exercises.CL.java_3_homework_exercise_4;

import b_exercises.CL.java_2_homework.Book;
import b_exercises.CL.java_2_homework.User;
import b_exercises.CL.java_3_homework_exercise_3.Book2;

//Kopia klasy User
public class User2 {

    private int id;

    private String firstName;
    private String lastName;
    protected Book2[] books;

    public User2(int id, String firstName, String lastName, Book2[] books) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.books = books;
    }

    public void addBook(Book2 book) {
        //Jeśli nie ma żadnych książek dodanych to grozi rzuceniem wyjątku NullPointerException
        int currentSize = books == null ? 0 : books.length;
        Book2[] newBooks = new Book2[currentSize + 1];
        for (int i = 0; i < currentSize; i++) {
            newBooks[i] = books[i];
        }
        newBooks[newBooks.length-1] = book;
        books = newBooks;
    }

    public void displayAllTitles() {
        System.out.println(String.format("User %s %s has books: ", firstName, lastName));
        if (books != null) {
            for (Book2 book : books) {
                System.out.println(book.getTitle());
            }
        }
        else {
            System.out.println("There are no books");
        }
    }

    public int getNumberOfBooks() {
        return this.books.length;
    }

    public void returnBook(Book2 book) {
        int currentLength = this.books.length;
        Book2[] booksCopy = new Book2[currentLength];
        if (book != null) {
            booksCopy = new Book2[currentLength-1];
            for(int myBooksIndex=0, copyIndex=0; myBooksIndex < currentLength; myBooksIndex++) {
                if (this.books[myBooksIndex].equals(book)) {
                    this.books[myBooksIndex].setAvailable(true);
                }
                else {
                    booksCopy[copyIndex] = this.books[myBooksIndex];
                    copyIndex++;
                }
            }
            this.books = booksCopy;
        }
    }

    public void returnAllBooks() {
        for (Book2 book : this.books) {
            book.setAvailable(true);
        }
        this.books = new Book2[0];
    }
}
