package b_exercises.CL.java_1.a_alghoritms;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Znajdź wszystkie pozycje w tablicy gdzie znajduje się liczba X
public class Exercise_3 {

    public static int[] findIndexes(int[] arr, int lookingNumber) {
        List<Integer> indexes = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == lookingNumber) {
                indexes.add(i);
            }
        }
        return convertToArray(indexes);
    }

    private static int[] convertToArray(List<Integer> list) {
        int[] arr = new int[list.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }
    @Test
    public void findIndexesTest() {
        int[] arr = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        int lookingNumber = 8;
        int[] expectedIndexes = {2};
        Assert.assertArrayEquals(expectedIndexes, findIndexes(arr, lookingNumber));
    }

    @Test
    public void findIndexesTest2() {
        int[] arr = {10, 9, 8, 7, 10, 5, 4, 3, 2, 10};
        int lookingNumber = 10;
        int[] expectedIndexes = {0, 4, 9};
        Assert.assertArrayEquals(expectedIndexes, findIndexes(arr, lookingNumber));
    }
}
