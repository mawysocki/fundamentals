package b_exercises.CL.java_1.a_alghoritms;

import java.util.Scanner;

public class Exercise_4 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int myNumber;
        int counter = 0;
        double sum = 0;
        do {
            myNumber = s.nextInt();
            counter++;
            sum += myNumber;
        } while (myNumber != 0);

        System.out.println("SUM: " + sum);
        System.out.println("AVG: " + sum/counter);
    }
}
