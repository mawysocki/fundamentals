package b_exercises.CL.java_1.a_alghoritms;

import org.junit.Assert;
import org.junit.Test;

//Znajdź największą liczbę w 10 elementowym zbiorze
public class Exercise_1 {
    public static int findMax(int[] arr) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {max = arr[i];}
        }
        return max;
    }

    @Test
    public void findMaxTest() {
        int[] arr = {-2, -7, 1, -3, 3, 9, -23, 1, 0, 2};
        int expectedMax = 9;
        Assert.assertEquals(expectedMax, findMax(arr));
    }
}
