package b_exercises.CL.java_1.a_alghoritms;

import org.junit.Assert;
import org.junit.Test;

//Dodaj wszystkie elementy 10 elementowej tablicy, a wynik napisz na ekranie
public class Exercise_2 {
    public static int sumArray(int[] arr) {
        int sum = 0;
        for (int x: arr) {
            sum += x;
        }
        return sum;
    }

    @Test
    public void sumArrayTest() {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int expectedSum = 55;
        Assert.assertEquals(expectedSum, sumArray(arr));
    }

    @Test
    public void sumArrayTest2() {
        int[] arr = {1, -2, 3, -4, 5, -6, 7, -8, 9, -10};
        int expectedSum = -5;
        Assert.assertEquals(expectedSum, sumArray(arr));
    }
}
