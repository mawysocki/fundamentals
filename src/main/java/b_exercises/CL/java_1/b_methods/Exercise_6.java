package b_exercises.CL.java_1.b_methods;

//Napisz publiczną metodę checkMaturity, która:
//
//przyjmie parametr liczbowy age oznaczający wiek użytkownika,
//sprawdzi, czy użytkownik jest pełnoletni,
//zwróci wartość true – jeśli jest pełnoletni,
//zwróci wartość false – jeśli nie jest.
public class Exercise_6 {
    public static void main(String[] args) {
        int age = 20;
        boolean result = checkMaturity(age);
        System.out.println("User is adult: " + result);

        age = 17;
        result = checkMaturity(age);
        System.out.println("User is adult: " + result);

        age = 18;
        result = checkMaturity(age);
        System.out.println("User is adult: " + result);
    }

    public static boolean checkMaturity(int age) {
        return age >= 18; //Nie trzeba przypisywać wyniku do zmiennej gdy chcemy go od razu zwrócić
    }
}
