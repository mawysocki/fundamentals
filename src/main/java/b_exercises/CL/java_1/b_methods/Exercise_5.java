package b_exercises.CL.java_1.b_methods;

import org.junit.Assert;

//W pliku Main05.java napisz publiczną metodę calculateNetto, która przyjmie argumenty:
//gross: kwotę brutto ceny zakupu,
//vat: wartość podatku VAT. Możesz założyć, że VAT ma być liczbą zmiennoprzecinkową z zakresu 0 – 1.
public class Exercise_5 {

    public static void main(String[] args) {
        double vat = 0.23;
        double netto = 100;
        double priceGross = calculateGross(netto, vat);
        System.out.println("Price gross: " + priceGross);

        double priceNetto = calculateNetto(priceGross, vat);
        System.out.println("Price gross: " + priceGross);

        //Sprawdzenie czy cena początkowa netto jest równa cennie netto po obliczeniach netto->brutto->netto
        Assert.assertEquals(netto, priceNetto, 0.0);
    }

    public static double calculateNetto(double gross, double vat) {
        if (vat <0 || vat > 1) {
            System.out.println("VAT z poza zakresu. Nie można obliczyć podatku");
            return gross;
        }
        return gross / (1+vat);
    }
    public static double calculateGross(double netto, double vat) {
        if (vat <0 || vat > 1) {
            System.out.println("VAT z poza zakresu. Nie można obliczyć podatku");
            return netto;
        }
        return netto + netto*vat;
    }
}
