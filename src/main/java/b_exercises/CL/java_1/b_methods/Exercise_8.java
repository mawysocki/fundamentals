package b_exercises.CL.java_1.b_methods;

//W pliku Main08.java napisz publiczną metodę maxOfThree, która przyjmie trzy parametry liczbowe. Metoda ma zwrócić największą liczbę.
public class Exercise_8 {
    public static void main(String[] args) {
        int result = maxOfThree(3, 9, 7);
        System.out.println(result);

        result = maxOfThree(11, 2, 7);
        System.out.println(result);

        result = maxOfThree(1, 2, 5);
        System.out.println(result);
    }
    public static int maxOfThree(int a, int b, int c) {
        int max = a;
        if (b>a) {
            max = b;
            if (c>b) {
                max = c;
            }
        }
        return max;
    }
}
