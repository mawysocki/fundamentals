package b_exercises.CL.java_1.b_methods;

//Gdy klub piłkarski A gra dwumecz z klubem piłkarskim B, oznacza to, że grają jeden mecz na boisku drużyny A i jeden na boisku drużyny B.
//Wygrywa ta drużyna, która zdobędzie więcej goli w obu meczach.
//W przypadku, gdy drużyny zdobyły tyle samo bramek, gole zdobyte na wyjeździe liczą się jako "trochę ważniejsze" i wygrywa ta drużyna, która zdobyła więcej bramek na wyjeździe.
//Remis w dwumeczu wypada wtedy, gdy obie drużyny zdobyły tyle samo bramek i mają tyle samo bramek na wyjeździe.

//Na przykład:
//W Pucharze Polski grają dwa zespoły: Grom i Błyskawica. Zespoły rozegrały następujące mecze:
//Gospodarz: Grom.
//Grom 0:2 Błyskawica
//Gospodarz: Błyskawica.
//Błyskawica 1:3 Grom
//Sumaryczny wynik dwumeczu wynosi 3:3. Jednak Grom na wyjeździe zdobył 3 bramki, a Błyskawica tylko 2. Zatem wygrywa Grom.

//Napisz metodę, footballWin, która przyjmie następujące parametry:
//gole zdobyte przez zespół A w pierwszym meczu (na boisku zepołu A),
//gole zdobyte przez zespół B w pierwszym meczu (na boisku zepołu A),
//gole zdobyte przez zespół A w drugim meczu (na boisku zespołu B),
//gole zdobyte przez zespół B w drugim meczu (na boisku zespołu B),
//po czym zwróci 1, jeśli dwumecz wygrał zespół A, 2 – jeśli B. W przypadku remisu, zwróć X. Wynik ma być łańcuchem tekstowym, a nie liczbą!
public class Exercise_10 {

    public static void main(String[] args) {
        //X 0:2 Y, Y 2:3 X -> Wygrana Y 4:3
        String winner = footballWin(0, 3, 2, 2);
        System.out.println(winner);

        //X 2:0 Y, Y 1:0 X-> Wygrana X 2:1
        winner = footballWin(2, 0, 1, 0);
        System.out.println(winner);

        //X 4:4 Y, Y 3:3 X-> Remis 7:7 -> Wygrna Y 4:3 w bramkach na wyjeździe
        winner = footballWin(4, 3, 3, 4);
        System.out.println(winner);

        //X 2:4 Y, Y 2:4 X-> Remis 6:6 -> Remis 4:4 w bramkach na wyjeździe
        winner = footballWin(2, 4, 2, 4);
        System.out.println(winner);

    }
    public static String footballWin(int teamAhome, int teamAaway, int teamBhome, int teamBaway) {
        int sumA = teamAhome + teamAaway;
        int sumB = teamBhome + teamBaway;
        String result = "";
        if (sumA == sumB) {
            //Najlepiej na początku sprawdzać sytuację remisową.
            if (teamAaway == teamBaway) {
                result = "X";
            }

            else {
                //W przypadku else nie musimy obawiać się sytuacji, że trafimy na sytuację A==B, bo zostało to już wcześniej rozstrzygnięte
                result = teamAaway > teamBaway ? "1": "2";
            }
        }
        else {
            result = sumA > sumB ? "1": "2";
        }
        return result;
    }
}
