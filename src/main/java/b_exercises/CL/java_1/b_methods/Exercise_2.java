package b_exercises.CL.java_1.b_methods;

import org.junit.Assert;
import org.junit.Test;

//Napisz metodę o sygnaturze: public static int square(int num),
//która zwróci wartość num podniesioną do kwadratu.
public class Exercise_2 {

    @Test
    public void squareTestA1() {
        int x = 3;
        int expected = 9;
        Assert.assertEquals(expected, square(x));
    }

    @Test
    public void squareTestA2() {
        int x = 15;
        int expected = 225;
        Assert.assertEquals(expected, square(x));
    }

    @Test
    public void squareTestB1() {
        int x = 3;
        int expected = 9;
        Assert.assertEquals(expected, square2(x));
    }

    @Test
    public void squareTestB2() {
        int x = 15;
        int expected = 225;
        Assert.assertEquals(expected, square2(x));
    }
    public static int square(int num) {
        return num*num;
    }

    public static int square2(int num) {
        return (int) Math.pow(num,2);
    }
}
