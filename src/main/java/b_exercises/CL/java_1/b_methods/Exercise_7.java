package b_exercises.CL.java_1.b_methods;

//W pliku Main07.java napisz publiczną metodę checkEvenOdd, która:
//przyjmie parametr liczbowy number,
//będzie zwracać wartość typu String,
//zwróci wynik "even", jeśli liczba jest parzysta,
//zwróci wynik "odd", jeśli liczba jest nieparzysta.
public class Exercise_7 {
    public static void main(String[] args) {
        System.out.println(checkEvenOdd(15));
        System.out.println(checkEvenOdd(0));
        System.out.println(checkEvenOdd(22));
    }

    public static String checkEvenOdd(int number) {
        if (number % 2 == 0) {
            return "Even";
        }
        else {
            return "Odd";
        }
    }
}
