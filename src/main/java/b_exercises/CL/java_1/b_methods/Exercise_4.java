package b_exercises.CL.java_1.b_methods;

//W pliku Main04.java napisz publiczną metodę createName, która przyjmie następujące parametry:
//name: imię,
//surname: nazwisko,
//nickname: pseudonim.
//Metoda ma zwrócić łańcuch tekstowy z połączonymi parametrami, w postaci: "imię pseudonim nazwisko".
public class Exercise_4 {

    public static void main(String[] args) {
        String text = createName("ABC", "DEF", "GHI");
        System.out.println(text);
    }
    public static String createName(String name, String surname, String nickname) {
        return String.format("%s %s %s", name, surname, nickname);
    }
}
