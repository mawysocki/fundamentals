package b_exercises.CL.java_1.b_methods;

//Napisz publiczną metodę factorial, która przyjmie jako parametr liczbę naturalną n.
//Metoda ma zwrócić wartość n! (silnia),czyli wynik mnożenia wszystkich liczb naturalnych w zakresie 1...n
public class Exercise_9 {

    public static void main(String[] args) {
        int n = 5;
        int result = factorial(n);
        System.out.println(n + "! = " + result);
        //1*2*3*4*5 = 120

        n = -3;
        result = factorial(n);
        System.out.println(n + "! = " + result);
    }
    public static int factorial(int n) {
        int result = 1;
        if (n < 0) {
            result = 0;
            System.out.println("Liczba nie może być ujemna!");
        }
        else {
            //Należy zignorować 0, żeby przy mnożeniu wynik nie wychodził 0
            for (int i = 1; i <= n; i++) {
                result *= i;
            }
        }
        return result;
    }

}
