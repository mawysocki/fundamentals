package b_exercises.CL.java_1.b_methods;

//napisz metodę o sygnaturze:
//public static int multiply(int multipler, int index),
//która zwróci wartość zmiennej multipler pomnożonej przez wartość argumentu index.
public class Exercise_1 {
    public static void main(String[] args) {
        //Wywołanie metody z przypisaniem wartości do
        int a = multiply(5, 10);
        int b = multiply(-3, 9);
        int c = multiply(10, 10);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }

    //Deklaracja metody
    public static int multiply(int multipler, int index) {
        return multipler * index;
    }
}
