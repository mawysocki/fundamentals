package b_exercises.CL.java_1.b_methods;

//napisz publiczną metodę convertToUsd, która przyjmuje parametr pln, czyli kwotę w złotówkach.
// Metoda ma zwrócić podaną kwotę w dolarach amerykańskich.
//Jako przelicznik przyjmij wartość 4.04 PLN = 1 USD.
public class Exercise_3 {

    public static void main(String[] args) {
        double usd = convertToUSD(5000);
        //Formatuje liczbę do dwóch miejsc po przecinku
        System.out.println(String.format("%.2f", usd));
    }
    public static double convertToUSD(double pln) {
        return pln/4.04;
    }
}
