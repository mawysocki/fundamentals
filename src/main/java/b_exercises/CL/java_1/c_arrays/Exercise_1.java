package b_exercises.CL.java_1.c_arrays;

//stwórz 50-elementową tablicę o nazwie mainTab z kolejnymi liczbami całkowitymi od 0 do 49,
//wypisz na konsoli po kolei elementy tej tablicy, po dziesięć w każdym wierszu,
//liczby jednocyfrowe uzupełnij dodatkowym zerem na początku (np. 03).
public class Exercise_1 {

    public static void main(String[] args) {
        int[] mainTab = new int[50];
        for (int i = 0; i < 50; i++) {
            mainTab[i] = i;
        }
        for (int i = 0; i < 50; i++) {
            if (i < 10) {
                System.out.print("0");
            }
            System.out.print(i + ", ");
            if (i % 10 == 9) {
                System.out.println();
            }
        }
    }
}
