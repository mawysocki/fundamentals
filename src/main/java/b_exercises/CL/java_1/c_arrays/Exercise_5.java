package b_exercises.CL.java_1.c_arrays;

import java.util.Arrays;
import java.util.Comparator;

//stwórz tablicę o nazwie numbers zawierającą 10 dowolnych liczb,
//posortuj rosnąco tablicę wykorzystując metody klasy Arrays,
//wyświetl w konsoli posortowaną tablicę – również przy pomocy metody z klasy Arrays
public class Exercise_5 {
    public static void main(String[] args) {
        //Tablica jest typu referencyjnego, aby można było wykorzystać gotowy mechanizm do sortowania w dwie strony.
        //Klasa opakowująca Integer działa w ten sam sposób jak typ prosty int
        Integer[] arr = {3, 5, 7, 2, 1, 7, 11, 99, 12};
        sortAscending(arr);
        sortDescending(arr);
    }

    public static void sortAscending(Integer[] arr) {
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr));
    }
    private static void sortDescending(Integer[] arr) {
        Arrays.sort(arr, Comparator.reverseOrder());
        System.out.println(Arrays.toString(arr));
    }
}
