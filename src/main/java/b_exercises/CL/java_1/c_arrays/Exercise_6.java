package b_exercises.CL.java_1.c_arrays;

import org.junit.Assert;
import org.junit.Test;

//stwórz tablicę o nazwie numbers zawierającą 10 dowolnych liczb,
//stwórz drugą tablicę, nazwij ją secondNumbers, wypełnij 10 dowolnymi liczbami,
//stwórz trzecią tablicę zawierającą sumy elementów, które mają w tablicach pozycję o tym samym indeksie.
public class Exercise_6 {

    @Test
    public void arrayTest() {
        int[] numbers = {2, 3};
        int[] secondNumbers = {3, 4};
        int[] expected = {5, 7};
        Assert.assertArrayEquals(expected, sumArrays(numbers, secondNumbers));
    }

    @Test
    public void arrayTest2() {
        int[] numbers = {2, 3, 1, -5, 0, 12, 99, 0, 12, 1};
        int[] secondNumbers = {-2, 33, 11, 5, 0, 12, 101, 5000, 112, 11};
        int[] expected = {0, 36, 12, 0, 0, 24, 200, 5000, 124, 12};
        Assert.assertArrayEquals(expected, sumArrays(numbers, secondNumbers));
    }

    private int[] sumArrays(int[] numbers, int[] secondNumbers) {
        int[] sum = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            sum[i] = numbers[i] + secondNumbers[i];
        }
        return sum;
    }
}
