package b_exercises.CL.java_1.c_arrays;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

//Stwórz tablicę o nazwie numbers zawierającą 10 dowolnych liczb,
//Stwórz zmienną tablicową reverse i umieść w niej elementy tablicy numbers w odwrotnej kolejności,
//Wypisz na konsoli elementy z tablicy reverse.
public class Exercise_4 {

    @Test
    public void reverseTest1() {
        int[] arr = {3, 5, 7, 2, 1};
        int[] expected = {1, 2, 7, 5, 3};
        Assert.assertArrayEquals(expected, reverse(arr));
    }

    @Test
    public void reverseTest2() {
        int[] arr = {2, 4, 6, 8, 10, 12, 14, 16, 20, 25};
        int[] expected = {25, 20, 16, 14, 12, 10, 8, 6, 4, 2};
        Assert.assertArrayEquals(expected, reverse(arr));
    }

    private int[] reverse(int[] arr) {
        int[] reverse = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            int index = arr.length - 1 - i;
            reverse[i] = arr[index];
        }
        System.out.println(Arrays.toString(reverse));
        return reverse;
    }
}
