package b_exercises.CL.java_1.c_arrays;

import java.util.Arrays;

//1) stwórz 10-elementową tablicę wypełnioną pojedynczą wartością "2".
//2) zadanie wykonaj bez użycia pętli for
public class Exercise_3 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(loopArray(10)));
        System.out.println(Arrays.toString(manualArray(10)));
        System.out.println(Arrays.toString(recursiveArray(10)));
    }

    public static int[] loopArray(int size) {
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = 2;
        }
        return arr;
    }

    public static int[] manualArray(int size) {
        int[] arr = new int[size];
        arr[0] = 2;
        arr[1] = 2;
        arr[2] = 2;
        arr[3] = 2;
        arr[4] = 2;
        arr[5] = 2;
        arr[6] = 2;
        arr[7] = 2;
        arr[8] = 2;
        arr[9] = 2;
        return arr;
    }

    public static int[] recursiveArray(int size) {
        int[] arr = new int[size];
        int index = 0;
        arr = addValue(arr, index);
        return arr;
    }
    private static int[] addValue(int[] arr, int index) {
        arr[index] = 2;
        index++;
        if (index < arr.length) {
            //Metoda wywołuje sama siebie. Może działać jak pętla ale musi też mieć jakies zakończenie dlatego jest w instrukcji IF
            arr = addValue(arr, index);
        }
        return arr;
    }
}
