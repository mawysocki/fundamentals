package b_exercises.CL.java_1.c_arrays;

import java.util.Random;

//Przy użyciu pętli for stwórz tablicę o nazwie randNumbers, zawierającą 20 losowych liczb z zakresu 0-100
//wypisz w konsoli minimalną wartość z tablicy.
public class Exercise_2 {
    public static void main(String[] args) {
        int count = 20;
        int range = 100;
        int[] randNumbers = generateNumbers(count, range);
        int min = findMin(randNumbers);
        System.out.println("MIN: " + min);
        displayAllNumbers(randNumbers);
    }




    private static int[] generateNumbers(int count, int range) {
        Random random = new Random();
        int[] arr = new int[count];
        for (int i = 0; i < count; i++) {
            arr[i] = random.nextInt(range+1);
        }
        return arr;
    }

    private static int findMin(int[] randNumbers) {
        int min = Integer.MAX_VALUE;
        for (int randNumber : randNumbers) {
            if (randNumber < min) {
                min = randNumber;
            }
        }
        return min;
    }

    private static void displayAllNumbers(int[] randNumbers) {
        for (int randNumber : randNumbers) {
            System.out.println(randNumber);
        }
    }
}
