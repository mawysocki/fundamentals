package b_exercises.CL.java_2_homework;

public class Author {

    private int id;
    private String firstName;
    private String lastName;

    private String pseudonim;

    public Author(int id, String firstName, String lastName, String pseudonim) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pseudonim = pseudonim;
    }

    public int getID() {
        return id;
    }


}
