package b_exercises.CL.java_2_homework;

import b_exercises.CL.java_3_homework_exercise_3.Book2;

public class Library {

    public static final Book book1 = new Book(1, "Moce i Dniepr", true, Exercise_2.author2, null);
    public static final Book book2 = new Book(2, "Zbrodnia Ikara", true, Exercise_2.author4, null);
    public static final Book book3 = new Book(3, "Antyzgony", true, Exercise_2.author3, null);
    public static final Book book4 = new Book(4, "Dziady i chłopi", true, Exercise_2.author1, new Author[]{Exercise_2.author2, Exercise_2.author3, Exercise_2.author4});

    public static final Book2 bookA = new Book2(1, "Moce i Dniepr", true, Exercise_2.author2, null);
    public static final Book2 bookB = new Book2(2, "Zbrodnia Ikara", true, Exercise_2.author4, null);
    public static final Book2 bookC = new Book2(3, "Antyzgony", true, Exercise_2.author3, null);
    public static final Book2 bookD = new Book2(4, "Dziady i chłopi", true, Exercise_2.author1, new Author[]{Exercise_2.author2, Exercise_2.author3, Exercise_2.author4});
}
