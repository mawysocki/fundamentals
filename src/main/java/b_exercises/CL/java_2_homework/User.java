package b_exercises.CL.java_2_homework;

public class User {
    private int id;


    private String firstName;
    private String lastName;
    protected Book[] books;

    public User(int id, String firstName, String lastName, Book[] books) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.books = books;
    }

    public void addBook(Book book) {
        //Jeśli nie ma żadnych książek dodanych to grozi rzuceniem wyjątku NullPointerException
        int currentSize = books == null ? 0 : books.length;
        Book[] newBooks = new Book[currentSize + 1];
        for (int i = 0; i < currentSize; i++) {
            newBooks[i] = books[i];
        }
        newBooks[newBooks.length-1] = book;
        books = newBooks;
    }

    public void displayAllTitles() {
        System.out.println(String.format("User %s %s has books: ", firstName, lastName));
        if (books != null) {
            for (Book book : books) {
                System.out.println(book.getTitle());
            }
        }
        else {
            System.out.println("There are no books");
        }
    }

    public int getNumberOfBooks() {
        return this.books.length;
    }
}
