package b_exercises.CL.java_2_homework;

public class Exercise_4 {

    public static void main(String[] args) {
        BankAccount account1 = new BankAccount(777777777);
        BankAccount account2 = new BankAccount(999999999);

        System.out.println(account1);
        account1.depositCash(1000);
        System.out.println(account1);
        account1.withDrawCash(600);
        System.out.println(account1);
        account1.withDrawCash(600);
        System.out.println(account1);
        account1.withDrawCash(600);
        System.out.println(account1);
        account1.depositCash(250);
        System.out.println(account1);
        System.out.println();

        System.out.println(account2);
        account2.depositCash(100);
        System.out.println(account2);
        account2.withDrawCash(300);
        System.out.println(account2);
        account2.withDrawCash(300);
        System.out.println(account2);
    }
}
