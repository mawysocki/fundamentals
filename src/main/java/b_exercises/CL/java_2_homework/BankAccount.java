package b_exercises.CL.java_2_homework;

//Stwórz klasę BankAccount, która ma spełniać następujące wymogi:
//
//Mieć prywatne atrybuty:
//number - atrybut ten powinien trzymać numer identyfikacyjny konta (dla uproszczenia możemy założyć, że numerem konta może być dowolna liczba całkowita),
//cash - atrybut określający ilość pieniędzy na koncie. Ma to być liczba zmiennoprzecinkowa. Atrybut cash powinien być zawsze nastawiany na 0 dla nowo tworzonego konta.
//Posiadać gettery do atrybutów number i cash, ale NIE posiadać do nich setterów (nie chcemy, żeby raz stworzone konto mogło zmienić swój numer, a do atrybutu cash dodamy specjalne funkcje modyfikujące jego wartość).
//
//Posiadać metodę void depositCash(amount) której rolą będzie zwiększenie wartości atrybutu cash o podaną wartość. Pamiętaj o sprawdzeniu, czy podana wartość jest:
//
//Większa od 0
//Posiadać metodę double withdrawCash(amount) której rolą będzie zmniejszenie wartości atrybutu cash o podaną wartość. Metoda ta powinna zwracać ilość wypłaconych pieniędzy. Dla uproszczenia zakładamy, że ilość pieniędzy na koncie nie może zejść poniżej 0, np. jeżeli z konta, na którym jest 300 zł, próbujemy wypłacić 500 zł, to metoda zwróci nam tylko 300 zł. Pamiętaj o sprawdzeniu, czy podana wartość jest:
//Większa od 0
//Posiadać metodę String toString() nieprzyjmującą żadnych parametrów. Metoda ta ma zwracać informację o numerze konta i jego stanie.
public class BankAccount {

    private final int number;
    private double cash;

    public BankAccount(int number) {
        this.number = number;
    }

    public void depositCash(double amount) {
        if (amount < 0) {
            System.out.println("Invalid value for deposit");
        } else {
            cash += amount;
            System.out.println(amount + " has been added to acount");
        }
    }
    public double withDrawCash(double withDraw) {
        if (cash >= withDraw) {
            cash -= withDraw;
            System.out.println("Your cash: " + withDraw);
            return withDraw;
        } else {
            if (cash >0) {
                double realWithdraw = cash;
                cash = 0;
                System.out.println("Your cash: " + realWithdraw);
                return realWithdraw;
            }
            else {
                System.out.println("Your account is empty");
                return 0;
            }
        }
    }

    @Override
    public String toString() {
        return String.format("Number account: %s - deposit: %s", number, cash);
    }
}
