package b_exercises.CL.java_2_homework;

//Stwórz klasę Book, która ma spełniać następujące wymogi:
//Mieć prywatne atrybuty:
//id - atrybut typu int ten powinien trzymać numer identyfikacyjny książki,
//title - atrybut typu String określający imię pracownika,
//available - atrybut typu boolean określający czy książka jest możliwa do wypożyczenia, z domyślną wartością ustawioną na true, książka może być wypożyczona, lub np. w renowacji - ma wtedy atrybut określony na false.
//author - atrybut typu Author,
//additionalAuthors - tablica obiektów klasy Author.
//Posiadać gettery do wszystkich pól.
//Posiadać settery do wszystkich pól.
public class Exercise_2 {

    //Autorzy stworzeni jako stałe statyczne, żeby można było z nich korzystać w innych klasach
    public static final Author author1 = new Author(1, "Andrzej", "Krawczyk", "Rosomak");
    public static final Author author2 = new Author(2, "Michał", "Nowak", "Dur brzuszny");
    public static final Author author3 = new Author(3, "Anna", "Michniewicz", "Wicher");
    public static final Author author4 = new Author(4, "Rafał", "Kozłowski", "Brzoza");

    public static void main(String[] args) {
        Author mainAuthor = author1;
        Author[] authors = new Author[3];
        authors[0] = author2;
        authors[1] = author3;
        authors[2] = author4;


        Book book1 = new Book(1, "Pan Vateusz", true, mainAuthor, authors);
        Book book2 = new Book(2, "Krzyżacy po pracy", false, mainAuthor, null);

        System.out.println(book1.getTitle());
        System.out.println(book2.getTitle());
    }
}
