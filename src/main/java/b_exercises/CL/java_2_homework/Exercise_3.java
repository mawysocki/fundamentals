package b_exercises.CL.java_2_homework;

//Stwórz klasę User, która ma spełniać następujące wymogi:
//Mieć prywatne atrybuty:
//id - atrybut ten powinien trzymać numer identyfikacyjny,
//firstName - atrybut określający imię autora,
//lastName - atrybut określający nazwisko autora,
//books - tablica obiektów klasy Book.
//Dodaj metodę addBook(Book book), która doda nową książkę do tablicy książek danego użytkownika.
//Zaimplementuj możliwość dynamicznej zmiany rozmiaru tablicy.
public class Exercise_3 {


    public static void main(String[] args) {

        User userA = new User(1, "Anna", "Czytelniczka", new Book[] {Library.book1, Library.book2, Library.book3});
        userA.addBook(Library.book4);
        userA.displayAllTitles();

        User userB = new User(2, "Marcin", "Leser", null);
        userB.displayAllTitles();
        userB.addBook(Library.book1);
        userB.addBook(Library.book4);
        userB.displayAllTitles();
    }
}
