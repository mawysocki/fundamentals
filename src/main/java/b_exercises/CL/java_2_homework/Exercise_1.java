package b_exercises.CL.java_2_homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//Stwórz klasę Author, która ma spełniać następujące wymogi:
//Mieć prywatne atrybuty:
//id - atrybut ten powinien trzymać numer identyfikacyjny,
//firstName - atrybut określający imię autora,
//lastName - atrybut określający nazwisko autora,
//pseudonim - atrybut określający pseudonim autora,
public class Exercise_1 {

    public static void main(String[] args) {
        List<Author> authors = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        for (int i = 1; i <= 3; i++) {
            System.out.println("Author with ID: " + i);
            System.out.println("Set name: ");
            String name = scanner.nextLine();
            System.out.println("Set lastname: ");
            String lastName = scanner.nextLine();
            System.out.println("Set pseudonim: ");
            String pseudonim = scanner.nextLine();

            authors.add(new Author(i, name, lastName, pseudonim));
        }

        System.out.println("IDs..");
        for (Author author : authors) {
            System.out.println(author.getID());
        }

    }
}
