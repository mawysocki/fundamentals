package b_exercises.CL.java_3_homework_exercise_3;

import b_exercises.CL.java_2_homework.Author;
import b_exercises.CL.java_2_homework.Book;

//Kopia klasy Book
public class Book2 {

    private int id;

    private String title;
    private boolean available;
    private Author author;

    private int popularity = 0;
    private Author[] additionalAuthors;

    public Book2(int id, String title, boolean available, Author author, Author[] additionalAuthors) {
        this.id = id;
        this.title = title;
        this.available = available;
        this.author = author;
        this.additionalAuthors = additionalAuthors;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAvailable(boolean available) {
        this.available = available;
        if (!available) {
            popularity++;
        }
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public void setAdditionalAuthors(Author[] additionalAuthors) {
        this.additionalAuthors = additionalAuthors;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public boolean isAvailable() {
        return available;
    }

    public Author getAuthor() {
        return author;
    }

    public Author[] getAdditionalAuthors() {
        return additionalAuthors;
    }

    public boolean equals(Book2 book) {
        if (book == null) {return false;}
        return book.getId() == this.getId();
    }


    public int getPopularity() {
        return popularity;
    }
}
