package b_exercises.CL.java_3_homework_exercise_3;

import org.junit.Assert;

//Zmodyfikuj klasę Book:
//
//Dodaj prywatny atrybut popularity który będzie przechowywał ilość wypożyczeń, ilość ta powinna się zwiększać o 1 z każdym wypożyczeniem.
//Zdefiniuj metodę equals(Book book), która na podstawie atrybutu id zwróci informacje czy obiekty są równe.
public class Runner {

    public static void main(String[] args) {
        Book2 b1 = new Book2(1, "Untitled", true, null, null);
        Book2 b2 = new Book2(2, "Random Title", true, null, null);
        Book2 b3 = new Book2(1, "Not Unique ID", true, null, null);

        Assert.assertTrue(b1.equals(b1));
        Assert.assertTrue(b1.equals(b3));
        Assert.assertFalse(b1.equals(b2));
        Assert.assertFalse(b2.equals(b3));

        b1.setAvailable(false);
        b1.setAvailable(true);
        b1.setAvailable(false);
        int expectedPopularity = 2;
        boolean expectedAvailability = false;
        Assert.assertEquals(expectedAvailability, b1.isAvailable());
        Assert.assertEquals(expectedPopularity, b1.getPopularity());
        Assert.assertEquals(0, b2.getPopularity());
        Assert.assertEquals(0, b3.getPopularity());
    }

}
