package b_exercises.CL.java_0_prework.c_loops;

//Napisz program który wyświetli taki schemat
//*
//* *
//* * *
//* * * *
//* * * * *
public class Exercise_9 {
    public static void main(String[] args) {
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
