package b_exercises.CL.java_0_prework.c_loops;

//W pliku Main06.java napisz kod, który na podstawie wartości zmiennej n=6
//wypisuje wszystkie liczby od zera do n. Przy każdej liczbie wypisz, czy jest parzysta, czy nie.
public class Exercise_6 {
    public static void main(String[] args) {
        int n = 6;
        method1(n);
        System.out.println();
        method2(n);
    }

    //Klasyczny IF
    public static void method1(int n) {
        for (int i = 0; i <= n; i++) {
            if (i % 2 == 0) {
                System.out.println(i + " - parzysta");
            }
            else {
                System.out.println(i + " - nieparzysta");
            }
        }
    }

    //Użycie IFa trójargumentowego
    public static void method2(int n) {
        for (int i = 0; i <= n; i++) {
            String text = i % 2==0 ? i + " - parzysta" :  i + " - nieparzysta";
            System.out.println(text);
        }
    }
}
