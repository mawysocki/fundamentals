package b_exercises.CL.java_0_prework.c_loops;

public class Exercise_5 {
    public static void main(String[] args) {
        int resultFor = 0;
        int resultWhile = 0;
        int max = 10;
        for (int i = 1; i <= max; i++) {
            resultFor += i;
        }

        int i = 1; //int i może być na nowo zadeklarowany ponieważ poprzedni if przestał istnieć.
        // Był zadeklarowany w pętli i tam skończył żywot
        while (i <= max) {
            resultWhile += i;
            i++;
        }

        System.out.println(resultFor);
        System.out.println(resultWhile);
    }
}
