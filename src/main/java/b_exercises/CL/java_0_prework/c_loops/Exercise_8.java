package b_exercises.CL.java_0_prework.c_loops;

//Napisz program który wyświetli taki schemat dla n = 5
//* 2 3 4 5
//* * 3 4 5
//* * * 4 5
//* * * * 5
//* * * * *
public class Exercise_8 {

    public static void main(String[] args) {
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 5; j++) {
                if (j > i) {
                    System.out.print(j + " ");
                } else {
                    System.out.print("* ");
                }

            }
            System.out.println();
        }
    }
}
