package b_exercises.CL.java_0_prework.c_loops;

//Stwórz dwie pętle niezależne i wypisz wartości ich liczników w każdej iteracji dla
// i<3 oraz j<3
public class Exercise_7 {
    public static void main(String[] args) {
        int max = 3;
        for (int i = 0; i< max; i++) {
            for (int j = 0; j < max; j++) {
                System.out.println("i = " + i + " j = " + j);
            }
        }
    }
}
