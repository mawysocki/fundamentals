package b_exercises.CL.java_0_prework.d_arrays;

//Temperatura w tablicy zapisana jest w stopniach Celsjusza, zamień ją na stopnie Fahrenheita.
//Przelicznik jest następujący: tempCelc * 1,8 + 32.
//Aby to zrobić przeiteruj tablicę i nadpisz daną temperaturę nową, obliczoną wartością.
//Oblicz średnią temperaturę z nowo wyliczonych wartości i przypisz do zmiennej avg.
//Wypisz ją w konsoli wg wzoru: ŚREDNIA: 3,95 – zaokrąglij liczbę do dwóch miejsc po przecinku.
//Możesz użyć metody:
public class Exercise_7 {
    public static void main(String[] args) {
        double[] temps = {30, 29, 14, 42, -4, -10, 8, 14, 32, 11, 8, 0, 0};
        double sum = 0;
        for (int i = 0; i < temps.length ; i++) {
            sum += (temps[i] * 1.8 + 32);
        }
        System.out.printf("%.2f%n", sum/temps.length);
    }
}
