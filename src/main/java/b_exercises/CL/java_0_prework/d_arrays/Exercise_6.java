package b_exercises.CL.java_0_prework.d_arrays;

//Stwórz tablicę z liczbami (4, 643, 112, 9999, -69), tablicę nazwij numbers.
//Wypisz elementy tablicy od końca, używając pętli for – każdy element w nowej linii.
public class Exercise_6 {
    public static void main(String[] args) {
        int[] numbers = {4, 643, 112, 9999, -69};
        for (int i = numbers.length-1; i >= 0; i--) {
            System.out.println(numbers[i]);
        }
    }

}
