package b_exercises.CL.java_0_prework.d_arrays;

//Stwórz tablicę z liczbami (4, 643, 112, 9999, -69), tablicę nazwij numbers.
//Stwórz zmienną min i przypisz do niej 0.
//Za pomocą pętli for znajdź liczbę, która jest najmniejsza, przypisz ją do zmiennej min i wypisz na konsoli.
public class Exercise_5 {
    public static void main(String[] args) {
        int[] numbers = {4, 643, 112, 9999, -69};
        int min = Integer.MAX_VALUE;
        //UWAGA w treści zadania jest błąd w sugestii. Nie deklarujemy zmiennej min z wartością 0, ponieważ nigdy nie znajdziemy liczb ujemnych
        //Jeśli w zadaniu mamy szukanie najmniejszej liczby to na wstępie należy nadać wartość NAJWIĘKSZĄ z możliwych
        //jeśli szukamy największej wartości to zaczynamy od Integer.MIN_VALUE
        for (int number : numbers) {
            if (number < min) {
                min = number;
            }
        }
        System.out.println(min);
    }


}
