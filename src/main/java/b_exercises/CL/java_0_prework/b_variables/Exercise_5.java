package b_exercises.CL.java_0_prework.b_variables;

//w metodzie main stwórz trzy zmienne:
//nr1, typu double o wartości 5.1,
//nr2, typu float o wartości 5.0f,
//result, typu boolean, przechowującą wynik sprawdzania, czy pierwsza liczba jest większa od drugiej.
//Wypisz wartość zmiennej result na konsoli.
public class Exercise_5 {
    public static void main(String[] args) {
        double nr1 = 5.1;
        float nr2 = 5.0f;
        boolean result = nr1 > nr2;
        System.out.println(result);
    }
}
