package b_exercises.CL.java_3_constructor.exercise_2;

//Stwórz klasę Calculator. Konstruktor ma nie przyjmować żadnych danych. Każdy nowo stworzony obiekt powinien mieć tablicę String, w której będzie trzymać historię ostatnich operacji (stwórz ją w konstruktorze – z początkowym rozmiarem 0). Następnie dodaj do klasy następujące metody:
//
//add(num1, num2) – metoda ma dodać do siebie dwie zmienne i zwrócić wynik. Dodatkowo na liście operacji ma zapamiętać napis: "added num1 to num2 got result".
//multiply(num1, num2) – metoda ma pomnożyć przez siebie dwie zmienne i zwrócić wynik. Dodatkowo na liście operacji ma zapamiętać napis: "multiplied num1 with num2 got result".
//subtract(num1, num2) – metoda ma odjąć od siebie dwie zmienne i zwrócić wynik. Dodatkowo na liście operacji ma zapamiętać napis: "subtracted num1 from num2 got result".
//divide(num1, num2) – metoda ma podzielić przez siebie dwie zmienne i zwrócić wynik. Dodatkowo na liście operacji ma zapamiętać napis: "divided num1 by num2 got result". Pamiętaj, że nie można dzielić przez zero.
//printOperations() – metoda ma wypisać wszystkie zapamiętane operacje.
//clearOperations() – metoda ma wyczyścić wszystkie zapamiętane operacje.
//Możesz dopisać metodę pomocniczą, która będzie dodawała wpis na liście operacji oraz zwiększała rozmiar tablicy przez kopiowanie.
//
//Pamiętaj o używaniu this w odpowiednich miejscach. Stwórz kilka kalkulatorów i przetestuj ich działanie.
public class Runner {

    public static void main(String[] args) {
        Calculator c1 = new Calculator();
        Calculator c2 = new Calculator();
        Calculator c3 = new Calculator();
        c1.add(10, 20);
        c1.multiply(10,20);
        c2.subtract(100,30);
        c2.subtract(50, 30);
        c3.divide(10,2);
        c3.divide(10,0);
        c1.printOperations();
        c2.printOperations();
        c3.printOperations();
    }



}
