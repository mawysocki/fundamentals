package b_exercises.CL.java_3_constructor.exercise_2;

public class Calculator {

    private String[] history;
    public Calculator() {
        history = new String[0];
    }

    public int add(int num1, int num2) {
        int result = num1+num2;
        String message = String.format("added %s to %s got %s", num1, num2, result);
        addToHistory(message);
        return result;
    }
    public int subtract(int num1, int num2) {
        int result = num1-num2;
        String message = String.format("subtracted %s from %s got %s", num1, num2, result);
        addToHistory(message);
        return result;
    }
    public int multiply(int num1, int num2) {
        int result = num1*num2;
        String message = String.format("multiplied %s from %s got %s", num1, num2, result);
        addToHistory(message);
        return result;
    }
    public int divide(int num1, int num2) {
        int result = 0;
        String message = String.format("divided %s from %s got ", num1, num2);
        if (num2 != 0) {
            result = num1/num2;
            message += result;
        } else {
            message += "CANNOT DIVIDE BY ZERO!";
        }
        addToHistory(message);
        return result;
    }

    protected void addToHistory(String message) {
        int oldLenght = this.history.length;
        String[] history = new String[oldLenght+1];
        for (int i = 0; i < oldLenght; i++) {
            history[i] = this.history[i];
        }
        history[oldLenght] = message;
        this.history = history;
    }

    public void printOperations() {
        System.out.println("Operations history:");
        for (String s : this.history) {
            System.out.println(s);
        }
    }
    public void clearOperations() {
        this.history = new String[0];
    }
}
