package b_exercises.CL.java_3_constructor.exercise_1;

//Utwórz klasę Burger, dodaj w niej atrybuty:
//size,
//price.
//Utwórz konstruktor, który przyjmie i ustawi parametry size i price.
//Utwórz konstruktor bezargumentowy.
public class Burger {


    public String size;

    public int price;
    public Burger(String size, int price) {
        this.size = size;
        this.price = price;
    }

    public Burger() {
    }

}
