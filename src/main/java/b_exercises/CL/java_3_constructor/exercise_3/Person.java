package b_exercises.CL.java_3_constructor.exercise_3;


//Utwórz klasę Person, dodaj w niej atrybuty:
//name
//surname
//age
//gender
//Wszystkie atrybuty mają być prywatne.
//
//Utwórz konstruktor, który przyjmie i ustawi parametry name, surname.
//Utwórz konstruktor, który przyjmie i ustawi parametry name, age.
//Utwórz konstruktor, który przyjmie i ustawi parametry name, surname, age, gender.
public class Person {

    private String name;

    private String surname;
    private String gender;

    private int age;

    public Person(String name, String surname, int age, String gender) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.gender = gender;
    }

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

}
