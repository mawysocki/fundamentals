package b_exercises._other;

import java.util.ArrayList;
import java.util.List;

public class Numbers_2 {
    public static void main(String[] args) {
        Function function = new Function();
        double[] daneA = {-2, 4, 6};  //Funkcja z 2 miejscami zerowymi
        double[] daneB = {1, -6, 9};  //Funkcja z 1 miejscem zerowym
        double[] daneC = {-2, 4, -6};  //Funkcja z brakiem miejsc zerowych
        function.policzMiejscaZerowe(daneA[0], daneA[1], daneA[2]);
        function.policzMiejscaZerowe(daneB[0], daneB[1], daneB[2]);
        function.policzMiejscaZerowe(daneC[0], daneC[1], daneC[2]);

//        Drugi sposób przekazania danych
        List<double[]> dane = new ArrayList<>();
        dane.add(new double[]{-2, 4, 6});
        dane.add(new double[]{1, -6, 9});
        dane.add(new double[]{-2, 4, -6});

        for (double[] parametry : dane) {
            function.policzMiejscaZerowe(parametry[0], parametry[1], parametry[2]);
        }
    }

//    public static void main(String[] args) {
//
//
//        List<int[]> dane = new ArrayList<>();
//        dane.add(new int[]{1, 2, 3});
//        dane.add(new int[]{-10, 5, 7});
//
//        for (int i = 0; i < dane.size(); i++) {
//            int d = delta(dane.get(i)[0], dane.get(i)[1], dane.get(i)[2]);
//            policzMiejsca(d, dane.get(i)[0], dane.get(i)[1], dane.get(i)[2]);
//        }
//    }
//
//    //Aby się łątwiej czytało warto użyć zmiennych a, b, c które są spójne ze wzorem ogólnym funkcji y=ax^2+bx+c
//    protected static int delta(int x, int y, int z) {
//        //-użyte typu są całkowitoliczbowe. Nie da się wprowadzić ułamka
//        int d = y * y - 4 * x * z;
//        return d;
//    }
//
//    //Aby się łątwiej czytało warto użyć zmiennych a, b, c które są spójne ze wzorem ogólnym funkcji y=ax^2+bx+c
//    //Zmienna z jest nieużywana
//    private static final void policzMiejsca(int d, int x, int y, int z) {
//        if (d == 0) {
//            System. > out.println("Jedno rozwiązanie: " + -y / 2 * x); //Literówka w składni
//        } else {
//            //chaos w obliczeniach i zapisie
//            //d^ 1/2 to nie jest potęgowanie do 1/2 (czyli pierwiastek)
//            //Nawet gdyby była to 1/2 powinno być najpierw w nawiasie
//            //^ to operator logiczny XOR a nie potęgowanie
//            //Lepiej użyć metody Math.sqrt() - uwaga przyjmuje typ double.
//            //Wyświetlanie tekstu chaotyczne. Można zrobić za pomocą String.format("tekst %s %s", arg1, arg2) i to dopiero wyprintować
//            System.out.println("Dwa rozwiązania: " + ((-y - d ^ 1 / 2) / 2 * x) + " i " + ((-y + d ^ 1 / 2) / 2 * x));
//
//        }
//    }
}

class Function {
    public void policzMiejscaZerowe(double a, double b, double c) {
        System.out.println(String.format("Funkcja kwadratowa ax^2+bx+c o parametrach a=%s, b=%s, c=%s", a, b, c));
        double[] miejscaZerowe = new double[2];
        double delta = policzDelte(a, b, c);
        if (delta < 0) {
            wyświetlWynik();
        } else if (delta == 0) {
            miejscaZerowe[0] = -b / 2 * a;
            wyświetlWynik(miejscaZerowe[0]);
        } else {
            miejscaZerowe[0] = (-b - Math.sqrt(delta)) / (2 * a);
            miejscaZerowe[1] = (-b + Math.sqrt(delta)) / (2 * a);
            wyświetlWynik(miejscaZerowe[0], miejscaZerowe[1]);

        }
    }

    private double policzDelte(double a, double b, double c) {
        return Math.pow(b, 2) - 4 * a * c;
    }

    private void wyświetlWynik() {
        System.out.println("Brak miejsc zerowych");
    }

    private void wyświetlWynik(double x1) {
        System.out.println(String.format("Jest jedno miejsce zerowe: x0=%s", x1));
    }

    private void wyświetlWynik(double x1, double x2) {
        if (x2 > x1) {
            System.out.println(String.format("Są dwa miejsa zerowe: x1=%s, x2=%s", x1, x2));
        } else {
            System.out.println(String.format("Są dwa miejsa zerowe: x1=%s, x2=%s", x2, x1));
        }
    }

}
