package b_exercises._other;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Optional;

//Zsumuj dwie tablice typu int z konwersją na typ double
public class ArraysSum {
    public Optional<Double> sumArrays(int[] a, int[] b) {
        if (a == null && b == null) {
            return Optional.empty();
        }
        double sum = 0;
        if (a != null) {
            sum += getSum(a);
        }
        if (b != null) {
            sum += getSum(b);
        }
        return Optional.of(sum);
    }

    private double getSum(int[] arr) {
        double sum = 0;
        for (Iterator<Integer> i = Arrays.stream(arr).iterator(); i.hasNext();) {
            sum += (double) i.next();
        }
        return sum;
    }

    @Test
    public void test1() {
        int[] arr1 = {3,5};
        int[] arr2 = {10};
        Assert.assertEquals(sumArrays(arr1, arr2).get(), Double.valueOf(18));
    }

    @Test
    public void test2() {
        int[] arr1 = {3,5};
        int[] arr2 = null;
        Assert.assertEquals(sumArrays(arr1, arr2).get(), Double.valueOf(8));
    }

    @Test
    public void test3() {
        int[] arr1 = null;
        int[] arr2 = null;
        Assert.assertEquals(sumArrays(arr1, arr2), Optional.empty());
    }
}
